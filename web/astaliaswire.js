var result;
function getAliasWireACHResult() {
	return result;
}
/* This is the one it uses.  Not the one in ast_web !!! */
async function getACHToken(aURL, aPublishedKey, aAccountType, anAccountName, anAccountNumber, aRoutingNumber) {
  var myp = new Promise((resolve, reject) => {
    callAliasForTokenACH(
                aURL,
                aPublishedKey,
                aAccountType,
                anAccountName,
                anAccountNumber,
                aRoutingNumber,
				function (reply) {
					//result = JSON.stringify(reply);
					result = reply;
					resolve(reply);
				}
				);
	});
	return myp;
}
