function s(txt) {
	alert('x' + txt + 'x');
}
var SlimCD = (function () {
return {



	"library": {
		"name": "SlimCD_JavaScript"
		,"version": "1.1"
	}

	, "debug" : false
	, transURL : "https://trans.slimcd.com"
	, statsURL : "https://stats.slimcd.com"


/*	,"request" : {

			"username" : null
			,"password" : null
			,"metabankid" : null
			,"bankid" : null
			,"clientid" : null
			,"siteid" : null
			,"priceid" : null
			,"product" : null
			,"ver" : null
			,"key" : null
			,"kiosk" : null
			,"readerpresent" : null
			,"contactlessreader" : null
			,"encryption_device" : null
			,"encryption_type" : null
			,"encryption_key" : null
			,"firstname" : null
			,"lastname" : null
			,"address" : null
			,"city" : null
			,"state" : null
			,"zip" : null
			,"country" : null
			,"phone" : null
			,"email" : null
			,"birthdate" : null
			,"driverlic" : null
			,"ssn" : null
			,"gateid" : null
			,"use_pooled" : null
			,"processor_token" : null
			,"temporary_token" : null
			,"cardtype" : null
			,"corporatecard" : null
			,"trackdata" : null
			,"cardnumber" : null
			,"expmonth" : null
			,"expyear" : null
			,"cvv2" : null
			,"seccode" : null
			,"pinblockdata" : null
			,"pinblock" : null
			,"ksn" : null
			,"Checks" : null
			,"micrreader" : null
			,"accttype" : null
			,"checktype" : null
			,"routeno" : null
			,"accountno" : null
			,"checkno" : null
			,"fullmicr" : null
			,"serialno" : null
			,"statecode" : null
			,"achcode" : null
			,"transtype" : null
			,"amount" : null
			,"clienttransref" : null
			,"po" : null
			,"salestaxtype" : null
			,"salestax" : null
			,"authcode" : null
			,"cashback" : null
			,"gratuity" : null
			,"Allow_Partial" : null
			,"allow_duplicates" : null
			,"blind_credit" : null
			,"extra_credit" : null
			,"recurring" : null
			,"installmentcount" : null
			,"installmentseqno" : null
			,"billpayment" : null
			,"debtindicator" : null
			,"clientip" : null
			,"clerkname" : null
			,"cardpresent" : null
			,"contactless" : null
			,"send_email" : null
			,"send_cc" : null
			,"send_sms" : null
			,"cc_email" : null

	} */

	, "reply": {
		"response": "Error"
		,"responsecode": 2
		,"description": "Nothing processed"
		,"responseurl": ""
		,"datablock": null
		,"requestdata"  : null
		,"senddata" : null
		,"recvdata" : null
	}


/*
		{
		"response": null
		,"responsecode": null
		,"description": null
		,"responseurl": null
		,"datablock": {
			"authcode": null
			,"approved": null
			,"cvv2reply": null
			,"avsreply": null
			,"gateid": null
			,"bankid": null
			,"corporatecardindicator" : null
			,"invoiceno" : null
			,"firstname" : null
			,"lastname" : null
			,"cardtype" : null
			,"last4" : null
			,"expmonth" : null
			,"expyear" : null
			,"processor_token" : null
			,"approvedhsaamt" : null
			,"approvedamt" : null
			,"bal" : null
			,"returncheckservicecharge" : null
		}
	  }
*/

	, "Transact" : {

		 ProcessTransaction: function (request) {

			var timeout = 0;
			var callback=null;

			// set the URL, and specify the callback (which is the name of our own JavaScript function above)
			SlimCD.internal.url = SlimCD.internal.getTransServer() + '/soft/json/jsonpayment.asp?callback=SlimCD.internal.getreply'


    			// If more than 1 argument and last argument is a function
			if(arguments.length > 1)  {
				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}


			if (typeof(timeout) === "undefined" || timeout == 0)
				timeout = 60 ;

			if (typeof(callback) === "undefined")
				callback = null ;

			return(SlimCD.internal.CreateJSONP(SlimCD.internal.url,request,timeout,callback)) ;
                }


	 	, GetTemporaryToken : function (request) {

			var timeout = 0;
			var callback=null;

			// set the URL, and specify the callback (which is the name of our own JavaScript function above)
			SlimCD.internal.url = SlimCD.internal.getTransServer() + '/wswebservices/GetTemporaryToken.asp?callback=SlimCD.internal.getreply'

    			// If more than 1 argument and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}


			if (typeof(timeout) === "undefined" || timeout == 0)
				timeout = 30 ;

			if (typeof(callback) === "undefined")
				callback = null ;

			return(SlimCD.internal.CreateJSONP(SlimCD.internal.url,request,timeout,callback)) ;
                }


		, CloseBatch : function (request) {

			var timeout = 0;
			var callback=null;

    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("CloseBatch",request,timeout,callback) ;
		}

	 	, SetExtra : function (request) {

			var timeout = 0;
			var callback=null;

			// set the URL, and specify the callback (which is the name of our own JavaScript function above)
			SlimCD.internal.url = SlimCD.internal.getTransServer() + '/soft/json/jsonscript.asp?service=SetExtra&callback=SlimCD.internal.getreply'

    			// If more than 1 argument and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}


			if (typeof(timeout) === "undefined" || timeout == 0)
				timeout = 30 ;

			if (typeof(callback) === "undefined")
				callback = null ;

			return(SlimCD.internal.CreateJSONP(SlimCD.internal.url,request,timeout,callback)) ;
                }

	 	, GetExtra : function (request) {

			var timeout = 0;
			var callback=null;

			// set the URL, and specify the callback (which is the name of our own JavaScript function above)
			SlimCD.internal.url = SlimCD.internal.getTransServer() + '/soft/json/jsonscript.asp?service=GetExtra&callback=SlimCD.internal.getreply'

    			// If more than 1 argument and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}


			if (typeof(timeout) === "undefined" || timeout == 0)
				timeout = 30 ;

			if (typeof(callback) === "undefined")
				callback = null ;

			return(SlimCD.internal.CreateJSONP(SlimCD.internal.url,request,timeout,callback)) ;
                }


	}
	, "Login" : {

		 GetUserSettings : function (request) {

			var timeout = 0;
			var callback=null;

    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("GetUserSettings",request,timeout,callback) ;
		}

		, GetUserClients : function (request) {

			var timeout = 0;
			var callback=null;

    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("GetUserClients",request,timeout,callback) ;
		}
		, GetUserClientSite : function (request) {

			var timeout = 0;
			var callback=null;

    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("GetUserClientSite",request,timeout,callback) ;
		}

	}

	, "Reports" : {

		GetBatchHistory : function (request) {

			var timeout = 0;
			var callback=null;


    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("GetBatchHistory2",request,timeout,callback) ;
		}

		, GetBatchSummary : function (request) {

			var timeout = 0;
			var callback=null;


    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("GetBatchSummary2",request,timeout,callback) ;
		}


		, GetClosedBatchTransactions : function (request) {

			var timeout = 0;
			var callback=null;


    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("GetClosedBatchTransactions2",request,timeout,callback) ;
		}



		, GetDailySummary : function (request) {

			var timeout = 0;
			var callback=null;

    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("GetDailySummary",request,timeout,callback) ;
		}

		, GetFTGminiStatement : function (request) {

			var timeout = 0;
			var callback=null;


    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("GetFTGminiStatement",request,timeout,callback) ;
		}


		, GetOpenAuths : function (request) {

			var timeout = 0;
			var callback=null;

    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("GetOpenAuths2",request,timeout,callback) ;
		}


		, GetOpenBatch : function (request) {

			var timeout = 0;
			var callback=null;

    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("GetOpenBatch2",request,timeout,callback) ;
		}

		, GetTransactionDetails : function (request) {

			var timeout = 0;
			var callback=null;

    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("GetTransactionDetails2",request,timeout,callback) ;
		}

		, SearchTransactions : function (request) {

			var timeout = 0;
			var callback=null;

    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("SearchTransactions2",request,timeout,callback) ;
		}

	}

	, "Images" : {

		DownloadCheck : function (request) {

			var timeout = 0;
			var callback=null;


    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("DownloadCheck",request,timeout,callback) ;
		}

		, DownloadReceipt : function (request) {

			var timeout = 0;
			var callback=null;


    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("DownloadReceipt",request,timeout,callback) ;
		}


		, DownloadSignature : function (request) {

			var timeout = 0;
			var callback=null;


    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("DownloadSignature",request,timeout,callback) ;
		}

		, UploadCheck : function (request) {

			var timeout = 0;
			var callback=null;


    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("UploadCheck",request,timeout,callback) ;
		}

		, UploadReceipt : function (request) {

			var timeout = 0;
			var callback=null;


    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("UploadReceipt",request,timeout,callback) ;
		}

		, UploadSignature : function (request) {

			var timeout = 0;
			var callback=null;


    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("UploadSignature",request,timeout,callback) ;
		}

		, GetSignatureImage : function (request) {

			var timeout = 0;
			var callback=null;


    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("GetSignatureImage",request,timeout,callback) ;
		}

		, SendReceipt : function (request) {

			var timeout = 0;
			var callback=null;

    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("SendReceipt",request,timeout,callback) ;
		}

		, GetReceipt : function (request) {

			var timeout = 0;
			var callback=null;

    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("GetReceipt",request,timeout,callback) ;
		}

	}

	, "Sessions" : {

		CreateSession : function (request) {

        		var timeout = 0;
		        var callback = null;

		        // If more than 2 arguments and last argument is a function
		        if (arguments.length > 1) {

		            if (typeof arguments[arguments.length - 1] === 'function') {
                		callback = arguments[arguments.length - 1];
		                if ((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
                		    timeout = arguments[arguments.length - 2];
		            }
		            else if (typeof arguments[arguments.length - 1] === 'number')
		                timeout = arguments[arguments.length - 1];
		        }

		        SlimCD.CallWebService("CreateSession", request, timeout, callback);
		}

	    	, CheckSession : function (request) {

		        var timeout = 0;
		        var callback = null;

		        // If more than 2 arguments and last argument is a function
		        if (arguments.length > 1) {

		            if (typeof arguments[arguments.length - 1] === 'function') {
                		callback = arguments[arguments.length - 1];
		                if ((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
		                    timeout = arguments[arguments.length - 2];
		            }
		            else if (typeof arguments[arguments.length - 1] === 'number')
		                timeout = arguments[arguments.length - 1];
		        }

		        SlimCD.CallWebService("CheckSession", request, timeout, callback);
		}

    		, CancelSession : function (request) {

			var timeout = 0;
			var callback = null;

			// If more than 2 arguments and last argument is a function
		        if (arguments.length > 1) {

				if (typeof arguments[arguments.length - 1] === 'function') {
			                callback = arguments[arguments.length - 1];
		                if ((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
		                    timeout = arguments[arguments.length - 2];
		            }
		            else if (typeof arguments[arguments.length - 1] === 'number')
		                timeout = arguments[arguments.length - 1];
		        }

		        SlimCD.CallWebService("CancelSession", request, timeout, callback);
		}


		, DestroySession: function (request) {

			var timeout = 0;
			var callback = null;

			// If more than 2 arguments and last argument is a function
			if (arguments.length > 1) {

				if (typeof arguments[arguments.length - 1] === 'function') {
					callback = arguments[arguments.length - 1];
				if ((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
					timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
					timeout = arguments[arguments.length - 1];
		        }

        		SlimCD.CallWebService("DestroySession", request, timeout, callback);
		}

		, SendSession: function (request) {

			var timeout = 0;
			var callback = null;

			// If more than 2 arguments and last argument is a function
			if (arguments.length > 1) {

				if (typeof arguments[arguments.length - 1] === 'function') {
					callback = arguments[arguments.length - 1];
				if ((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
					timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
					timeout = arguments[arguments.length - 1];
		        }

        		SlimCD.CallWebService("SendSession", request, timeout, callback);
		}

		, SpawnSession: function (request) {

			var timeout = 0;
			var callback = null;

			// If more than 2 arguments and last argument is a function
			if (arguments.length > 1) {

				if (typeof arguments[arguments.length - 1] === 'function') {
					callback = arguments[arguments.length - 1];
				if ((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
					timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
					timeout = arguments[arguments.length - 1];
		        }

        		SlimCD.CallWebService("SpawnSession", request, timeout, callback);
		}

		, ShowSession: function (sessionid) {

			window.location.href = SlimCD.internal.getStatsServer() + "/soft/showsession.asp?sessionid=" +  encodeURIComponent(sessionid)
		}


		, MultiSession: function (sessionid) {

			if(arguments.length > 1 && typeof arguments[1] !== 'function' && arguments[1] !== null && arguments[1] != '' )
				window.location.href = SlimCD.internal.getStatsServer() + "/soft/multisession.asp?sessionid=" +  encodeURIComponent(sessionid) + "&amount=" +  encodeURIComponent(arguments[1])
			else
				window.location.href = SlimCD.internal.getStatsServer() + "/soft/multisession.asp?sessionid=" +  encodeURIComponent(sessionid)
		}

		, GetSessionFields : function (request) {

			var timeout = 0;
			var callback=null;

    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("GetSessionFields",request,timeout,callback) ;
		}

		, SearchSessions : function (request) {

			var timeout = 0;
			var callback=null;

    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}

			SlimCD.CallWebService("SearchSessions",request,timeout,callback) ;
		}

	}


	, "Validate" : {


		// Credit Card Validation Javascript
		// copyright 12th May 2003, by Stephen Chapman, Felgall Pty Ltd
		// You have permission to copy and use this javascript provided that
		// the content of the script is not changed in any way.

		isValidCardNumber : function (s) {

			// remove non-numerics
			var v = "0123456789";
			var w = "";
			var i,j,k,m,c ;
			for (i=0; i < s.length; i++) {
				x = s.charAt(i);
				if (v.indexOf(x,0) != -1)
				w += x;
			}
			// validate number
			j = w.length / 2;
			if (j < 6.5 || j > 8 || j == 7)
				return false;
			k = Math.floor(j);
			m = Math.ceil(j) - k;
			c = 0;

			for (i=0; i<k; i++) {
				a = w.charAt(i*2+m) * 2;
				c += a > 9 ? Math.floor(a/10 + a%10) : a;
			 }
			for (i=0; i<k+m; i++)
				c += w.charAt(i*2+1-m) * 1;

			return (c%10 == 0) ;

		}

		, isValidExpDate : function (m,y) {

			var d = new Date();
			var mon = d.getMonth()+1;
			var yr = d.getFullYear()%100;

			if( (y%100) > yr || (yr == (y%100) && m >= mon))
				return true ;
			return false ;
		}

		, isValidCVV2 : function (cvv2) {

			if( cvv2.length >=3 && cvv2.length <= 4)
				return true ;
			return false ;
		}


		, isValidPhoneNumber : function (s) {
			// remove non-numerics
			var v = "0123456789";
			var w = "";
			var i=0;
			for (i=0; i < s.length; i++) {
				x = s.charAt(i);
				if (v.indexOf(x,0) != -1)
					w += x;
			}
			if (w.length == 10 || (w.length==11 && w.charAt(0) == '1'))
				return true ;
			return false ;
		}


		, isValidZipCode : function (s) {

				// remove non-numerics
				var v = "0123456789";
				var w = "";
				var i=0 ;
				for (i=0; i < s.length; i++)
				 {
					x = s.charAt(i);
					if (v.indexOf(x,0) != -1)
						w += x;
				}

				if (w.length == 5 || w.length == 9)
					return true  ;

				return false ;
			}

		, isValidCardType : function (s,t) {

			// remove non-numerics
			var v = "0123456789";
			var w = "";
			var p2 = "";
			var p4 = "";
			var x = "" ;
			var i=0 ;

			for (i=0; i < s.length; i++) {
				x = s.charAt(i);
				if (v.indexOf(x,0) != -1)
					w += x;
			}

			if (t == "V") {
				if (w.length != 13 && w.length != 16 && w.length != 19)
					return false ;
				if (w.substr(0,1) != "4")
					return false ;
			}
			else if (t == "M") {
				if (w.length != 16)
					return false ;
				p2 = w.substr(0,2)
				p4 = w.substr(0,4)
				if (p2 != "51" && p2 != "52" && p2 != "53" && p2 != "54" && p2 != "55" && p2 != "22" && p2 != "23" && p2 != "24" && p2 != "25" && p2 != "26" && p2 != "27")
					return false ;

			}
			else if (t == "D") {
				if (w.length != 16 && w.length != 19)
					return false ;
				p2 = w.substr(0,2)
				p4 = w.substr(0,4)
				if (p4 != "6011" && p2 != "62" && p2 != "63" && p2 != "64" && p2 != "65")
					return false ;
			}
			else if (t == "A") {
				if (w.length != 15)
					return false ;
				p2 = w.substr(0,2)
				if (p2 != "34" && p2 != "37")
					return false ;
			}
			else if (t == "C") {
				if (w.length != 14 && w.length != 16)
					return false ;
				p2 = w.substr(0,2)
				if (p2 != "30" && p2 != "36" && p2 != "38" && p2 != "39")
					return false ;
			}
			else if (t == "J") {
				if (w.length != 16)
					return false ;
				p2 = w.substr(0,2)
				if (p2 != "35" && p2 != "30" && p2 != "31" && p2 != "33")
					return false ;
			}

			return true ;
		}

		, isValidEmail : function (str) {

			var at="@"
			var dot="."
			var lat=str.indexOf(at)
			var lstr=str.length
			var ldot=str.indexOf(dot)

			if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr)
				return(false) ;

			if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr)
				return(false) ;

			if (str.indexOf(at,(lat+1))!=-1)
				return(false) ;

			if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot)
				return(false) ;

			if (str.indexOf(dot,(lat+2))==-1)
				return(false) ;

			if (str.indexOf(" ")!=-1)
				return(false) ;

			return (true) ;
		}
	}

	, CallWebService: function (webservice, request) {

			var timeout = 0;
			var callback=null;

			// set the URL, and specify the callback (which is the name of our own JavaScript function above)
			SlimCD.internal.url = SlimCD.internal.getStatsServer() + '/soft/json/jsonscript.asp?callback=SlimCD.internal.getreply&service=' + webservice


    			// If more than 2 arguments and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}


			if (typeof(timeout) === "undefined" || timeout == 0)
				timeout = 90 ;

			if (typeof(callback) === "undefined")
				callback = null ;

			return(SlimCD.internal.CreateJSONP(SlimCD.internal.url,request,timeout,callback)) ;
         }

	, CallStatusTest : function (request) {

			var timeout = 0;
			var callback=null;

			// set the URL, and specify the callback (which is the name of our own JavaScript function above)
			SlimCD.internal.url = SlimCD.internal.getStatsServer() + '/soft/json/testcode.asp?callback=SlimCD.internal.getreply'


    			// If more than 1 argument and last argument is a function
			if(arguments.length > 1)  {

				if (typeof arguments[arguments.length - 1] === 'function') {
				  	callback = arguments[arguments.length - 1];
					if((arguments.length > 2) && (typeof arguments[arguments.length - 2] === 'number'))
						timeout = arguments[arguments.length - 2];
				}
				else if (typeof arguments[arguments.length - 1] === 'number')
				   timeout = arguments[arguments.length - 1];
			}


			if (typeof(timeout) === "undefined" || timeout == 0)
				timeout = 10 ;

			if (typeof(callback) === "undefined")
				callback = null ;

			return(SlimCD.internal.CreateJSONP(SlimCD.internal.url,request,timeout,callback)) ;
                }
	, internal :

		{

		"url" : ""

		,"stack" : 0

		,"requestdata" : [ "" ]

		,"senddata" : [ "" ]

		, "IsLoaded" : "0"

		,"IsExecuted" : "0"

		, standard_error_replyblock : function(error_text) {

			var reply = new Object();

 			reply.response="Error" ;
			reply.responsecode="2" ;
			reply.description= error_text ;
			reply.datablock = null;

			return(reply) ;
		}

		, callback : [ function(reply) {} ]

		, getTransServer : function() {

			   if(SlimCD.transURL == '')
				{
				    var scripts = document.getElementsByTagName('script');
				    for (var i = 0 ; i < scripts.length ; i++)
					{
					    	var myScriptStr = scripts[i].src.toLowerCase();

						var idx = myScriptStr.lastIndexOf("slimcd.js") ;

						// are we on slimcd.com and are we loading the slimcd.js?
						if(myScriptStr.indexOf("slimcd.com/") > 0 && idx >= 0)
						{

							var url = myScriptStr.substring(0,idx) ;
							var idx2 = url.indexOf('//') ;
							idx2 = Math.min(idx2+2,url.length) ;
							var url2 = url.substring(idx2) ;
							var idx3 = url2.indexOf('/') ;

							if(idx3 >= 0)
								SlimCD.transURL = url.substring(0,idx2+idx3) ;
							else
								SlimCD.transURL = "https://trans.slimcd.com";
							break ;
						}
					}
				}

				return(SlimCD.transURL);
			}

		, getStatsServer : function() {

			   if(SlimCD.statsURL == '')
				{
				    var scripts = document.getElementsByTagName('script');
				    for (var i = 0 ; i < scripts.length ; i++)
					{
					    	var myScriptStr = scripts[i].src.toLowerCase();

						var idx = myScriptStr.lastIndexOf("slimcd.js") ;

						// are we on slimcd.com and are we loading the slimcd.js?
						if(myScriptStr.indexOf("slimcd.com/") > 0 && idx >= 0)
						{

							var url = myScriptStr.substring(0,idx) ;
							var idx2 = url.indexOf('//') ;
							idx2 = Math.min(idx2+2,url.length) ;
							var url2 = url.substring(idx2) ;
							var idx3 = url2.indexOf('/') ;

							if(idx3 >= 0)
								SlimCD.statsURL = url.substring(0,idx2+idx3) ;
							else
								SlimCD.statsURL = "https://stat.slimcd.com";
							break ;
						}
					}
				}

				return(SlimCD.statsURL);
			}

		, generate_url_for_object : function(obj) {

			var str = '' ;

			if(obj != undefined && obj != null)
				for(var prop in obj)
				    if(obj.hasOwnProperty(prop))
					if(typeof(obj[prop]) == "object")
						str += SlimCD.internal.generate_url_for_object(obj[prop]) ;
					else
						str += SlimCD.internal.generate_urlencoded_field(prop,obj[prop]) ;
			return(str) ;
		}

		, CreateJSONP: function (url,request,timeout,callback) {

			// add the request NAME/VALUE pairs to the URL, with URL-encoding for the values

			if(SlimCD.debug === true && (JSON !== 'undefined' && typeof JSON.stringify === 'function'))
				SlimCD.internal.requestdata[SlimCD.internal.stack] = JSON.stringify(request,null,'\t') ;

			SlimCD.internal.senddata[SlimCD.internal.stack] = SlimCD.internal.generate_url_for_object(request) ;

			url += (SlimCD.internal.senddata[SlimCD.internal.stack] + '&slimcdjs_stack=' + SlimCD.internal.stack) ;

			if(SlimCD.debug !== true)
				SlimCD.internal.senddata[SlimCD.internal.stack] = '' ;

			// for debugging... if they put this on the page, fill it in!
			var debug_url = document.getElementById('slimcd_debugging_url') ;
			if(debug_url != null)
				debug_url.value=url ;

			SlimCD.internal.callback[SlimCD.internal.stack]=callback;

			SlimCD.internal.IsLoaded=0;

			SlimCD.internal.IsExecuted=0;

			// add the URL to the page so that it is called now.
			SlimCD.internal.JSONP(url,timeout);

			return(SlimCD);
		}

		, JSONP : function (url,timeoutvalue) {

				var label = 'SlimCD_JSONP_URL' + SlimCD.internal.stack ;

				// url - the url of the script where we send the asynchronous call

				// create a new script element in memory
				var script = document.createElement('script');

				// set the src attribute to the url, which includes the parameters to pass
				script.setAttribute('src', url);
				script.setAttribute('id', label );


				// set the timeout for 60 seconds.  Note that this should be different per type of operation
				SlimCD.internal.timeouthandler[SlimCD.internal.stack] = SlimCD.internal.gettimeouthandler(SlimCD.internal.stack);

				if (timeoutvalue > 0 && timeoutvalue < 500)
					SlimCD.internal.timeouthandler[SlimCD.internal.stack].timeoutvalue=timeoutvalue*1000 ;
				else
					SlimCD.internal.timeouthandler[SlimCD.internal.stack].timeoutvalue=timeoutvalue ;


				if(SlimCD.internal.timeouthandler[SlimCD.internal.stack].timeoutvalue == null || SlimCD.internal.timeouthandler[SlimCD.internal.stack].timeoutvalue == 0)
					SlimCD.internal.timeouthandler[SlimCD.internal.stack].timeout = 60000; // Default to 60 seconds

				SlimCD.internal.timeouthandler[SlimCD.internal.stack].stack = SlimCD.internal.stack ;
				SlimCD.internal.timeouthandler[SlimCD.internal.stack].timerid = setTimeout( SlimCD.internal.timeouthandler[SlimCD.internal.stack].timeoutCallback, SlimCD.internal.timeouthandler[SlimCD.internal.stack].timeoutvalue );

				// Set the onload and onreadystatechange
				//script.onload = SlimCD.internal.script_onloadhandler ;
				//script.onreadystatechange = SlimCD.internal.script_onreadystatechangehandler ;

				//script.onerror = SlimCD.internal.script_errorhandler ;

				// insert the script into the HEAD element of our page, so that it is called asynchronously
				document.getElementsByTagName('head')[0].appendChild(script);

				SlimCD.internal.stack++ ;

				return(SlimCD);
			}

		, script_onerrorhandler : function (msg,url,linenumber) {

				 	// We tried using this to catch cases where the JSONP throws an error.
					alert('An error occurred:' + msg + ' at line ' + linenumber + ' of ' + urls);
				}

		, script_onloadhandler : function () {
				 	// We tried using this to catch cases where the JSONP throws an error.
					//alert('onload called');
				}

		, script_onreadystatechangehandler : function () {
				 	// We tried using this to catch cases where the JSONP throws an error.
					//alert('onreadystate:' +  this.readyState);
				}

		, "timeouthandler" : [ ]


		, "gettimeouthandler" : function(stack) {

			// retunr an enture timeout block object and code, including the handler....
			return ( {

				"timeoutvalue" : 60000

				,"timerid" : null

				, timeoutCallback: function () {

					var stacknumber = stack ;

					var script = document.getElementById('SlimCD_JSOP_URL' + stacknumber  );

					SlimCD.reply.response = "ERROR";
					SlimCD.reply.responsecode = "2";
					SlimCD.reply.description = "TIMEOUT CALLING SCRIPT";
					SlimCD.reply.datablock = null;

					if(script != null)
						SlimCD.reply.responseurl = script.getAttribute('src');

					SlimCD.internal.cleanup(stacknumber);

					SlimCD.internal.CallCallBack(stacknumber,SlimCD.reply) ;
				}
			} ) ;
		}


		, generate_urlencoded_field: function (fieldname,value) {

			// Only generate fields that actually contain data to keep the URL as short as possible
			if (value != null && value != undefined) // && (fieldname.toLowerCase() == "password" || value != ''))
				str = '&' + fieldname + '=' + encodeURIComponent(value).replace('%20','+') ;
			else
				str = '' ;

			return(str);
		}

		, getreply : function (response) {


			if(typeof(response) !== 'undefined' && response !== 'null' && typeof(response.stack) !== 'undefined')
				SlimCD.internal.cleanup(response.stack);
			else
				SlimCD.internal.cleanup(0); // assume we only have one call!

			// update our reply object to point to the new one, then return it.

			/*if (SlimCD.Debug.enable === "yes" && (JSON !== 'undefined' && typeof JSON.stringify === 'function'))
				SlimCD.Debug.recvdata=JSON.stringify(response.reply,null,'\t');
			*/

			if(typeof(response) !== 'undefined' && response !== 'null' && typeof(response.reply) !== 'undefined') {

				if (SlimCD.debug === true) {
					if (typeof(JSON) !== 'undefined' && typeof (JSON.stringify) === 'function')
						if( typeof(response.reply) !== 'undefined') {
							// JSON request & reply
							response.reply.recvdata=JSON.stringify(response.reply,null,'\t');
							response.reply.requestdata=SlimCD.internal.requestdata[response.stack]
						}

					if( typeof(response.reply) !== 'undefined')
						response.reply.senddata=SlimCD.internal.senddata[response.stack]
				}
				SlimCD.reply = response.reply;
			}
			else
			{
				SlimCD.reply.response = "ERROR";
				SlimCD.reply.responsecode = "2";
				SlimCD.reply.description = "NO ReplyBlock from SCRIPT";
				SlimCD.reply.datablock = null;
			}

			SlimCD.internal.CallCallBack(response.stack,SlimCD.reply) ;
		}

		, CallCallBack : function(stack, reply) {

			if(typeof (SlimCD.internal.callback[stack]) !== 'undefined' && typeof(SlimCD.internal.callback[stack]) === 'function' && SlimCD.internal.callback[stack] != null)
				SlimCD.internal.callback[stack](reply);

			SlimCD.internal.callback[stack] = null;

			// Check to see if there are any more that can be popped now...
			if (stack == SlimCD.internal.stack-1)
			{
				SlimCD.internal.stack-- ;
				while(SlimCD.internal.stack > 0 && SlimCD.internal.callback[SlimCD.internal.stack-1] === null)
					SlimCD.internal.stack-- ;
			}

		}

		, cleanup : function(stack) {

			if(SlimCD.internal.timeouthandler[stack].timerid != null)
				clearTimeout(SlimCD.internal.timeouthandler[stack].timerid);

			SlimCD.internal.timeouthandler[stack].timerid = null ;

			var script = document.getElementById('SlimCD_JSOP_URL' + stack);

			if(script != null) {
				// clean up IE memory leak (per stack overflow)
				//script.onload = script.onreadystatechange = script.onerror =  null;
				document.getElementsByTagName('head')[0].removeChild(script);
			}
		}

	}

};

}) () ;