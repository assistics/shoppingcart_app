/* This is the one it uses.  Not the one in ast_web !!! */
function callAliasForTokenACH(aURL, aPublishedKey, aAccountType, anAccountName, anAccountNumber, aRoutingNumber, aReply) {
fetch(aURL, {
    method: 'POST',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'aw-publishedkey': aPublishedKey
    },
    body: JSON.stringify({
	"tv3.tokenRequest": {
		"fundingAccount": {
			"@xsi.type": "tv3:achAccount",
			"number": anAccountNumber,
			"name": anAccountName,
			"bankRoutingNumber": aRoutingNumber,
			"bankAccountType": aAccountType/*,
			"bankName": "Watertown"*/
		}
	}
})
})
   //.then(response => console.log(JSON.stringify(response)))

   .then(response => response.json())
   .then(response => JSON.stringify(response))
   .then(response => aReply(response))
/*
.then(function (response){
var aJSON = response.json();
var aStringify = JSON.stringify(aJSON);
return aReply(aStringify);
})*/
}