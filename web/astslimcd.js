var slimcdResult;
function getSlimcdResult() {
	return slimcdResult;
}

async function getToken(username, cvv2, cardnumber, anExpMonth, anExpYear) {
  var myp = new Promise((resolve, reject) => {
 SlimCD.Transact.GetTemporaryToken({
					"username": username,
					"cvv2": cvv2,
					"cardnumber": cardnumber,
					"expmonth" : anExpMonth,
					"expyear" : anExpYear
				},
				function (reply) {
					slimcdResult = JSON.stringify(reply);
					resolve(reply);
				}
				);
	});
	return myp;
}
