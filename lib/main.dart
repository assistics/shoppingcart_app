// @dart>=2.12.0
import 'dart:async';

import 'package:ast_app/com/adk/ics/business/salesordersession.dart';
import 'package:ast_app/com/adk/utility/GlobalConfiguration.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
//import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:flutter_timezone/flutter_timezone.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shoppingcart_app/com/adk/config/routes.dart';
import 'package:shoppingcart_app/com/adk/sc/applocalizations.dart';
import 'package:shoppingcart_app/com/adk/sc/cartpage.dart';
import 'package:shoppingcart_app/com/adk/sc/chooseproductpage.dart';
import 'package:shoppingcart_app/com/adk/sc/loginpage.dart';
import 'package:shoppingcart_app/com/adk/sc/openorderspage.dart';
import 'package:shoppingcart_app/com/adk/sc/orderpage.dart';
import 'package:shoppingcart_app/com/adk/sc/paymentmethodspage.dart';
import 'package:shoppingcart_app/com/adk/sc/sidemenu.dart';
import 'package:shoppingcart_app/com/adk/sc/weekopenorderspage.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart';
import 'package:ast_app/com/adk/ics/business/icsuser.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:logger/logger.dart';

void main() async {
  Logger.level = Level.info;
  WidgetsFlutterBinding.ensureInitialized();
  await GlobalConfiguration().loadFromAsset("app_settings.json");

  String timezone;
  //ByteData tzf = await rootBundle.load('assets/2021e.tzf');
  String aBundleName = 'assets/packages/timezone/data/2021e.tzf';
  ByteData tzf = await rootBundle.load(aBundleName);
  //ByteData tzf = await rootBundle.load('assets/packages/timezone/data/2021e.tzf');
  //ByteData tzf = await rootBundle.load('2021e.tzf');
  initializeDatabase(tzf.buffer.asUint8List());
  tz.initializeTimeZones();

  final String timeZoneName = await FlutterTimezone.getLocalTimezone();
  ADKGlobal.setDefaultLocalTimeZoneName(timeZoneName);
  //Logger.log('Timezone: $timeZoneName', className: '$AppConfig');
  //browsertz.setLocalLocation(browsertz.getLocation(timeZoneName));
  ADKGlobal.initTimeZone();

  runApp(MyAppSC());
}

class MyAppSC extends StatefulWidget {
  @override
  _MyAppSCState createState() => _MyAppSCState();
}

class _MyAppSCState extends State<MyAppSC> {
  final _navigatorKey = GlobalKey<NavigatorState>();
  static Timer? _timer;

  @override
  void initState() {
    _startTimer();
    super.initState();
  }

  void _startTimer() {
    if (_timer != null) {
      Timer? aTimer = _timer;
      if (aTimer != null) {
        aTimer.cancel();
      }
    }

//Sets the timer to 300 seconds, after which the callback logs the user out
    _timer = Timer(const Duration(seconds: 1800), () {
      _timer?.cancel();
      _timer = null;

      ICSUser aUser = UserData.getUser();
      if (aUser.isPersistent()) {
        UserData.logout();
        Route aRoute = MaterialPageRoute(
          builder: (context) => MyAppSC(),
        );
        NavigatorState? aState = _navigatorKey.currentState;
        if (aState != null) {
          aState.pushReplacement(aRoute);
        }
      } else {
        this._startTimer();
      }
      //Navigator.popUntil(context, ModalRoute.withName("/"));
      //_navigatorKey.currentState.popUntil(ModalRoute.withName("/"));
      //Navigator.of(context, rootNavigator: true).pop(context);
      //Navigator.
    });
  }

  void _handleInteraction([_]) {
    _startTimer();
  }

  @override
  Widget build(BuildContext context) {
    CardTheme aCardTheme;
    ThemeData aTheme;
    if (ADKGlobal.isDesktop()) {
      ElevatedButtonThemeData aButtonTheme = ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
              foregroundColor: Colors.black,
              textStyle: TextStyle(
                fontSize: 20,
              ),
              backgroundColor: Colors.green[50],
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50),
                side: BorderSide.none,
              ),
              elevation: 5.0,
              padding: EdgeInsets.all(20)));
      aCardTheme = CardTheme(color: Colors.white, elevation: 3);
      aTheme = ThemeData(
          useMaterial3: false,
          elevatedButtonTheme: aButtonTheme,
          scaffoldBackgroundColor: Colors.white70,
          cardTheme: aCardTheme,
          appBarTheme: Theme.of(context).appBarTheme.copyWith(
                color: Colors.white,
              ),
          textTheme: GoogleFonts.robotoTextTheme(
            Theme.of(context).textTheme,
          ));
    } else {
      ElevatedButtonThemeData aButtonTheme = ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
              textStyle: TextStyle(
                fontSize: 20,
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50),
                side: BorderSide.none,
              ),
              elevation: 5.0,
              //hoverColor: Colors.lightGreen,
              backgroundColor: Colors.yellow,
              foregroundColor: Colors.black,
              padding: EdgeInsets.all(20)));
      aCardTheme = CardTheme(
        color: Colors.lightBlue[50],
      );
      aTheme = ThemeData(
          useMaterial3: false,
          appBarTheme: Theme.of(context).appBarTheme.copyWith(
                color: Colors.white,
              ),
          elevatedButtonTheme: aButtonTheme,
          cardTheme: aCardTheme,
          fontFamily: 'Arial');
    }
    void Function() aRefreshFunction = () {
      //Empty function as this is not used
    };

    Widget aMaterialApp = MaterialApp(
        navigatorKey: _navigatorKey,
        supportedLocales: [
          Locale('en', 'US'),
          Locale('es', ''),
        ],
        localizationsDelegates: [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        home: LoginPage(),
        title: "Shopping Cart",
        theme: aTheme,
        routes: <String, WidgetBuilder>{
          //'/': (context) => LoginPage(),
          ADKSCRoutes.PAGE_LOGIN: (BuildContext context) => new LoginPage(),
          ADKSCRoutes.PAGE_OPENORDERSPAGE: (BuildContext context) =>
              new OpenOrdersPage(aRefreshFunction),
          ADKSCRoutes.PAGE_MENUPAGE: (BuildContext context) => new MainPage(''),
          ADKSCRoutes.PAGE_WEEKORDERSPAGE: (BuildContext context) =>
              new WeekOpenOrdersPage(aRefreshFunction),
          ADKSCRoutes.PAGE_CHOOSEPRODUCTS: (BuildContext context) =>
              new ChooseProductScreen(aRefreshFunction),
          ADKSCRoutes.PAGE_CART: (BuildContext context) =>
              new CartPage(aRefreshFunction),
          ADKSCRoutes.PAGE_PAYMENTMETHODS: (BuildContext context) =>
              new PaymentMethodsPage(),
          ADKSCRoutes.PAGE_ORDER: (BuildContext context) =>
              new OrderPage(SalesOrderSession(), false, aRefreshFunction),
        });

    Widget aMain = GestureDetector(
      behavior: HitTestBehavior.translucent,
      //onPointerMove: _handleInteraction,
      onTap: _handleInteraction,
      onPanDown: _handleInteraction,
      child: aMaterialApp,
    );

    return aMain;
  }
}
