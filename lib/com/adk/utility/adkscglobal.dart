import 'package:ast_app/com/adk/ics/business/cbsi.dart';
import 'package:ast_app/com/adk/ics/business/salesorderlinesession.dart';
import 'package:ast_app/com/adk/ics/business/salesordersession.dart';
import 'package:ast_app/com/adk/utility/adkdecimal.dart';
import 'package:ast_app/com/adk/utility/environmentlocale.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';

class ADKSCGlobal {
  static bool scAllowPayments() {
    List<CBSI> aList = UserData.getUser().getCBSArray();
    if (aList.isEmpty) {
      return false;
    }
    CBSI aCBS = aList.first;

    return aCBS.isManualPaymentEnabled();
  }

  static bool scIsPaymentMethodEnabled() {
    List<CBSI> aList = UserData.getUser().getCBSArray();
    if (aList.isEmpty) {
      return false;
    }
    CBSI aCBS = aList.first;

    return aCBS.isPaymentMethodEnabled();
  }



  static ADKDecimal getSCConvenienceFeePercentage() {
    List<CBSI> aList = UserData.getUser().getCBSArray();
    if (aList.isEmpty) {
      return ADKDecimal.fromString('3');
    }
    CBSI aCBS = aList.first;
    ADKDecimal? aValue = aCBS.getConvenienceFeePercent();
    if (aValue == null) {
      return ADKDecimal.fromString('3');
    }
    return aValue;
  }


  static scDisplayPricePerUOM(EnvironmentLocale aLocale,
      ADKDecimal? aAdjustedUnitPrice, String aUOMKey) {
    if (ADKDecimal.le(aAdjustedUnitPrice, ADKDecimal.zero())) {
      return aLocale.translate("Price Pending");
    } else {
      return UserData.getLocale().displayUnitMonetaryData(aAdjustedUnitPrice) +
          '/' +
          aUOMKey;
    }
  }

  static String scDisplayTotalPrice(
      EnvironmentLocale aLocale, SalesOrderSession aSalesOrder) {
    bool isDirty = aSalesOrder.isDirty();
    ADKDecimal? aTotalPrice;
    ADKDecimal? aTotalPriceWithoutSurcharge;
    ADKDecimal? aSurcharge;
    ADKDecimal? aMinAmount;
    aMinAmount = aSalesOrder.getEntityMinimumOrderAmount();
    if (aMinAmount == null) {
      aMinAmount = ADKDecimal.zero();
    }
    aTotalPrice = aSalesOrder.calculateTotalPrice();
    String aText;
    if (ADKDecimal.gt(aMinAmount, ADKDecimal.zero())) {
      aTotalPriceWithoutSurcharge =
          aSalesOrder.calculateTotalPriceWithoutSurcharge();
      if (aSalesOrder.isPersistent() && isDirty) {
        //Recalculate totals and surcharge
        if (ADKDecimal.gt(aMinAmount, aTotalPriceWithoutSurcharge)) {
          //If below min, recalc
          aTotalPrice = aMinAmount;
          aSurcharge =
              ADKDecimal.subtract(aTotalPrice, aTotalPriceWithoutSurcharge);
        } else {
          aTotalPrice = aTotalPriceWithoutSurcharge;
        }
      } else if (aSalesOrder.isPersistent()) {
        aSurcharge = aSalesOrder.calculateBelowOrderMinimumSurcharge();
      } else {
        aSurcharge =
            ADKDecimal.subtract(aMinAmount, aTotalPriceWithoutSurcharge);
      }
      aText = UserData.getLocale().displayMonetaryData(aTotalPrice);

      if (ADKDecimal.gt(aMinAmount, aTotalPriceWithoutSurcharge)) {
        if (ADKDecimal.gt(aSurcharge, ADKDecimal.zero()) &&
            aSalesOrder.isPersistent()) {
          aText = aText +
              " (Including " +
              UserData.getLocale().displayMonetaryData(aSurcharge) +
              " surcharge)";
        } else {
          aText = aText +
              " (Minimum order " +
              UserData.getLocale().displayMonetaryData(aMinAmount) +
              ")";
        }
      }
    } else {
      aText = UserData.getLocale().displayMonetaryData(aTotalPrice);
    }

    return aText;
  }

  static String scDisplayTotalLinePrice(
      EnvironmentLocale aLocale, SalesOrderLineSession aLine) {
    ADKDecimal aTotalPrice = ADKDecimal.zero();
    String aText =
        UserData.getLocale().displayMonetaryData(aLine.getAmountExtended());

    return aText;
  }
}
