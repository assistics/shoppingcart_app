import 'package:ast_app/com/adk/ics/business/addressi.dart';
import 'package:ast_app/com/adk/ics/business/cart.dart';
import 'package:ast_app/com/adk/ics/business/salesordersession.dart';
import 'package:ast_app/com/adk/utility/adkdatetime.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkmiscstringfunctions.dart';
import 'package:ast_app/com/adk/utility/environmentlocale.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/chooseproductpage.dart';
import 'package:shoppingcart_app/com/adk/sc/orderpage.dart';
import 'package:shoppingcart_app/com/adk/utility/adkscglobal.dart';

abstract class OrderTile extends StatelessWidget {
  late SalesOrderSession salesOrderSession;
  bool isLoading = false;
  bool isEdit = false;
  late VoidCallback actionRefresh;

  OrderTile(
      {required this.salesOrderSession,
      required isEdit,
      required this.actionRefresh,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return orderTile(context, this.salesOrderSession);
  }

  Widget orderTile(BuildContext context, SalesOrderSession aSalesOrder);

  bool isListOrderMode() {
    return false;
  }

  Widget buildTileTitle(BuildContext context, SalesOrderSession aSalesOrder) {
    if (ADKGlobal.isWideScreen(context)) {
      return this.buildTileTitleWide(context, aSalesOrder);
    }
    List<Row> lines = [];
    if (aSalesOrder.getCustomerPO().isNotEmpty) {
      lines.add(Row(children: [
        new Text("Purchase Order: "),
        new Text(aSalesOrder.getCustomerPO(),
            style: TextStyle(fontWeight: FontWeight.bold)),
      ]));
    }
    if (aSalesOrder.getSalesOrderKey().isNotEmpty) {
      lines.add(Row(children: [
        new Text("Sales Order: "),
        new Text(aSalesOrder.getSalesOrderKey(),
            style: TextStyle(fontWeight: FontWeight.bold)),
      ]));
    }

    AddressI anAddress = aSalesOrder.getShipToAddress();
    String anAddressLine = "";
    String? anAddressLine1 = anAddress.getAddressLine1();
    String? anAddressCity = anAddress.getCity();
    String? anAddressState = anAddress.getState();
    String? anAddressPostalCode = anAddress.getPostalCode();
    if ((anAddressLine1.isNotEmpty ||
            anAddressCity.isNotEmpty ||
            anAddressState.isNotEmpty ||
            anAddressPostalCode.isNotEmpty)) {
      anAddressLine = /*anAddressLine1 +
          ", " +*/
          anAddressCity + ", " + anAddressState + " " + anAddressPostalCode;
    }

    ADKDateTime? aDeliveryDate = aSalesOrder.getDeliveryDate();
    if (aDeliveryDate != null) {
      lines.add(Row(children: [
        //new Text("Delivery: "),
        new Text(
          UserData.getLocale().displayDateTimeFormat(
              aDeliveryDate, EnvironmentLocale.DAYMONTHYYYY),
          style: TextStyle(fontWeight: FontWeight.bold),
        )
      ]));
    }
    String aShipToString = aSalesOrder.getShipToKey();
    if (aSalesOrder.getShipToName().isNotEmpty) {
      aShipToString = aShipToString + ' ' + aSalesOrder.getShipToName();
    } else if (anAddress.getAddressLine1().isNotEmpty) {
      aShipToString = aShipToString + ' ' + anAddress.getAddressLine1();
    }

    lines.add(Row(children: [new Text(aShipToString)]));

    lines.add(Row(children: [new Text(anAddressLine)]));

    return Column(children: lines);
  }

  Widget buildTileTitleWide(
      BuildContext context, SalesOrderSession aSalesOrder) {
    EnvironmentLocale aLocale = UserData.getLocale();
    ADKDateTime? aDeliveryDate = aSalesOrder.getDeliveryDate();
    Widget aDeliveryDateWidget;
    if (aDeliveryDate != null) {
      aDeliveryDateWidget = Wrap(children: [
        Text('Delivery Date: ', style: TextStyle(fontWeight: FontWeight.bold)),
        Text(
          aLocale.displayDateTimeFormat(
              aDeliveryDate, EnvironmentLocale.DAYMONTHYYYY),
        ),
      ]);
    } else {
      aDeliveryDateWidget = Wrap(children: [
        Text('Delivery Date: ', style: TextStyle(fontWeight: FontWeight.bold)),
        Text(
          '',
        ),
      ]);
    }
    AddressI anAddress = aSalesOrder.getShipToAddress();
    String anAddressLine1 = anAddress.getAddressLine1();
    if (anAddressLine1.trim().isEmpty) {
      anAddressLine1 = anAddress.getAddressLine2();
    }
    String anAddressLine = "";
    String? anAddressCity = anAddress.getCity();
    String? anAddressState = anAddress.getState();
    String? anAddressPostalCode = anAddress.getPostalCode();
    if ((anAddressCity.isNotEmpty ||
            anAddressState.isNotEmpty ||
            anAddressPostalCode.isNotEmpty)) {
      anAddressLine =
          // ", " +
          anAddressCity + ", " + anAddressState + " " + anAddressPostalCode;
    }
    Widget aPriceWidget;
    Widget aSpace = Text('');
    if (ADKGlobal.scShowPrices()) {
      aPriceWidget = Text(
          'Total: ' + ADKSCGlobal.scDisplayTotalPrice(aLocale, aSalesOrder));
    } else {
      aPriceWidget = aSpace;
    }
    Map<int, TableColumnWidth> aMap = <int, TableColumnWidth>{
      //0: IntrinsicColumnWidth(),
      //1: FlexColumnWidth(),
      0: IntrinsicColumnWidth(),
      1: IntrinsicColumnWidth(),
      2: IntrinsicColumnWidth(),
    };
    Widget x1y1 = aDeliveryDateWidget;
    Widget x1y2 = Wrap(children: [
      Text('Purchase Order: ', style: TextStyle(fontWeight: FontWeight.bold)),
      Text(
        aSalesOrder.getCustomerPO(),
      ),
    ]);
    Widget x1y3;
    String aSalesOrderKey = aSalesOrder.getSalesOrderKey();
    if (ADKMiscStringFunctions.isBlankOrWhitespace(aSalesOrderKey)) {
      x1y3 = aSpace;
    } else {
      x1y3 = Wrap(children: [
        Text('Sales Order: ', style: TextStyle(fontWeight: FontWeight.bold)),
        Text(
          aSalesOrderKey,
        ),
      ]);
    }
    Widget x1y4;
    ADKDateTime? aSalesOrderCreatedDate =
        aSalesOrder.getSalesOrderCreatedDate();
    if (aSalesOrderCreatedDate == null) {
      x1y4 = aSpace;
    } else {
      x1y4 = Wrap(children: [
        Text('Created: ', style: TextStyle(fontWeight: FontWeight.bold)),
        Text(
          aLocale.displayDateTime(aSalesOrderCreatedDate),
        ),
      ]);
    }
    Widget x2y1 = Wrap(children: [
      Text('Account #: ', style: TextStyle(fontWeight: FontWeight.bold)),
      Text(
        aSalesOrder.getShipToKey(),
      ),
    ]);
    Widget x2y2 = Text(aSalesOrder.getShipToName());
    Widget x2y3 = Text(anAddressLine1);
    Widget x2y4 = Text(anAddressLine);
    Widget x3y1 = aPriceWidget;
    Widget x3y2 = aSpace;
    Widget x3y3;
    if (this.isEdit) {
      x3y3 = TextButton(
        child: Text('Add Product'),
        onPressed: () {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => ChooseProductScreen(this.actionRefresh)));
        },
      );
    } else if (this.isListOrderMode()) {
      x3y3 = aSpace;
    } else {
      x3y3 = TextButton(
        child: Text('Edit Order'),
        onPressed: () {
          bool isEditOrderEnabled = this.salesOrderSession.isChangeable();
          if (!isEditOrderEnabled) {
            if (this.salesOrderSession.isCBSOnHold()) {
              ADKDialog2.showAlertDialogText(
                  context, ADKGlobal.scContactCustomerService());
            } else {
              ADKDialog2.showAlertDialogText(
                  context, 'This order cannot be changed at this time.');
            }
          } else {
            Cart aCart = UserData.getUser().getCart();
            if (aCart.isCartInProgress()) {
              ADKDialog2.showAlertDialogText(context,
                  'An existing order cannot be changed while a cart is in progress.');
            } else {
              aCart.resetCart();
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => OrderPage(
                      this.salesOrderSession, true, this.actionRefresh)));
            }
          }
        },
      );
    }

    Widget x3y4 = aSpace;
    return Table(
      //border: TableBorder.all(),
      //border:TableBorder.lerp(0,0,0,0),
      columnWidths: aMap,
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: <TableRow>[
        TableRow(
          children: <Widget>[x1y1, x2y1, x3y1],
        ),
        TableRow(
          children: <Widget>[x1y2, x2y2, x3y2],
        ),
        TableRow(
          children: <Widget>[x1y3, x2y3, x3y3],
        ),
        TableRow(
          children: <Widget>[x1y4, x2y4, x3y4],
        ),
      ],
    );
  }

  Widget buildTileTitleWideX(
      BuildContext context, SalesOrderSession aSalesOrder) {
    List<Widget> aMainRow = List.empty(growable: true);
    List<Row> aFirstColumnLines = [];
    if (aSalesOrder.getCustomerPO().isNotEmpty) {
      aFirstColumnLines.add(Row(children: [
        new Text("Purchase Order: "),
        new Text(aSalesOrder.getCustomerPO(),
            style: TextStyle(fontWeight: FontWeight.bold)),
      ]));
    }

    //if (aSalesOrder.getSalesOrderKey().isNotEmpty) {
    List<Widget> aRowWidgets = List.empty(growable: true);
    aRowWidgets.add(Row(children: [
      new Text("Sales Order: " + aSalesOrder.getSalesOrderKey(),
          style: TextStyle(fontWeight: FontWeight.bold)),
    ]));
    aRowWidgets.add(Row(children: [
      new Text('Account #: ' + aSalesOrder.getShipToKey(),
          style: TextStyle(fontWeight: FontWeight.bold)),
    ]));
    Row aRow = Row(children: aRowWidgets);
    //}
    ADKDateTime? aDeliveryDate = aSalesOrder.getDeliveryDate();
    if (aDeliveryDate != null) {
      aFirstColumnLines.add(Row(children: [
        //new Text("Delivery: "),
        new Text(
          UserData.getLocale().displayDateTimeFormat(
              aDeliveryDate, EnvironmentLocale.DAYMONTHYYYY),
          style: TextStyle(fontWeight: FontWeight.bold),
        )
      ]));
    }
    Column aFirstColumn = Column(
      children: aFirstColumnLines,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    );
    aMainRow.add(aFirstColumn);

    List<Row> aSecondColumnLines = [];
    aSecondColumnLines.add(Row(children: [
      new Text('Account #: '),
      new Text(aSalesOrder.getShipToKey(),
          style: TextStyle(fontWeight: FontWeight.bold)),
    ]));

    aSecondColumnLines.add(Row(children: [
      new Text(aSalesOrder.getShipToName()),
    ]));

    AddressI anAddress = aSalesOrder.getShipToAddress();
    String anAddressLine = "";
    String? anAddressLine1 = anAddress.getAddressLine1();
    String? anAddressCity = anAddress.getCity();
    String? anAddressState = anAddress.getState();
    String? anAddressPostalCode = anAddress.getPostalCode();
    if ((anAddressLine1.isNotEmpty ||
            anAddressCity.isNotEmpty ||
            anAddressState.isNotEmpty ||
            anAddressPostalCode.isNotEmpty)) {
      anAddressLine = /*anAddressLine1 +
          ", " +*/
          anAddressCity + ", " + anAddressState + " " + anAddressPostalCode;
    }

    String aShipToString = aSalesOrder.getShipToKey();
    if (aSalesOrder.getShipToName().isNotEmpty) {
      aShipToString = aShipToString + ' ' + aSalesOrder.getShipToName();
    } else if (anAddress.getAddressLine1().isNotEmpty) {
      aShipToString = aShipToString + ' ' + anAddress.getAddressLine1();
    }

    aSecondColumnLines.add(Row(children: [new Text(aShipToString)]));
    aSecondColumnLines.add(Row(children: [new Text(anAddressLine)]));

    Column aSecondColumn = Column(
      children: aSecondColumnLines,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
    );
    aMainRow.add(aSecondColumn);

    //Row aColumns = Row();
    Row aColumns = Row(
      children: aMainRow, //crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.max,
    );

    //return Expanded(child: aColumns);
    return aColumns;
  }
}
