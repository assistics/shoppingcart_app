import 'package:ast_app/com/adk/ics/business/invoicesession.dart';
import 'package:ast_app/com/adk/utility/adkdatetime.dart';
import 'package:ast_app/com/adk/utility/adkdecimal.dart';
import 'package:ast_app/com/adk/utility/adkmiscdatefunctions.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';

abstract class InvoiceTile extends StatelessWidget {
  final InvoiceSession invoiceSession;
  bool isLoading = false;
  bool isEdit = false;

  InvoiceTile(this.invoiceSession, this.isEdit, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return invoiceTile(context, this.invoiceSession);
  }

  Widget invoiceTile(BuildContext context, InvoiceSession aSalesOrder);

  static List<Widget> buildTileTitle(InvoiceSession anInvoiceSession) {
    List<Widget> lines = [];

    if (anInvoiceSession.isDocumentTypeInvoice() ||
        anInvoiceSession.isDocumentTypeMiscInvoice()) {
      String aDocumentKey = anInvoiceSession.getInvoiceKey();
      lines.add(Row(children: [new Text('Invoice: ' + aDocumentKey)]));
    } else if (anInvoiceSession.isDocumentTypeCreditMemo()) {
      String aDocumentKey = anInvoiceSession.getInvoiceKey();
      lines.add(Row(children: [new Text('Credit Memo: ' + aDocumentKey)]));
    } else if (anInvoiceSession.isDocumentTypeDebitMemo()) {
      String aDocumentKey = anInvoiceSession.getInvoiceKey();
      lines.add(Row(children: [new Text('Debit Memo: ' + aDocumentKey)]));
    }

    String aCustomerPO = anInvoiceSession.getCustomerPO();
    lines.add(Row(children: [new Text(aCustomerPO)]));
    String aShipToName = anInvoiceSession.getShipToName();
    //lines.add(Row(children: [new Text(aShipToName)]));

    String aShipToString = anInvoiceSession.getShipToKey();
    lines.add(Row(children: [new Text(aShipToString + ' - ' + aShipToName)]));

    ADKDateTime? aDueDate = anInvoiceSession.getDueDate();
    if (aDueDate != null) {
      aDueDate = ADKMiscDateFunctions.truncateTime(aDueDate);
      lines.add(Row(children: [
        new Text("Due: " + UserData.getLocale().displayDateTime(aDueDate))
      ]));
    }

    ADKDateTime? aTransactionDate = anInvoiceSession.getTransactionDate();
    if (aTransactionDate != null) {
      aTransactionDate = ADKMiscDateFunctions.truncateTime(aTransactionDate);
      lines.add(Row(children: [
        new Text(UserData.getLocale().displayDateTime(aTransactionDate))
      ]));
    }

    ADKDecimal? anAmount = anInvoiceSession.getOpenAmount();
    if (anAmount == null) {
      anAmount = ADKDecimal.zero();
    }
    if (anInvoiceSession.isDocumentTypeCreditMemo()) {
      ADKDecimal aCreditMemoAmt =
          ADKDecimal.multiply(anAmount, ADKDecimal.fromString("-1"));
      lines.add(Row(children: [
        new Text(
          UserData.getLocale().displayMonetaryData(aCreditMemoAmt),
          style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold),
        )
      ]));
    } else {
      lines.add(Row(children: [
        new Text(UserData.getLocale().displayMonetaryData(anAmount),
            style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold))
      ]));
    }
    return lines;
  }
}
