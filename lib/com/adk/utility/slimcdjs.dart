import 'dart:async';

import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/creditcard.dart';
import 'package:ast_app/com/adk/utility/creditcardresponse.dart';

class SlimcdJS {
  Future<CreditCardResponse> retrieveSLIMCDToken(CreditCard aCreditCard) async {
    return new ADKURLBuilder().retrieveSLIMCDTokenHTTP(aCreditCard);
  }
}
