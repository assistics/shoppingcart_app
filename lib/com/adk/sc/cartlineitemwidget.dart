import 'package:ast_app/com/adk/ics/business/cart.dart';
import 'package:ast_app/com/adk/ics/business/cartlineitemi.dart';
import 'package:ast_app/com/adk/ics/business/productsession.dart';
import 'package:ast_app/com/adk/ics/business/salesorderlinesession.dart';
import 'package:ast_app/com/adk/utility/adkdecimal.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkmiscfilefunctions.dart';
import 'package:ast_app/com/adk/utility/adkmiscstringfunctions.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shoppingcart_app/com/adk/sc/shadow_style.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/adktheme.dart';
import 'package:shoppingcart_app/com/adk/sc/chooseproductpage.dart';
import 'package:shoppingcart_app/com/adk/utility/adkscglobal.dart';

class CartLineItemWidget extends StatefulWidget {
  CartLineItemI item = new ProductSession();
  late void Function() removeFunction;
  late void Function() addFunction;
  late VoidCallback actionRefresh;
  bool forOrderInquiry = false;
  bool forOrderEdit = false;
  late bool forChooseProduct;

  CartLineItemWidget(
      {required Key key,
      required bool aForOrderInquiry,
      required bool aForOrderEdit,
      required CartLineItemI anItem,
      required void Function() aRemoveFunction,
      required void Function() anAddFunction,
      required VoidCallback anActionRefresh,
      required bool aForChooseProduct})
      : super(key: key) {
    this.item = anItem;
    this.forOrderInquiry = aForOrderInquiry;
    this.forOrderEdit = aForOrderEdit;
    this.removeFunction = aRemoveFunction;
    this.addFunction = anAddFunction;
    this.actionRefresh = anActionRefresh;
    this.forChooseProduct = aForChooseProduct;
  }

  @override
  _CartLineItemWidgetState createState() => _CartLineItemWidgetState();
}

class _CartLineItemWidgetState extends State<CartLineItemWidget> {
  double _plusMinusIconSize = 30;

  //late void Function() removeFunction;
  //late void Function() addFunction;
  //late VoidCallback actionRefresh;
  //bool forOrderInquiry = false;
  //bool forOrderEdit = false;

  _CartLineItemWidgetState() {}
  FocusNode _focusNode = FocusNode();
  TextEditingController _controller = TextEditingController();

  void initState() {
    super.initState();

    var aProduct = widget.item;
    String aValue;
    if (widget.forOrderEdit) {
      ADKDecimal? aQty = aProduct.getOrderQuantity();
      ADKDecimal anOrderQty;
      if (aQty == null) {
        anOrderQty = ADKDecimal.zero();
      } else {
        anOrderQty = aQty;
      }
      aValue = anOrderQty.toDecimal().toString();
    } else {
      aValue = this.getCart().productItemCount(aProduct).toDecimal().toString();
    }
    this._controller = TextEditingController(text: aValue);
    //_focusNode = new FocusNode();
    _focusNode.addListener(this._onFocusChange);
  }

  void _onFocusChange() {
    var aProduct = widget.item;
    final newText = _controller.text.toLowerCase();
    _controller.value = _controller.value.copyWith(
      text: newText,
      selection: TextSelection(
          baseOffset: newText.length, extentOffset: newText.length),
      composing: TextRange.empty,
    );
    //String newText = _controller.text;
    if (_focusNode.hasFocus) {
      _controller.selection = TextSelection(
          baseOffset: 0, extentOffset: _controller.value.text.length);
    }
    if (!_focusNode.hasFocus) {
      if (!ADKMiscStringFunctions.isAllDigits(newText)) {
        SalesOrderLineSession? anObject =
            this.getCart().getLineInCart(aProduct);
        if (anObject == null) {
          _controller.text = '0';
        } else {
          ADKDecimal? aQuantity = anObject.getOrderQuantity();
          if (aQuantity != null) {
            _controller.text = aQuantity.toDecimal().toString();
          } else {
            _controller.text = '0';
          }
        }
      } else {
        ADKDecimal aNumber = ADKDecimal.fromString(newText);
        SalesOrderLineSession? anObject =
            this.getCart().getLineInCart(aProduct);
        if ((anObject != null &&
                ADKDecimal.ne(aNumber, anObject.getOrderQuantity())) ||
            (anObject == null && ADKDecimal.gt(aNumber, ADKDecimal.zero()))) {
          if (aProduct.hasIncrementalQuantities()) {
            ADKDecimal? anIncrementQtyBelowThreshold =
                aProduct.getIncrementQtyBelowThreshold();
            ADKDecimal? anIncrementQtyAboveThreshold =
                aProduct.getIncrementQtyAboveThreshold();
            ADKDecimal aThresholdQty = ADKDecimal.zero();
            ADKDecimal? aNullableQty = aProduct.getThresholdQty();
            if (aNullableQty != null) {
              aThresholdQty = aNullableQty;
            }
            if (anIncrementQtyBelowThreshold != null &&
                ADKDecimal.lt(aNumber, aThresholdQty)) {
              /*
                double x = aNumber.toDecimal().toDouble() /
                    anIncrementQtyBelowThreshold.toDecimal().toDouble();
                double aRoundedDown = x.round() *
                    anIncrementQtyBelowThreshold.toDecimal().toDouble();
                if (aRoundedDown < aNumber.toDecimal().toDouble()) {
                  aRoundedDown = aRoundedDown +
                      anIncrementQtyBelowThreshold.toDecimal().toDouble();
                }
                */
              ADKDecimal aX = ADKDecimal.roundUpToIncrementQty(
                  anIncrementQtyBelowThreshold, aNumber);
              if (ADKDecimal.ne(aNumber, aX)) {
                if (ADKDecimal.eq(anIncrementQtyAboveThreshold,
                    anIncrementQtyBelowThreshold)) {
                  ADKDialog2.showAlertDialogText(
                      context,
                      'You must order in increments of ' +
                          anIncrementQtyBelowThreshold.toDecimal().toString() +
                          '. Your quantity has been rounded to ' +
                          aX.toDecimal().toString() +
                          '.');
                } else {
                  ADKDialog2.showAlertDialogText(
                      context,
                      'When ordering less than ' +
                          aThresholdQty.toDecimal().toString() +
                          ' you must order in increments of ' +
                          anIncrementQtyBelowThreshold.toDecimal().toString() +
                          '. Your quantity has been rounded to ' +
                          aX.toDecimal().toString() +
                          '.');
                }
                aNumber = aX;
              }
            } else if (anIncrementQtyAboveThreshold != null &&
                ADKDecimal.gt(aNumber, aThresholdQty)) {
              ADKDecimal aX = ADKDecimal.roundUpToIncrementQty(
                  anIncrementQtyAboveThreshold, aNumber);
              if (ADKDecimal.ne(aNumber, aX)) {
                if (ADKDecimal.eq(anIncrementQtyAboveThreshold,
                    anIncrementQtyBelowThreshold)) {
                  ADKDialog2.showAlertDialogText(
                      context,
                      'You must order in increments of ' +
                          anIncrementQtyAboveThreshold.toDecimal().toString() +
                          '. Your quantity has been rounded to ' +
                          aX.toDecimal().toString() +
                          '.');
                } else {
                  ADKDialog2.showAlertDialogText(
                      context,
                      'When ordering more than ' +
                          aThresholdQty.toDecimal().toString() +
                          ' you must order in increments of ' +
                          anIncrementQtyAboveThreshold.toDecimal().toString() +
                          '. Your quantity has been rounded to ' +
                          aX.toDecimal().toString() +
                          '.');
                }
                aNumber = aX;
              }
            }
          }
          _controller.text = aNumber.toDecimal().toString();

          this.getCart().setOrderQuantity(aProduct, aNumber);

          //setState(() {
          //  ChooseProductScreenState.shipToCardWidgetKey.currentState?.setState(() {});
          //});            //this.addFunction();
          setState(() {
            widget.actionRefresh();
            //_controller.clear();
            //_focusNode.requestFocus();
          });
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget aWidget = this.buildGridCard();
    return aWidget;
  }

  Widget productWidget() {
    return /*Align(
        alignment: Alignment.centerLeft,
        child:*/
        Text(
      widget.item.getProductKey(),
      textAlign: TextAlign.left,
      //style: ADKTheme.productStyle(),
    );
  }

  Widget productDescriptionWidget() {
    return /*Align(
        alignment: Alignment.centerLeft,
        child:*/
        Text(
      widget.item.getProductDescription(),
      textAlign: TextAlign.left,
      overflow: TextOverflow.ellipsis,
      //style: ADKTheme.productDescriptionStyle(),
    );
  }

  Widget lastPurchasedWidget() {
    return /*Align(
        alignment: Alignment.centerLeft,
        child:*/
        Text(
      widget.item.getProductDescription(),
      textAlign: TextAlign.left,
      overflow: TextOverflow.ellipsis,
      style: ADKTheme.productDescriptionStyle(),
    );
  }

  Widget buildGridCardWideScreen() {
    String aProductImageURL = widget.item.getImageFilename();
    String anImageURL;
    if (ADKMiscStringFunctions.isWebAddress(aProductImageURL)) {
      anImageURL = aProductImageURL;
    } else {
      anImageURL = ADKMiscFileFunctions.concatDirs(
          ADKURLBuilder().baseURL(), widget.item.getImageFilename());
    }
    Widget anImage = CachedNetworkImage(
        //placeholder: (context, url) => CircularProgressIndicator(),
        imageUrl: anImageURL,
        errorWidget: (context, url, error) => Text(''),
        fit: BoxFit.fill);

    anImage = GestureDetector(
      child: anImage,
      onTap: () async {
        await showDialog(
            context: context, builder: (_) => ImageDialog(anImageURL));
      },
    );

    //FYI TODO This does not seem to work
    //anImage =
    //    MouseRegion(cursor: SystemMouseCursors.pointer, child: anImage);

    List<Widget> aThirdColumnList = List.empty(growable: true);
    if (widget.item.wasPreviouslyPurchased()) {
      ADKDecimal? aLastQty = widget.item.getLastOrderQuantity();
      Widget aLabel;
      if (aLastQty == null) {
        aLabel = Text(
            'Last purchased on ' +
                UserData.getLocale()
                    .displayDateTime(widget.item.getLastOrderDate()),
            style: TextStyle(fontSize: 10));
      } else {
        aLabel = Text(
            'Last purchased ' +
                UserData.getLocale().displayDecimalAsInt(aLastQty) +
                ' on ' +
                UserData.getLocale()
                    .displayDateTime(widget.item.getLastOrderDate()),
            style: TextStyle(fontSize: 10));
      }
      aThirdColumnList.add(aLabel);
    }
    if (ADKGlobal.scShowPrices()) {
      aThirdColumnList.add(Text(
          ADKSCGlobal.scDisplayPricePerUOM(UserData.getLocale(),
              widget.item.getAdjustedUnitPrice(), widget.item.getOrderUOMKey()),
          style: TextStyle(
            fontSize: 14,
          )));

      SalesOrderLineSession? anObject;
      if (widget.item is SalesOrderLineSession) {
        anObject = widget.item as SalesOrderLineSession;
      } else {
        anObject = UserData.getUser().getCart().getLineInCart(widget.item);
      }
      if (anObject != null &&
          ADKDecimal.gt(anObject.getOrderQuantity(), ADKDecimal.zero())) {
        aThirdColumnList.add(Text(
            ADKSCGlobal.scDisplayTotalLinePrice(UserData.getLocale(), anObject),
            style: TextStyle(
              fontSize: 14,
            )));
      }
    }
    if (widget.forOrderInquiry) {
      aThirdColumnList.add(this.readOnlyQuantity(widget.item));
    } else if (widget.forOrderEdit) {
      if (widget.item.isChangeable()) {
        aThirdColumnList.add(this
            .buttons(widget.item, widget.removeFunction, widget.addFunction));
      }
    } else {
      aThirdColumnList.add(
          this.buttons(widget.item, widget.removeFunction, widget.addFunction));
    }
    Widget aRightSection = new Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: aThirdColumnList);

    aRightSection = new Container(child: aRightSection);

    Widget aMiddleSection = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          //width: MediaQuery.of(context).size.width * 0.5,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
            child: Text(
              widget.item.getProductKey(),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
          ),
        ),
        Container(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(5, 10, 0, 0),
            child: Text(
              widget.item.getProductDescription(),
              //style: TextStyle(
              //  fontSize: 12,
              //),
            ),
          ),
        ),
      ],
    );

    aMiddleSection = new Expanded(
        child: new Container(
            padding: new EdgeInsets.only(left: 8.0), child: aMiddleSection));

    double aWidth = 180;

    return Container(
      /*child: Card(
        color: aTileBackgroundColor,
        clipBehavior: Clip.hardEdge,*/
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.fromLTRB(5, 3, 2, 3),
              child: Container(
                width: this.getImageBoxWidth(),
                child: Center(
                    child: ConstrainedBox(
                  constraints: BoxConstraints(
                    maxWidth: this.getMaxImageWidth(),
                    maxHeight: this.getMaxImageHeight(),
                  ),
                  child: anImage,
                )),
              )),
          aMiddleSection,
          aRightSection
        ],
      ),
      //),
    );
  }

  Widget buildGridCardThinScreen() {
    Widget aProductLabel = this.productWidget();
    Widget aProductDescription = this.productDescriptionWidget();
    List<Widget> aColumnWidgets = List.empty(growable: true);
    aColumnWidgets.add(this.imageForMobile(widget.item));

    double aLabelPad = 6;
    aColumnWidgets.add(Padding(
        padding: EdgeInsets.only(left: aLabelPad), child: aProductLabel));
    aColumnWidgets.add(Padding(
        padding: EdgeInsets.only(left: aLabelPad), child: aProductDescription));

    if (ADKGlobal.scShowPrices()) {
      aColumnWidgets.add(Padding(
          padding: EdgeInsets.only(left: aLabelPad),
          child: Text(
              ADKSCGlobal.scDisplayPricePerUOM(
                  UserData.getLocale(),
                  widget.item.getAdjustedUnitPrice(),
                  widget.item.getOrderUOMKey()),
              style: TextStyle(
                fontSize: 14,
              ))));
    }
    bool wasPreviouslyPurchased = widget.item.wasPreviouslyPurchased();
    if (wasPreviouslyPurchased) {
      ADKDecimal? aLastQty = widget.item.getLastOrderQuantity();
      Widget aLastPurchasedLabel;
      String aLastOrderedDateS =
          UserData.getLocale().displayDateTime(widget.item.getLastOrderDate());
      if (aLastQty == null) {
        aLastPurchasedLabel = Text(
          'Last purchased on ' + aLastOrderedDateS,
        );
      } else {
        aLastPurchasedLabel = Text(
          'Last purchased ' +
              UserData.getLocale().displayDecimalAsInt(aLastQty) +
              ' on ' +
              aLastOrderedDateS,
        );
      }
      aColumnWidgets.add(Padding(
          padding: EdgeInsets.only(left: aLabelPad),
          child: aLastPurchasedLabel));
    }

    if (widget.forOrderInquiry) {
      aColumnWidgets.add(this.readOnlyQuantity(widget.item));
    } else if (widget.forOrderEdit) {
      aColumnWidgets.add(
          this.buttons(widget.item, widget.removeFunction, widget.addFunction));
    } else {
      aColumnWidgets.add(
          this.buttons(widget.item, widget.removeFunction, widget.addFunction));
    }
    Column aColumn = Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: aColumnWidgets);

    return aColumn;
  }

  Widget buildGridCard() {
    if (ADKGlobal.isWideScreen(context)) {
      Widget aWidget = buildGridCardWideScreen();
      return aWidget;
    } else {
      Widget aWidget = buildGridCardThinScreen();
      double aWidth = 180;
      double aHeight = 200;
      return Expanded(
          child: Container(
              //height: aHeight,
              width: aWidth,
              //clipBehavior: Clip.hardEdge,
              //constraints: BoxConstraints.tightForFinite(width: aWidth),
              padding: const EdgeInsets.all(1),
              margin: const EdgeInsets.all(3),
              decoration: BoxDecoration(
                boxShadow: [ShadowStyle.verticalProductShadow],
                borderRadius: BorderRadius.circular(16),
                color: Colors.white,
              ),
              child: aWidget));
    }
  }

  Widget readOnlyQuantity(CartLineItemI aProduct) {
    ADKDecimal? anOrderQuantity = widget.item.getOrderQuantity();
    ADKDecimal aQuantity = ADKDecimal.zero();
    if (anOrderQuantity != null) {
      aQuantity = anOrderQuantity;
    }
    Widget aWidget = Align(
        alignment: Alignment.bottomRight,
        child: Padding(
          padding: const EdgeInsets.only(
            right: 8.0,
            bottom: 4.0,
          ),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
            Text(
              "Quantity: " + aQuantity.toDecimal().toString(),
              //style: TextStyle(fontSize: 18.0),
            ),
          ]),
        ));
    return Container(child: aWidget);
  }

  Widget buttons(CartLineItemI aProduct, void Function() aRemoveFunction,
      void Function() anAddFunction) {
    Widget aWidget = Align(
        alignment: Alignment.bottomRight,
        child: Padding(
          padding: const EdgeInsets.only(
            right: 8.0,
            bottom: 8.0,
          ),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
            _decrementButton(aProduct, 1, aRemoveFunction),
            this.getQtyText(aProduct),
            _incrementButton(aProduct, 1, anAddFunction),
          ]),
        ));
    return Container(child: aWidget);
  }

  Widget getQtyText(CartLineItemI aProduct) {
    Widget aQtyWidget;
    Widget aQty;
    if (aProduct.isChangeable()) {
      aQty = TextFormField(
        //key: GlobalKey(),
        textAlign: TextAlign.center,
        controller: this._controller,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          isDense: true,
          counterText: '',
          contentPadding: EdgeInsets.fromLTRB(5, 5, 5, 5),
          isCollapsed: true,
        ),
        keyboardType: TextInputType.number,
        style: TextStyle(fontSize: 18.0),
        maxLength: 4,
        focusNode: _focusNode,
        onChanged: (aValue) {
          //TODO Check increment quantities
          //ADKDecimal aNumber = ADKDecimal.fromString(aValue);
          //this.getCart().setOrderQuantity(aProduct, aNumber);
          //WARN Do not do this...causes crazy results
          //setState(() {});
        },
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly
        ], // Only numbers can be entered,
      );
    } else {
      aQty = Text("Quantity: " + this._controller.value.text);
    }

    aQtyWidget = Container(width: 50, child: aQty);
    return aQtyWidget;
  }

  void dispose() {
    // Clean up the controller when the widget is removed from the widget tree.
    // This also removes the _printLatestValue listener.
    _controller.dispose();
    //this._focusNode.removeListener(_onFocusChange);
    //_focusNode.dispose();
    super.dispose();
  }

  Cart getCart() {
    return UserData.getUser().getCart();
  }

  Widget _decrementButton(
      CartLineItemI aProduct, int index, void Function() aRemoveFunction) {
    if (!aProduct.isChangeable()) {
      //FYI This is not executed
      return Text('');
    }
    IconData anIcon = Icons.remove;
    if (ADKDecimal.le(aProduct.getOrderQuantity(), ADKDecimal.one())) {
      if (aProduct is SalesOrderLineSession) {
        SalesOrderLineSession aLine = aProduct;
        if (!aLine.isPersistent()) {
          anIcon = Icons.delete_forever;
        }
      } else {
        SalesOrderLineSession? anObject =
            UserData.getUser().getCart().getLineInCart(widget.item);
        if (anObject != null &&
            anObject.isPersistent() &&
            ADKDecimal.le(anObject.getOrderQuantity(), ADKDecimal.one())) {
          anIcon = Icons.delete_forever;
        }
      }
    }
    Color aFillColor = this.buttonFillColor(aProduct);
    Color aColor = this.buttonColor(aProduct);
    return RoundedIconButton(
      icon: anIcon,
      onPress: () {
        this.getCart().decreaseQuantity(widget.item);
        SalesOrderLineSession? aLine = this.getCart().getLineInCart(aProduct);
        String aQtyText = '0';

        if (aLine != null) {
          ADKDecimal? aQty = aLine.getOrderQuantity();
          ADKDecimal anOrderQty;
          if (aQty == null) {
            anOrderQty = ADKDecimal.zero();
          } else {
            anOrderQty = aQty;
          }
          aQtyText = anOrderQty.toDecimal().toString();
        }
        this._controller.text = aQtyText;
        setState(() {
          aRemoveFunction();
        });
      },
      iconSize: _plusMinusIconSize,
      fillColor: aFillColor,
      color: aColor,
    );
  }

  Color buttonFillColor(CartLineItemI aProduct) {
    Color aFillColor = ADKTheme.CARD_PRODUCT_COLOR;
    if (this.getCart().isProductCountChanged(widget.item)) {
      aFillColor = Colors.green;
    }
    return aFillColor;
  }

  Color buttonColor(CartLineItemI aProduct) {
    Color aColor = Colors.black;
    if (this.getCart().isProductCountChanged(widget.item)) {
      aColor = Colors.white;
    }
    return aColor;
  }

  Widget _incrementButton(
      CartLineItemI aProduct, int index, void Function() anAddFunction) {
    if (!aProduct.isChangeable()) {
      //FYI This is not executed
      return Text('');
    }

    Color aFillColor = this.buttonFillColor(aProduct);
    Color aColor = this.buttonColor(aProduct);
    return RoundedIconButton(
      icon: Icons.add,
      onPress: () {
        this.getCart().add(widget.item);
        SalesOrderLineSession? aLine = this.getCart().getLineInCart(aProduct);
        String aQtyText = '0';

        if (aLine != null) {
          ADKDecimal? aQty = aLine.getOrderQuantity();
          ADKDecimal anOrderQty;
          if (aQty == null) {
            anOrderQty = ADKDecimal.zero();
          } else {
            anOrderQty = aQty;
          }
          aQtyText = anOrderQty.toDecimal().toString();
        }
        setState(() {
          this._controller.text = aQtyText;
          //ChooseProductScreenState.shipToCardWidgetKey.currentState
          //    ?.setState(() {});
          //ChooseProductScreenState.orderWidgetKey.currentState?.setState(() {});
          //OrderPageState.orderWidgetKey.currentState?.setState(() {});

          anAddFunction();
        });
      },
      iconSize: _plusMinusIconSize,
      fillColor: aFillColor,
      color: aColor,
    );
  }

  Widget imageForMobile(CartLineItemI item) {
    String aBaseURL = ADKURLBuilder().baseURL();
    String aFilename = item.getImageFilename();
    return Align(
        alignment: Alignment.topCenter,
        child: CachedNetworkImage(
          //placeholder: (context, url) => CircularProgressIndicator(),
          imageUrl: aBaseURL + aFilename,
          width: 290.0,
          height: 100.0,
        ));
  }

  double getMaxImageHeight() {
    return 100;
  }

  double getMaxImageWidth() {
    return 100;
  }

  double getImageBoxWidth() {
    return getMaxImageWidth() + 10;
  }
}

class ImageDialog extends StatelessWidget {
  String imageFilename = '';

  ImageDialog(String anImageFilename) {
    this.imageFilename = anImageFilename;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
          //width: 500,
          //height: 500,
          margin: EdgeInsets.all(10),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: Image.network(this.imageFilename),
          )),
    );
  }
}
