import 'package:ast_app/com/adk/ics/business/cart.dart';
import 'package:ast_app/com/adk/ics/business/salesordersession.dart';
import 'package:ast_app/com/adk/ics/cg/salesorderlinesessioncg.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:shoppingcart_app/com/adk/config/routes.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/adktheme.dart';
import 'package:shoppingcart_app/com/adk/sc/cartpage.dart';
import 'package:shoppingcart_app/com/adk/sc/loginpage.dart';
import 'package:shoppingcart_app/com/adk/sc/openinvoicespage.dart';
import 'package:shoppingcart_app/com/adk/sc/openorderspage.dart';
import 'package:shoppingcart_app/com/adk/sc/orderpage.dart';
import 'package:shoppingcart_app/com/adk/sc/paymentmethodspage.dart';
import 'package:shoppingcart_app/com/adk/sc/selectcbspage.dart';
import 'package:shoppingcart_app/com/adk/sc/weekopenorderspage.dart';
import 'package:shoppingcart_app/com/adk/utility/adkscglobal.dart';

class ADKBottomNavigationBar extends StatefulWidget {
  ADKBottomNavigationBar();

  static Widget? buildBar(BuildContext context) {
    bool isWide = ADKGlobal.useLeftMenu(context);

    if (isWide) {
      return null;
    } else {
      return ADKBottomNavigationBar();
    }
  }

  @override
  _ADKBottomNavigationBarState createState() => _ADKBottomNavigationBarState();
}

class _ADKBottomNavigationBarState extends State<ADKBottomNavigationBar> {
  final Color? backgroundColor = ADKTheme.APPBAR_BACKGROUNDCOLOR;

  List<Widget> widgets = List.empty(growable: true);

  @override
  Widget build(BuildContext context) {
    double iconSize = 50;
    Cart aCart = UserData.getUser().getCart();
    List<SalesOrderLineSessionCG> aCartList = aCart.getCartList();

    List<BottomNavigationBarItem> anItemList = List.empty(growable: true);
    BottomNavigationBarItem aHomeItem = BottomNavigationBarItem(
      icon: new Icon(
        Icons.home,
        color: ADKTheme.BUTTON_ICONCOLOR,
      ),
      label: 'Home',
    );
    BottomNavigationBarItem aCreateItem = BottomNavigationBarItem(
      icon: new Icon(
        Icons.add_circle_outline,
        color: ADKTheme.BUTTON_ICONCOLOR,
      ),
      label: 'Create Order',
    );
    BottomNavigationBarItem aCartItem = BottomNavigationBarItem(
      icon: Stack(children: <Widget>[
        new Icon(
          Icons.shopping_cart,
          color: ADKTheme.BUTTON_ICONCOLOR,
        ),
        if (aCart.isCartInProgress())
          Padding(
            padding: const EdgeInsets.only(left: 2.0, top: 8),
            child: CircleAvatar(
              radius: 8.0,
              backgroundColor: Colors.red,
              foregroundColor: Colors.white,
              child: Text(
                aCartList.length.toString(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 12.0,
                ),
              ),
            ),
          ),
      ]),
      label: 'Cart',
    );
    BottomNavigationBarItem aMenuItem = BottomNavigationBarItem(
      icon: GestureDetector(
        onTapDown: (TapDownDetails tapDownDetails) => onTapDown(
          context,
          tapDownDetails,
        ),
        child: Icon(
          Icons.menu,
          //size: 35.0,
        ),
      ),
      label: 'Menu',
    );
    anItemList.add(aHomeItem);
    anItemList.add(aCreateItem);
    anItemList.add(aCartItem);
    anItemList.add(aMenuItem);

    void Function() aRefreshFunction = () {};

    return BottomNavigationBar(
        showSelectedLabels: true,
        showUnselectedLabels: true,
        unselectedItemColor: ADKTheme.BUTTON_BACKGROUNDCOLOR,
        selectedItemColor: ADKTheme.BUTTON_BACKGROUNDCOLOR,
        iconSize: iconSize,
        currentIndex: 0,
        type: BottomNavigationBarType.fixed,
        // this will be set when a new tab is tapped
        onTap: (index) {
          if (index == 0) {
            //Home
            Navigator.of(context).pushNamedAndRemoveUntil(
                ADKSCRoutes.PAGE_WEEKORDERSPAGE,
                (Route<dynamic> route) => false);
          } else if (index == 1) {
            //Create Sales Order
            if (aCart.isCartInProgress()) {
              ADKDialog2.showAlertDialogText(
                  context, 'A cart has already been started');
            } else {
              aCart.resetCart();
              Route route = MaterialPageRoute(
                  builder: (context) => SelectCBSPage(aRefreshFunction));
              Navigator.of(context).push(route);
            }
          } else if (index == 2) {
            //Shopping Cart
            if (aCart.isCartInProgress()) {
              if (aCart.isCartInUpdateMode()) {
                SalesOrderSession? aSalesOrder = aCart.getSalesOrderSession();
                if (aSalesOrder != null) {
                  Route route = MaterialPageRoute(
                      builder: (context) =>
                          OrderPage(aSalesOrder, true, aRefreshFunction));
                  Navigator.of(context).push(route);
                }
              } else {
                Route route = MaterialPageRoute(
                    builder: (context) => CartPage(aRefreshFunction));
                Navigator.of(context).push(route);
              }
            }
          } else if (index == 3) {
            //Menu
            //Route route = MaterialPageRoute(builder: (context) => LoginPage());
            //Navigator.push(context, route);

          }
        },
        items: anItemList);
  }

  void onTapDown(
    BuildContext context,
    TapDownDetails tapDownDetailsDetails,
  ) {
    final double pressX = tapDownDetailsDetails.globalPosition.dx,
        pressY = tapDownDetailsDetails.globalPosition.dy;

    showMenu(
      context: context,
      position: RelativeRect.fromLTRB(pressX, pressY, pressX, pressY),
      items: getMenuItems(),
    ).then(
      (value) => perfomSelectedAction(/*context,*/ value),
    );
  }

  List<PopupMenuEntry<dynamic>> getMenuItems() {
    List<PopupMenuEntry<dynamic>> aList = List.empty(growable: true);
    if (!UserData.getUser().hasMultipleShipTos()) {
      aList.add(PopupMenuItem(
        value: 'weekOpenOrders',
        child: Row(
          children: <Widget>[
            Icon(
              Icons.calendar_view_week,
              color: ADKTheme.BUTTON_ICONCOLOR,
            ),
            Text('My Week'),
          ],
        ),
      ));
    }
    aList.add(PopupMenuItem(
      value: 'openOrders',
      child: Row(
        children: <Widget>[
          Icon(
            Icons.shopping_bag,
            color: ADKTheme.BUTTON_ICONCOLOR,
          ),
          Text("Open Orders"),
        ],
      ),
    ));
    aList.add(PopupMenuItem(
      value: 'openInvoices',
      child: Row(
        children: <Widget>[
          Icon(
            Icons.payments,
            color: ADKTheme.BUTTON_ICONCOLOR,
          ),
          Text('Open Invoices'),
        ],
      ),
    ));
    if (ADKSCGlobal.scIsPaymentMethodEnabled()) {
      aList.add(PopupMenuItem(
        value: 'paymentMethods',
        child: Row(
          children: <Widget>[
            Icon(
              Icons.payment,
              color: ADKTheme.BUTTON_ICONCOLOR,
            ),
            Text('Payment Methods'),
          ],
        ),
      ));
    }
    if (UserData.getUser().isSystemAdminType()) {
      aList.add(PopupMenuItem(
        value: 'about',
        child: Row(
          children: <Widget>[
            Icon(
              Icons.info_outline,
              color: ADKTheme.BUTTON_ICONCOLOR,
            ),
            Text('About'),
          ],
        ),
      ));
    }
    aList.add(PopupMenuItem(
      value: 'logout',
      child: Row(
        children: <Widget>[
          Icon(
            Icons.logout,
            color: ADKTheme.BUTTON_ICONCOLOR,
          ),
          Text('Logout'),
        ],
      ),
    ));
    return aList;
  }

  void perfomSelectedAction(dynamic value) {
    void Function() aRefreshFunction = () {
      setState(() {});
      //this.myAppBar.callBack();
      //this.callback();
    };
    switch (value) {
      case 'openOrders':
        Route route = MaterialPageRoute(
            builder: (context) => OpenOrdersPage(aRefreshFunction));
        Navigator.push(context, route);
        break;
      case 'paymentMethods':
        Route route =
            MaterialPageRoute(builder: (context) => PaymentMethodsPage());
        Navigator.push(context, route);
        break;
      case 'weekOpenOrders':
        Route route = MaterialPageRoute(
            builder: (context) => WeekOpenOrdersPage(aRefreshFunction));
        Navigator.push(context, route);
        break;
      case 'openInvoices':
        print('option 1');
        Route route =
            MaterialPageRoute(builder: (context) => OpenInvoicesPage());
        Navigator.push(context, route);
        break;
      case 'logout':
        UserData.logout();
        Route route = MaterialPageRoute(builder: (context) => LoginPage());
        Navigator.push(context, route);
        break;
      case 'about':
        showAboutDialog(
            context: context,
            applicationLegalese: 'Alpha Baking Shopping Cart Application',
            applicationName: 'ASSISTics Cart',
            applicationVersion: ADKGlobal.getPackageInfo().version +
                ' (' +
                ADKGlobal.getPackageInfo().buildNumber +
                ')',
            applicationIcon: Image.asset(
              ADKGlobal.getLogoFilename(),
              fit: BoxFit.contain,
              //scale: bo,
              height: 43,
            ));
        //Route route = MaterialPageRoute(builder: (context) => AboutPage());
        //Navigator.push(context, route);
        break;
      default:
        break;
    }

    //return value;
  }

  _handleClick(String value) {
    switch (value) {
      case 'Logout':
        break;
      case 'Settings':
        break;
    }
  }
}
