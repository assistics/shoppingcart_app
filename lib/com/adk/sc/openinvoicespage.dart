import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:flutter/material.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkbottomnavigationbar.dart';
import 'package:shoppingcart_app/com/adk/sc/invoicelistview.dart';

class OpenInvoicesPage extends StatefulWidget {
  String? snackBarMessage;

  OpenInvoicesPage({String? snackBarMessage}) {
    this.snackBarMessage = snackBarMessage;
  }

  @override
  _OpenInvoicesPageState createState() =>
      _OpenInvoicesPageState(this.snackBarMessage);
}

class _OpenInvoicesPageState extends State<OpenInvoicesPage> {
  bool _isLoading = false;
  String? snackBarMessage;

  _OpenInvoicesPageState(this.snackBarMessage) {
    this.snackBarMessage = snackBarMessage;
  }

  @override
  Widget build(BuildContext context) {
    Widget aBody = this.getBody();
    aBody = ADKGlobal.scaffoldFullPage(context, aBody);

    return Scaffold(
        appBar: ADKAppBar.buildBar(context, 'Open Invoices'),
        body: aBody,
        bottomNavigationBar: ADKBottomNavigationBar.buildBar(context));
  }

  Widget getBody() {
    InvoiceListView aList = new InvoiceListView(this.snackBarMessage);
    return aList;
  }
}
