import 'package:ast_app/com/adk/ics/business/invoicesession.dart';
import 'package:ast_app/com/adk/utility/adkdecimal.dart';
import 'package:ast_app/com/adk/utility/adkjsonresponse.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/creditcard.dart';
import 'package:ast_app/com/adk/utility/creditcardresponse.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shoppingcart_app/com/adk/config/routes.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkbottomnavigationbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/cartpage.dart';
import 'package:shoppingcart_app/com/adk/utility/adkscglobal.dart';
import 'package:shoppingcart_app/com/adk/utility/slimcdjs.dart'
    if (dart.library.html) 'package:ast_web/com/adk/utility/slimcdjs.dart'
    if (dart.library.io) 'package:shoppingcart_app/com/adk/utility/slimcdjs.dart';

class CreditCardPayment extends StatefulWidget {
  List<InvoiceSession> invoiceList = List.empty();

  CreditCardPayment(List<InvoiceSession> anInvoiceList) {
    this.invoiceList = anInvoiceList;
  }

  @override
  State<StatefulWidget> createState() {
    return CreditCardPaymentState(this.invoiceList);
  }

  /**
   * FYI - Method passes back null if no payments were selected
   */
  static ADKDecimal? getSubTotalPaymentAmount(
      List<InvoiceSession> anInvoiceList) {
    ADKDecimal? aTotal = null;
    for (InvoiceSession anInvoice in anInvoiceList) {
      if (anInvoice.isSelected()) {
        if (aTotal == null) {
          aTotal = ADKDecimal.zero();
        }
        ADKDecimal? anOpenAmountABS = anInvoice.getOpenAmount();
        ADKDecimal? anOpenAmount;
        if (anInvoice.isDocumentTypeCreditMemo()) {
          anOpenAmount =
              ADKDecimal.multiply(anOpenAmountABS, ADKDecimal.fromString('-1'));
        } else {
          anOpenAmount = anOpenAmountABS;
        }
        aTotal = ADKDecimal.add(aTotal, anOpenAmount);
      }
    }
    return aTotal;
  }
}

class CreditCardPaymentState extends State<CreditCardPayment> {
  String _cardNumber = '';
  String _expiryDate = '';
  String _cardHolderName = '';
  String _email = '';
  String _cvvCode = '';
  bool isCvvFocused = false;
  bool _isLoading = true;
  bool _rememberCardInfo = false;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  List<InvoiceSession> invoiceList = List.empty();

  CreditCardPaymentState(List<InvoiceSession> anInvoiceList) {
    this.invoiceList = anInvoiceList;
  }

  ADKDecimal _getSubTotalPaymentAmount() {
    ADKDecimal? aTotal =
        CreditCardPayment.getSubTotalPaymentAmount(this.invoiceList);
    if (aTotal == null) {
      aTotal = ADKDecimal.zero();
    }
    return aTotal;
  }

  ADKDecimal _getConvenienceFee() {
    ADKDecimal? aTotal =
        CreditCardPayment.getSubTotalPaymentAmount(this.invoiceList);
    if (aTotal == null) {
      aTotal = ADKDecimal.zero();
    }
    return ADKDecimal.multiply(
        aTotal,
        ADKDecimal.divide(ADKSCGlobal.getSCConvenienceFeePercentage(),
            ADKDecimal.fromString('100')));
  }

  ADKDecimal _getTotalPaymentAmount() {
    ADKDecimal? aSubTotal =
        CreditCardPayment.getSubTotalPaymentAmount(this.invoiceList);
    if (aSubTotal == null) {
      aSubTotal = ADKDecimal.zero();
    }

    ADKDecimal aConvenieneFee = this._getConvenienceFee();

    return ADKDecimal.add(aSubTotal, aConvenieneFee);
  }

  void initState() {
    super.initState();
    this.asyncMethod();
  }

  static final String _PREF_REMEMBER_CARD_INFO = 'remembercreditcardinfo';
  static final String _PREF_CARD_NUMBER = 'cardnumber';
  static final String _PREF_CARD_HOLDER_NAME = 'cardholdername';
  static final String _PREF_EXPIRY_DATE = 'expirydate';
  static final String _PREF_CVVCODE = 'cvvcode';

  void asyncMethod() async {
    _isLoading = true;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? aCardNumber = prefs.getString(_PREF_CARD_NUMBER);
    if (aCardNumber != null) {
      this._cardNumber = aCardNumber;
    }
    String? aFullName = prefs.getString(_PREF_CARD_HOLDER_NAME);
    if (aFullName != null) {
      this._cardHolderName = aFullName;
    }
    String? anExpiryDate = prefs.getString(_PREF_EXPIRY_DATE);
    if (anExpiryDate != null) {
      this._expiryDate = anExpiryDate;
    }
    String? aCVV2 = prefs.getString(_PREF_CVVCODE);
    if (aCVV2 != null) {
      this._cvvCode = aCVV2;
    }

    bool? aRememberCardInfo = prefs.getBool(_PREF_REMEMBER_CARD_INFO);
    if (aRememberCardInfo != null) {
      this._rememberCardInfo = aRememberCardInfo;
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_isLoading) {
      return Scaffold(
        appBar: ADKAppBar.buildBar(context, 'Payment'),
        body: _showCircularProgress(),
        bottomNavigationBar: ADKBottomNavigationBar.buildBar(context), //    )
      );
    } else {
      ADKDecimal aSubTotal = this._getSubTotalPaymentAmount();
      ADKDecimal aConvenienceFee = this._getConvenienceFee();
      ADKDecimal aTotal = this._getTotalPaymentAmount();

      TextStyle aStyle = TextStyle(fontSize: 24);

      Widget aTotalsSummary = Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text(
              'Sub-Total:',
              style: aStyle,
            ),
            Text(
              'Convenience Fee:',
              style: aStyle,
            ),
            Text(
              'Total:',
              style: aStyle,
            ),
          ]),
          Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
            Text(
              UserData.getLocale().displayMonetaryData(aSubTotal),
              style: aStyle,
            ),
            Text(
              UserData.getLocale().displayMonetaryData(aConvenienceFee),
              style: aStyle,
            ),
            Text(
              UserData.getLocale().displayMonetaryData(aTotal),
              style: aStyle,
            ),
          ]),
        ],
      );

      Widget aCardWidget = CreditCardWidget(
        height: 160,
        cardNumber: this._cardNumber,
        expiryDate: this._expiryDate,
        cardHolderName: this._cardHolderName,
        cvvCode: this._cvvCode,
        showBackView: this.isCvvFocused,
        obscureCardNumber: true,
        obscureCardCvv: true,
        //TODO
        onCreditCardWidgetChange: (CreditCardBrand) {},
      );
      Widget aCardForm = CreditCardForm(
        formKey: formKey,
        obscureCvv: true,
        obscureNumber: true,
        cardNumber: this._cardNumber,
        cvvCode: this._cvvCode,
        cardHolderName: this._cardHolderName,
        expiryDate: this._expiryDate,
        //TODO
        //themeColor: Colors.blue,
        inputConfiguration: const InputConfiguration(
            cardNumberDecoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Number',
              hintText: 'XXXX XXXX XXXX XXXX',
            ),
            expiryDateDecoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Expired Date',
              hintText: 'XX/XX',
            ),
            cvvCodeDecoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'CVV',
              hintText: 'XXX',
            ),
            cardHolderDecoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Card Holder',
            )),
        onCreditCardModelChange: onCreditCardModelChange,
      );

      Widget aStoreCreditCardInfo = CheckboxListTile(
        title: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            'Remember Card Information',
          ),
        ),
        controlAffinity: ListTileControlAffinity.leading,
        value: this._rememberCardInfo,
        onChanged: (bool? newValue) {
          setState(() {
            if (newValue != null) {
              this._rememberCardInfo = newValue;
            }
          });
        },
      );

      Widget aScaffold = Scaffold(
          appBar: ADKAppBar.buildBar(context, 'Payment'),
          body: SafeArea(
            child: Column(
              children: <Widget>[
                SizedBox(height: 10),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        aTotalsSummary,
                        aCardWidget,
                        aCardForm,
                        aStoreCreditCardInfo,
                        this.makePaymentButton(this.invoiceList),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          bottomNavigationBar: ADKBottomNavigationBar.buildBar(context));
      return GestureDetector(
          onTap: // () => FocusManager.instance.primaryFocus?.unfocus(),
              () {
            //FocusScope.of(context).unfocus();
            FocusScopeNode currentFocus = FocusScope.of(context);

            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: aScaffold);
    }
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget makePaymentButton(List<InvoiceSession> anInvoiceList) {
    return OpenFlutterButton(
      backgroundColor: Colors.yellow,
      textColor: Colors.black,
      borderColor: Colors.grey,
      onPressed: () => processPayment(anInvoiceList),
      title: 'MAKE PAYMENT FOR ' +
          UserData.getLocale()
              .displayMonetaryData(this._getTotalPaymentAmount()),
    );
  }

  void processPayment(List<InvoiceSession> anInvoiceList) async {
    this.setState(() {
      this._isLoading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (this._rememberCardInfo) {
      prefs.setString(_PREF_CARD_NUMBER, this._cardNumber);
      prefs.setString(_PREF_CARD_HOLDER_NAME, this._cardHolderName);
      prefs.setString(_PREF_CVVCODE, this._cvvCode);
      prefs.setString(_PREF_EXPIRY_DATE, this._expiryDate);
    } else {
      prefs.remove(_PREF_CARD_NUMBER);
      prefs.remove(_PREF_CARD_HOLDER_NAME);
      prefs.remove(_PREF_CVVCODE);
      prefs.remove(_PREF_EXPIRY_DATE);
    }
    prefs.setBool(_PREF_REMEMBER_CARD_INFO, this._rememberCardInfo);

    CreditCard aCreditCard = CreditCard(this._cardHolderName, this._email,
        this._cardNumber, this._expiryDate, this._cvvCode);
    if (formKey.currentState!.validate()) {
      this._isLoading = true;
      CreditCardResponse aCreditCardResponse =
          await SlimcdJS().retrieveSLIMCDToken(aCreditCard).catchError((error) {
        this.setState(() {
          this._isLoading = false;
        });
        return new CreditCardResponse.error(error.toString());
      });

      if (aCreditCardResponse.isError()) {
        ADKDialog2.showAlertDialogText(
            context, aCreditCardResponse.getErrorMessage());
      } else {
        ADKDecimal paymenttotal = this._getTotalPaymentAmount();
        ADKDecimal paymentconveniencefee = this._getConvenienceFee();
        ADKDecimal paymentsubtotal = this._getSubTotalPaymentAmount();
        try {
          ADKJSONResponse aResponse = await ADKURLBuilder().makePaymentOnServer(
              aCreditCard,
              aCreditCardResponse,
              anInvoiceList,
              paymenttotal,
              paymentconveniencefee,
              paymentsubtotal);

          this.setState(() {
            this._isLoading = false;
          });

          if (aResponse.isSuccess()) {
            ADKDialog2.showDialogText(
                context,
                '',
                'Payment of ' +
                    UserData.getLocale().displayMonetaryData(paymenttotal) +
                    ' was successfully processed',
                () => {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          ADKSCRoutes.PAGE_OPENORDERSPAGE, (route) => false)
                    });
          } else {
            ADKDialog2.showAlertDialogText(context, aResponse.getMessageText());
            this.setState(() {
              this._isLoading = false;
            });
          }
        } catch (all) {
          ADKDialog2.showAlertDialog(context, all, 1);
          this.setState(() {
            this._isLoading = false;
          });
        }
      }
    } else {
      ADKDialog2.showAlertDialogText(
          context, 'Invalid credit card information');
      this.setState(() {
        this._isLoading = false;
      });
    }
  }

  void onCreditCardModelChange(CreditCardModel? creditCardModel) async {
    setState(() {
      _cardNumber = creditCardModel!.cardNumber;
      _expiryDate = creditCardModel.expiryDate;
      _cardHolderName = creditCardModel.cardHolderName;
      _cvvCode = creditCardModel.cvvCode;
      isCvvFocused = creditCardModel.isCvvFocused;
    });
  }
}
