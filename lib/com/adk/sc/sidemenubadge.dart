import 'package:ast_app/com/adk/ics/business/cart.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';

class SideMenuBadgeWidget extends StatefulWidget {
  late bool selected;
  SideMenuBadgeWidget(Key? key, bool isSelected) : super(key: key) {
    selected = isSelected;
  }

  @override
  _SideMenuBadgeWidgetState createState() => _SideMenuBadgeWidgetState();
}

class _SideMenuBadgeWidgetState extends State<SideMenuBadgeWidget> {
  _SideMenuBadgeWidgetState() {}

  @override
  Widget build(BuildContext context) {
    Cart aCart = UserData.getUser().getCart();
    Icon anIcon;
    if (widget.selected) {
      anIcon = Icon(
        Icons.shopping_cart_outlined,
        //size: 36.0,
        color: Colors.blue,
      );
    } else {
      anIcon = Icon(
        Icons.shopping_cart_outlined,
        //size: 36.0,
      );
    }

    Widget aCartSelectedWidget = Stack(
      alignment: Alignment.topCenter,
      children: <Widget>[
        anIcon,
        if (aCart.isCartInProgress() &&
            aCart.getNumberOfSalesOrderLinesInCart() > 0)
          Padding(
            padding: const EdgeInsets.only(left: 2.0, top: 0),
            child: CircleAvatar(
              radius: 8.0,
              backgroundColor: Colors.red,
              foregroundColor: Colors.white,
              child: Text(
                aCart.getNumberOfSalesOrderLinesInCart().toString(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 12.0,
                ),
              ),
            ),
          ),
      ],
    );
    return aCartSelectedWidget;
  }
}
