import 'package:ast_app/com/adk/ics/business/icsuser.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkmiscstringfunctions.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:logger/logger.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shoppingcart_app/com/adk/config/routes.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/adktheme.dart';
import 'package:shoppingcart_app/com/adk/sc/applocalizations.dart';
import 'package:shoppingcart_app/com/adk/sc/forgotpasswordpage.dart';

//import 'package:flutter_login_demo/services/authentication.dart';

class LoginPage extends StatefulWidget {
  String snackMessage = "";

  LoginPage();

  LoginPage.LoginPageWithMessage(String aSnackMessage) {
    this.snackMessage = aSnackMessage;
  }

  //final BaseAuth auth;
  //VoidCallback loginCallback;

  @override
  State<StatefulWidget> createState() => new _LoginPageState(snackMessage);
}

class _LoginPageState extends State<LoginPage> {
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final logger = Logger();
  final _formKey = new GlobalKey<FormState>();

  String _email = "";
  String _password = "";
  String snackMessage = "";
  String _errorMessage = "";
  static final String _PREF_REMEMBER_ME = 'rememberlogin';
  static final String _PREF_REMEMBER_LOGIN = 'sclogin';
  bool _isLoading = false;
  bool _rememberMe = false;
  TextEditingController emailController = TextEditingController();

  _LoginPageState(String aSnackMessage) {
    this.snackMessage = aSnackMessage;
  }

  // Check if form is valid before perform login or signup
  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form != null) {
      if (form.validate()) {
        form.save();
        return true;
      }
    }
    return false;
  }

  // Perform login or signup
  void validateAndSubmit() async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });

    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (this._rememberMe) {
      prefs.setString(_PREF_REMEMBER_LOGIN, this._email);
    } else {
      prefs.remove(_PREF_REMEMBER_LOGIN);
    }
    prefs.setBool(_PREF_REMEMBER_ME, this._rememberMe);

    if (validateAndSave()) {
      String userId = "";
      ICSUser? aUser;
      aUser = await this
          .fetchICSUser(_email, _password)
          . //.onError((error, stackTrace) {}).
          catchError((all) {
        //print('Func Error: $all');
        setState(() {
          this._isLoading = false;
          this.resetForm();
        });
        ADKDialog2.showLoginAlertDialog(context, all);
      });

      if (aUser != null) {
        UserData.setUser(aUser);
        UserData.initLocale();
        _showWelcomeScreen(aUser);
        print('Signed in: $userId');
      }

      if (userId.length > 0) {
        //widget.loginCallback();
      }
    }
  }

  void _showWelcomeScreen(ICSUser aUser) {
    if (ADKGlobal.useLeftMenu(context)) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil(ADKSCRoutes.PAGE_MENUPAGE, (route) => false);
    } else {
      Navigator.of(context).pushNamedAndRemoveUntil(
          ADKSCRoutes.PAGE_WEEKORDERSPAGE, (route) => false);
    }
  }

  void _showForgotPasswordScreen() {
    _isLoading = false;
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => ForgotPasswordPage()));
  }

  Future<ICSUser?> fetchICSUser(String aLogin, String aPassword) async {
    aLogin = aLogin.trim();
    aPassword = aPassword.trim();

    ADKURLBuilder aURLBuilder = ADKURLBuilder.forLogin();
    await aURLBuilder.scLogin(aLogin, aPassword);

    return UserData.getUser();
  }

  @override
  void initState() {
    this._errorMessage = "";
    //this._isLoading = false;
    //this._isLoginForm = true;
    super.initState();
    _initPackageInfo();
    this.asyncMethod();
  }

  void asyncMethod() async {
    _isLoading = true;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool? aRememberMe = prefs.getBool(_PREF_REMEMBER_ME);
    if (aRememberMe != null) {
      this._rememberMe = aRememberMe;
      if (this._rememberMe) {
        String? aLogin = prefs.getString(_PREF_REMEMBER_LOGIN);
        if (aLogin != null) {
          _email = aLogin;
          this.emailController.text = _email;
        }
      }
    }

    setState(() {
      _isLoading = false;
    });
  }

  Future<void> _initPackageInfo() async {
    final info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
      ADKGlobal.setPackageInfo(info);
    });
  }

  void showInSnackBar(String? value) {
    String aValue;
    if (value == null) {
      aValue = "";
    } else {
      aValue = value;
    }
    ScaffoldState? aState = _scaffoldKey.currentState;
    if (aState != null) {
      //aState.showSnackBar(new SnackBar(
      ScaffoldMessenger.of(context).showSnackBar(new SnackBar(
        content: new Text(aValue),
        duration: Duration(seconds: 3),
      ));
    }
  }

  void resetForm() {
    FormState? aState = _formKey.currentState;
    if (aState != null) {
      aState.reset();
    }
    _errorMessage = "";
  }

  void toggleFormMode() {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    String userId = "";
    _showForgotPasswordScreen();
    if (userId.length > 0) {
      //widget.loginCallback();
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget aScaffold = new Scaffold(
        key: _scaffoldKey,
        body: Stack(
          children: <Widget>[
            _showForm(),
            _showCircularProgress(),
          ],
        ));

    if (this.snackMessage.length > 0) {
      SchedulerBinding? anInstance = SchedulerBinding.instance;
      anInstance.addPostFrameCallback((_) {
        //show snackbar here
        this.showInSnackBar(this.snackMessage);
        this.snackMessage = "";
      });
    }
    return GestureDetector(
        onTap: // () => FocusManager.instance.primaryFocus?.unfocus(),
            () {
          //FocusScope.of(context).unfocus();
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: aScaffold);
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget _showForm() {
    List<Widget> aList = List.empty(growable: true);
    aList.add(showLogo());
    aList.add(showEmailInput());
    aList.add(showPasswordInput());
    aList.add(showRememberMe());
    aList.add(showPrimaryButton());
    aList.add(showSecondaryButton());
    aList.add(showErrorMessage());

    String aText1 = ADKGlobal.getLoginHelpLine1();
    if (!ADKMiscStringFunctions.isBlankOrWhitespace(aText1)) {
      //aList.add(Align(
      //    alignment: Alignment.center,
      //     child: Text('For assistance, contact Customer Service')));
      aList.add(Align(alignment: Alignment.center, child: Text(aText1)));
    }
    String aText2 = ADKGlobal.getLoginHelpLine2();
    if (!ADKMiscStringFunctions.isBlankOrWhitespace(aText2)) {
      //aList.add(
      //    Align(alignment: Alignment.center, child: Text('(1) 488-846-7337')));
      aList.add(Align(alignment: Alignment.center, child: Text(aText2)));
    }
    String aText3 = ADKGlobal.getLoginHelpLine3();
    if (!ADKMiscStringFunctions.isBlankOrWhitespace(aText3)) {
      //aList.add(Align(
      //    alignment: Alignment.center,
      //    child: Text('M-F 7AM – 4 PM EST | Sat 7AM – 12 PM EST')));
      aList.add(Align(alignment: Alignment.center, child: Text(aText3)));
    }

    aList.add(showVersion());

    if (ADKGlobal.isWideScreen(context)) {
      return Center(
          child: Align(
              alignment: Alignment(0, -.5),
              child: Material(
                  elevation: 10,
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  child: Container(
                      decoration: BoxDecoration(
                        //color: Colors.grey[100],
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                      ),
                      width: 600,
                      padding: EdgeInsets.all(16.0),
                      child: new Form(
                        key: _formKey,
                        child: new ListView(shrinkWrap: true, children: aList),
                      )))));
    } else {
      return new Container(
          width: 600,
          padding: EdgeInsets.all(16.0),
          child: new Form(
            key: _formKey,
            child: new ListView(shrinkWrap: true, children: aList),
          ));
    }
  }

  Widget showRememberMe() {
    Color aColor = Colors.blueAccent;
    Widget aCheckBox = CheckboxListTile(
      activeColor: aColor,
      title: Align(
        alignment: Alignment.centerLeft,
        child: Text(
          'Remember Me',
        ),
      ),
      controlAffinity: ListTileControlAffinity.leading,
      value: this._rememberMe,
      onChanged: (bool? newValue) {
        setState(() {
          if (newValue != null) {
            this._rememberMe = newValue;
          }
        });
      },
    );
    //Widget aBox=SizedBox(child:aCheckBox,height: 500,);
    return aCheckBox;
  }

  Widget showErrorMessage() {
    if (_errorMessage.length > 0) {
      return new Text(
        _errorMessage,
        style: TextStyle(
            fontSize: 13.0,
            color: Colors.red,
            height: 1.0,
            fontWeight: FontWeight.w300),
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }

  Widget showVersion() {
    return Align(
        alignment: Alignment.center,
        child: new Text('v' +
            this._packageInfo.version +
            '+' +
            this._packageInfo.buildNumber));
  }

  Widget showLogo() {
    return new Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
        child: CircleAvatar(
          backgroundColor: Colors.white,
          radius: 150,
          child: Image.asset(
            ADKGlobal.getLogoFilename(),
            width: 250,
            height: 250,
            //color: Colors.red,
          ),
        ),
      ),
    );
  }

  Widget showEmailInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        controller: emailController,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: AppLocalizations.of(context)
                .translate('Email or Account Number'),
            icon: new Icon(
              Icons.account_circle,
              color: Colors.grey,
            )),
        validator: (value) => (value != null && value.isEmpty)
            ? AppLocalizations.of(context).translate('Field can\'t be empty')
            : null,
        onChanged: (value) => {
          {_email = value.trim()}
        },
      ),
    );
  }

  Widget showPasswordInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextFormField(
        onFieldSubmitted: (value) {
          validateAndSubmit();
        },
        maxLines: 1,
        obscureText: true,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: AppLocalizations.of(context).translate('Password'),
            icon: new Icon(
              Icons.lock,
              color: Colors.grey,
            )),
        validator: (value) {
          if (value != null && value.isEmpty) {
            this._isLoading = false;
            return AppLocalizations.of(context)
                .translate('Password can\'t be empty');
          } else {
            return null;
          }
        },
        onSaved: (value) => {
          if (value != null) {_password = value.trim()}
        },
      ),
    );
  }

  Widget showSecondaryButton() {
    return new TextButton(
        child: new Text(
            AppLocalizations.of(context).translate('Forgot Password?'),
            style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300)),
        onPressed: toggleFormMode);
  }

  Widget showPrimaryButton() {
    return new Padding(
      //padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
      //child: SizedBox(
      //height: 40.0,
      child: new ElevatedButton(
        //elevation: 5.0,
        style: ElevatedButton.styleFrom(
          padding: EdgeInsets.symmetric(vertical: 16),
          elevation: 5,
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(30.0)),
          //primary: Colors.white,
          backgroundColor: ADKTheme.BUTTON_BACKGROUNDCOLOR,
          //fixedSize: const Size(150, 200)
        ),
        //shape: new RoundedRectangleBorder(
        //    borderRadius: new BorderRadius.circular(30.0)),
        //color: ADKTheme.BUTTON_BACKGROUNDCOLOR,
        child: new Text('Login',
            style: new TextStyle(fontSize: 20.0, color: Colors.white)),
        onPressed: validateAndSubmit,
      ),
    );
  }
}
