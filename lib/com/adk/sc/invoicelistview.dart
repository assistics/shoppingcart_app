import 'package:ast_app/com/adk/ics/business/invoicesession.dart';
import 'package:ast_app/com/adk/utility/adkdecimal.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:flutter/material.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/cartpage.dart';
import 'package:shoppingcart_app/com/adk/sc/creditcardpayment.dart';
import 'package:shoppingcart_app/com/adk/sc/invoicetileforlist.dart';
import 'package:shoppingcart_app/com/adk/utility/adkscglobal.dart';

class InvoiceListView extends StatelessWidget {
  String? snackBarMessage;

  InvoiceListView(String? snackBarMessage) {
    this.snackBarMessage = snackBarMessage;
  }

  bool isShowOpen() {
    return true;
  }

  Widget makePaymentButton(
      BuildContext context, List<InvoiceSession> anInvoiceList) {
    return OpenFlutterButton(
      backgroundColor: Colors.yellow,
      textColor: Colors.black,
      borderColor: Colors.grey,
      onPressed: () => pressPaySelectedDocuments(context, anInvoiceList),
      title: 'PAY SELECTED DOCUMENTS...',
    );
  }

  void pressPaySelectedDocuments(
      BuildContext context, List<InvoiceSession> anInvoiceList) {
    ADKDecimal? aTotal =
        CreditCardPayment.getSubTotalPaymentAmount(anInvoiceList);

    if (aTotal == null) {
      ADKDialog2.showAlertDialogText(
          context, 'Please select at least one invoice');
    } else if (ADKDecimal.lt(aTotal, ADKDecimal.zero())) {
      ADKDialog2.showAlertDialogText(
          context, 'Total Payment Amount Cannot Be Negative');
    } else {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => CreditCardPayment(anInvoiceList)));
    }
  }

  @override
  Widget build(BuildContext context) {
    FutureBuilder<List<InvoiceSession>?> aList =
        FutureBuilder<List<InvoiceSession>?>(
      future: _fetchOpenInvoices(context),
      builder: (context, snapshot) {
        this._showSnackBar(context);

        if (snapshot.hasData) {
          List<InvoiceSession>? data = snapshot.data;
          if (data == null || data.length == 0) {
            String aTitleText;
            if (this.isShowOpen()) {
              aTitleText = 'You don\'t have any open invoices';
            } else {
              aTitleText = 'You don\'t have any past invoices';
            }
            return ADKGlobal.messageYellow(context, aTitleText, null);
          } else {
            return _openInvoicesPage(context, data);
          }
        } else if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        }
        return Center(child: CircularProgressIndicator());
      },
    );
    return aList;
  }

  Future<List<InvoiceSession>?> _fetchOpenInvoices(BuildContext context) async {
    ADKURLBuilder aURLBuilder = new ADKURLBuilder();
    Future<List<InvoiceSession>?> aList =
        aURLBuilder.fetchOpenInvoiceSessions();
    return aList;
  }

  Widget _openInvoicesPage(
      BuildContext context, List<InvoiceSession> anInvoiceList) {
    List<Widget> aList = List.empty(growable: true);
    if (ADKGlobal.isWideScreen(context)) {
      aList.add(ADKGlobal.messageGreen(context, 'Open Invoices'));
    }
    if (ADKSCGlobal.scAllowPayments()) {
      aList.add(this.makePaymentButton(context, anInvoiceList));
    }
    aList.add(
      Expanded(child: this._openInvoicesList(anInvoiceList)),
    );
    Widget aWidget = Column(children: aList);
    return aWidget;
  }

  final ScrollController _controller = ScrollController();

  Widget listWidget(
      BuildContext context, int index, List<InvoiceSession> anInvoiceList) {
    if (index == -1) {
      List<Widget> aColumnWidgetList = [];
      if (!ADKGlobal.isDesktop()) {
        Widget aButton = Align(
            alignment: Alignment.center,
            child: this.makePaymentButton(context, anInvoiceList));
        Row aRow = Row(children: [
          //SizedBox(width: 30),
          aButton
        ]);
        aColumnWidgetList.add(SizedBox(
          height: 5,
        ));
        aColumnWidgetList.add(aRow);
      }
      aColumnWidgetList.add(
        SizedBox(
          height: 10,
        ),
      );
      aColumnWidgetList.add(InvoiceTileForList(anInvoiceList[index]));
      return Column(children: aColumnWidgetList);
    } else {
      return InvoiceTileForList(anInvoiceList[index]);
    }
  }

  Widget _openInvoicesList(List<InvoiceSession> anAppointmentList) {
    Widget aWidget = ListView.builder(
        controller: _controller,
        //physics: const NeverScrollableScrollPhysics(),
        itemCount: anAppointmentList.length,
        itemBuilder: (context, index) {
          return this.listWidget(context, index, anAppointmentList);
        });
    if (ADKGlobal.isDesktop()) {
      //This padding makes the scrollbar not overlap the data
      aWidget =
          Padding(padding: EdgeInsets.fromLTRB(0, 0, 16, 0), child: aWidget);
      aWidget = Scrollbar(
          controller: _controller,
          //hoverThickness: 15,
          trackVisibility: false,
          thickness: 15,
          thumbVisibility: true,
          child: aWidget);
    }

    return aWidget;
  }

  _showSnackBar(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (this.snackBarMessage != null) {
        String msg = this.snackBarMessage!;
        this.snackBarMessage = null;
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(msg),
          duration: Duration(seconds: 6),
          //backgroundColor: kErrorColor,
        ));
      }
    });
  }
}
