import 'package:ast_app/com/adk/ics/business/calendardatesession.dart';
import 'package:ast_app/com/adk/ics/business/cart.dart';
import 'package:ast_app/com/adk/ics/business/cbsi.dart';
import 'package:ast_app/com/adk/ics/business/icsuser.dart';
import 'package:ast_app/com/adk/ics/business/salesordersession.dart';
import 'package:ast_app/com/adk/utility/adkdatetime.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkmiscdatefunctions.dart';
import 'package:ast_app/com/adk/utility/adkmiscstringfunctions.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/environmentlocale.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:shoppingcart_app/com/adk/sc/accountpage.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/chooseproductpage.dart';
import 'package:shoppingcart_app/com/adk/sc/ordertileforlist.dart';

class WeekOrderListView extends StatelessWidget {
  String? snackBarMessage;
  ValueChanged<bool>? notifyParent;
  late VoidCallback actionRefresh;
  bool _isLoading = false;
  List<CalendarDateSession> availableDates = List.empty();

  WeekOrderListView(
      String? snackBarMessage,
      ValueChanged<bool> aNotifyParent,
      VoidCallback anActionRefresh,
      List<CalendarDateSession> anAvailableDates) {
    this.snackBarMessage = snackBarMessage;
    this.notifyParent = aNotifyParent;
    this.actionRefresh = anActionRefresh;
    this.availableDates = anAvailableDates;
  }

  bool isShowOpen() {
    return true;
  }

  void _manageState() {
    if (this.notifyParent != null) {
      this.notifyParent!(this._isLoading);
    }
  }

  @override
  Widget build(BuildContext context) {
    FutureBuilder<List<SalesOrderSession>?> aList =
        FutureBuilder<List<SalesOrderSession>?>(
      future: _fetchOpenOrders(),
      builder: (context, snapshot) {
        this._showSnackBar(context);

        if (snapshot.hasData) {
          List<SalesOrderSession>? data = snapshot.data;
          if (data == null) {
            data = List.empty();
          }
          return _openOrdersListView(context, data);
        } else if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        }
        return Center(child: CircularProgressIndicator());
        //return _showCircularProgress();
      },
    );
    return aList;
  }

  Future<List<SalesOrderSession>?> _fetchOpenOrders() async {
    ADKURLBuilder aURLBuilder = new ADKURLBuilder();
    ICSUser aUser = UserData.getUser();
    String aShipToKey = '';
    CBSI? aDefaultCBS = aUser.getDefaultCBS();
    if (aDefaultCBS == null) {
      if (aUser.hasShipTos()) {
        aDefaultCBS = aUser.getCBSArray().first;
        aUser.setDefaultCBS(aDefaultCBS);
        aShipToKey = aDefaultCBS.getShipToKey();
      }
    } else {
      aShipToKey = aDefaultCBS.getShipToKey();
    }
    Future<List<SalesOrderSession>?> aList =
        aURLBuilder.fetchMyWeekSalesOrdersByShipTo(aShipToKey);
    return aList;
  }

  List<SalesOrderSession> getOrdersForDate(
      List<SalesOrderSession> anAppointmentList, ADKDateTime aDate) {
    List<SalesOrderSession> aList = List.empty(growable: true);
    for (var i = 0; i < anAppointmentList.length; i++) {
      SalesOrderSession anOrder = anAppointmentList[i];
      if (ADKMiscDateFunctions.equals(aDate, anOrder.getDeliveryDate())) {
        aList.add(anOrder);
      }
    }
    return aList;
  }

  Widget unavailableDate(BuildContext context, ADKDateTime aDate) {
    ICSUser aUser = UserData.getUser();
    Icon aLeadingIcon = Icon(
      Icons.cancel_outlined,
      color: Colors.grey,
    );

    List<Row> lines = List.empty(growable: true);
    lines.add(Row(children: [
      Text(
          UserData.getLocale()
              .displayDateTimeFormat(aDate, EnvironmentLocale.DAYMONTHYYYY),
          style: TextStyle(fontWeight: FontWeight.bold))
    ]));
    lines.add(Row(children: [
      Text(
        'Delivery date not available',
      )
    ]));

    Card aCard = Card(
        child: ListTile(
      tileColor: Colors.grey.shade200,
      title: Column(
        children: lines,
      ),
      leading: aLeadingIcon,
      //trailing: Icon(Icons.chevron_right),
    ));
    return aCard;
  }

  Widget emptyOrder(BuildContext context, ADKDateTime aDate) {
    ICSUser aUser = UserData.getUser();
    Icon aLeadingIcon;
    Color aTileColor;
    if (ADKGlobal.isWideScreen(context)) {
      aLeadingIcon = Icon(
        Icons.add_circle,
        size: 40,
        color: Colors.green,
      );
      aTileColor = Colors.white;
    } else {
      aLeadingIcon = Icon(
        Icons.add,
        color: Colors.green,
      );
      aTileColor = Colors.grey.shade200;
    }
    List<Row> lines = List.empty(growable: true);
    lines.add(Row(children: [
      Text(
          UserData.getLocale()
              .displayDateTimeFormat(aDate, EnvironmentLocale.DAYMONTHYYYY),
          style: TextStyle(fontWeight: FontWeight.bold))
    ]));

    String aText;
    CalendarDateSession? aCalendarDate = this.getCalendarDate(aDate);
    ADKDateTime? anOrderByDate;
    if (aCalendarDate != null) {
      anOrderByDate = aCalendarDate.getOrderByDate();
    }
    if (anOrderByDate == null) {
      aText = 'No order submitted';
    } else {
      aText = 'No order submitted. Order due by ' +
          UserData.getLocale().displayDateTime(anOrderByDate) +
          '.';
    }
    lines.add(Row(children: [
      Text(
        aText,
      )
    ]));

    Card aCard = Card(
        child: ListTile(
      tileColor: aTileColor,
      title: Column(
        children: lines,
      ),
      leading: aLeadingIcon,
      //trailing: Icon(Icons.chevron_right),
      onTap: () async {
        this._isLoading = true;
        this._manageState();
        Cart aCart = UserData.getUser().getCart();

        List<CBSI> aCBSList = aUser.getCBSArray();
        ADKDateTime aDeliveryDate = aDate;
        aCart.setDeliveryDate(aDeliveryDate);
        CBSI? aDefaultCBS = aUser.getDefaultCBS();
        if (aDefaultCBS != null) {
          aCart.setCBS(aDefaultCBS);
        } else if (aCBSList.length > 0) {
          aCart.setCBS(aCBSList.first);
        }
        //Retrieve products for date
        ADKURLBuilder aBuilder = new ADKURLBuilder();

        try {
          //TODO If single ship to then select
          await aBuilder.validateDeliveryDate2(aDate).then((value) {
            Route route = MaterialPageRoute(
                builder: (context) => ChooseProductScreen(this.actionRefresh));
            Navigator.pushReplacement(context, route);
          });
          //TODO Turn off progress bar?
          this._isLoading = false;
          this._manageState();
        } catch (all) {
          print('Func Error: $all');
          ADKDialog2.showAlertDialog(context, all, 1);
          aCart.resetCart();
        }
      },
    ));
    return aCard;
  }

  final ScrollController _controller = ScrollController();

  Widget _openOrdersListView(
      BuildContext context, List<SalesOrderSession> anAppointmentList) {
    int aDaysForward = ADKGlobal.getSCWeekViewDaysForward();
    //Show 7 cards (days forward)
    List<Widget> aList = List.empty(growable: true);
    EnvironmentLocale aLocale = UserData.getLocale();

    ICSUser aUser = UserData.getUser();
    CBSI? aDefaultCBS = aUser.getDefaultCBS();
    if (aDefaultCBS != null && aUser.hasMultipleShipTos()) {
      aList.add(_buildShipToCard(context, aDefaultCBS));
    }
    ADKDateTime aToday = ADKMiscDateFunctions.currentDate(aLocale);
    //Look for open orders prior to today
    for (int j = 0; j < anAppointmentList.length; j++) {
      SalesOrderSession anOrder = anAppointmentList[j];
      if (ADKMiscDateFunctions.before(anOrder.getRequiredDate(), aToday)) {
        aList.add(OrderTileForList(
            salesOrderSession: anOrder, anActionRefresh: this.actionRefresh));
      }
    }
    ADKDateTime aDate = ADKMiscDateFunctions.currentDate(aLocale);
    for (var i = 0; i < aDaysForward; i++) {
      aDate = ADKMiscDateFunctions.addDays(aToday, i);
      List<SalesOrderSession> anOrderList =
          this.getOrdersForDate(anAppointmentList, aDate);
      if (anOrderList.isNotEmpty) {
        for (var j = 0; j < anOrderList.length; j++) {
          SalesOrderSession anOrder = anOrderList[j];
          aList.add(OrderTileForList(
              salesOrderSession: anOrder, anActionRefresh: this.actionRefresh));
        }
      } else {
        if (this.isDateAvailable(aDate)) {
          aList.add(this.emptyOrder(context, aDate));
        } else {
          aList.add(this.unavailableDate(context, aDate));
        }
      }
    }
    for (int j = 0; j < anAppointmentList.length; j++) {
      SalesOrderSession anOrder = anAppointmentList[j];
      if (ADKMiscDateFunctions.after(anOrder.getRequiredDate(), aDate)) {
        aList.add(OrderTileForList(
            salesOrderSession: anOrder, anActionRefresh: this.actionRefresh));
      }
    }

    Widget aWidget = Column(children: aList);
    if (ADKGlobal.isWideScreen(context)) {
      aWidget = SingleChildScrollView(
          controller: _controller,
          //hoverThickness: 15,
          //trackVisibility: true,
          //showTrackOnHover: false,
          //thickness: 15,
          //isAlwaysShown: true,
          child: aWidget);
    }

    return aWidget;
  }

  Widget _buildShipToCard(BuildContext context, CBSI aCBS) {
    CBSI anAddress = aCBS;
    List<Widget> aColumn = List.empty(growable: true);
    String anAddressLine1 = anAddress.getAddressLine1();
    String aLine1 = anAddressLine1;
    if (ADKMiscStringFunctions.isBlankOrWhitespace(aLine1)) {
      aLine1 = anAddress.getAddressLine2();
    }
    String anAddressCity = anAddress.getCity();
    String anAddressState = anAddress.getState();
    String anAddressPostalCode = anAddress.getPostalCode();
    String aLine2 =
        anAddressCity + ", " + anAddressState + " " + anAddressPostalCode;

    Icon anIcon;
    Color? aTileColor;
    aTileColor = Colors.blue[50];
    Color aColor;
    if (aCBS.isCBSOnHold()) {
      aColor = Colors.yellow;
    } else {
      aColor = Colors.blue;
    }

    anIcon = Icon(Icons.where_to_vote_outlined, color: aColor);
    aColumn.add(Text("Account #: " + anAddress.getShipToKey()));
    aColumn.add(Text(anAddress.getShipToName()));
    if (!ADKMiscStringFunctions.isBlankOrWhitespace(aLine1)) {
      aColumn.add(Text(aLine1));
    }
    if (!ADKMiscStringFunctions.isBlankOrWhitespace(aLine2)) {
      aColumn.add(Text(aLine2));
    }
    ICSUser aUser = UserData.getUser();
    Widget aChangeWidgetShipTo;
    if (aUser.hasMultipleShipTos()) {
      aChangeWidgetShipTo = Align(
          alignment: Alignment.bottomRight,
          child: // Expanded(child:
              TextButton(
            child: Text('Switch Ship To'),
            onPressed: () {
              Route route =
                  MaterialPageRoute(builder: (context) => AccountPage());
              Navigator.of(context).push(route);
            },
          ));
    } else {
      aChangeWidgetShipTo = Text('');
    }
    Widget aDisplay = Card(
        elevation: 6.0,
        child: Stack(children: [
          ListTile(
            leading: anIcon,
            tileColor: aTileColor,
            title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: aColumn),
          ),
          aChangeWidgetShipTo,
        ]));

    return aDisplay;
  }

  bool isDateAvailable(ADKDateTime aDate) {
    List<CalendarDateSession> aList = this.availableDates;
    for (var i = 0; i < aList.length; i++) {
      CalendarDateSession aCalendarDate = aList[i];
      if (aCalendarDate.isAWorkDay()) {
        if (ADKMiscDateFunctions.equal(
            aCalendarDate.getCalendarDate(), aDate)) {
          return true;
        }
      }
    }
    return false;
  }

  CalendarDateSession? getCalendarDate(ADKDateTime aDate) {
    List<CalendarDateSession> aList = this.availableDates;
    for (var i = 0; i < aList.length; i++) {
      CalendarDateSession aCalendarDate = aList[i];
      if (aCalendarDate.isAWorkDay()) {
        if (ADKMiscDateFunctions.equal(
            aCalendarDate.getCalendarDate(), aDate)) {
          return aCalendarDate;
        }
      }
    }
    return null;
  }

  _showSnackBar(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (this.snackBarMessage != null) {
        String msg = this.snackBarMessage!;
        this.snackBarMessage = null;
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(msg),
          duration: Duration(seconds: 6),
          //backgroundColor: kErrorColor,
        ));
      }
    });
  }
}
