import 'package:ast_app/com/adk/ics/business/cart.dart';
import 'package:ast_app/com/adk/ics/business/salesorderlinesession.dart';
import 'package:ast_app/com/adk/ics/business/salesordersession.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkmiscstringfunctions.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/environmentlocale.dart';
import 'package:ast_app/com/adk/utility/exception/functionalwarningexception.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:ast_app/com/adk/utility/widget/messagecard.dart';
import 'package:flutter/material.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkbottomnavigationbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/cartlineitemwidget.dart';
import 'package:shoppingcart_app/com/adk/sc/ordertilewidget.dart';

class OrderPage extends StatefulWidget {
  SalesOrderSession salesOrderSession = new SalesOrderSession();
  bool isEdit = false;
  String? snackBarMessage;
  late VoidCallback actionRefresh;

  OrderPage(SalesOrderSession aSalesOrderSession, bool isEditable,
      VoidCallback anActionRefresh,
      {String? snackBarMessage}) {
    this.salesOrderSession = aSalesOrderSession;
    this.isEdit = isEditable;
    this.actionRefresh = anActionRefresh;
    this.snackBarMessage = snackBarMessage;
  }

  OrderPageState createState() => OrderPageState(this.salesOrderSession,
      this.isEdit, this.actionRefresh, this.snackBarMessage);
}

class OrderPageState extends State<OrderPage> {
  SalesOrderSession salesOrderSession = new SalesOrderSession();
  bool _isLoading = false;
  bool isEdit = false;
  late Future<SalesOrderSession?> myFuture;
  String? snackBarMessage;
  late VoidCallback actionRefresh;
  GlobalKey<FormState> _formKey =
      GlobalKey<FormState>(debugLabel: '_OrderPageState.formKey');
  GlobalKey orderWidgetKey = new GlobalKey();
  TextEditingController customerPOController = TextEditingController();
  bool placeOrderButtonPressed = false;

  OrderPageState(SalesOrderSession aSalesOrderSession, bool isEdittable,
      VoidCallback anActionRefresh, String? snackBarMessage) {
    this.salesOrderSession = aSalesOrderSession;
    this.isEdit = isEdittable;
    this.actionRefresh = anActionRefresh;
    this.snackBarMessage = snackBarMessage;
  }

  @override
  void initState() {
    super.initState();
    if (!this.isEdit) {
      // Assign that variable your Future.
      myFuture = _fetchSalesOrderSession();
      myFuture.then((value) {
        if (value != null) {
          this.salesOrderSession = value;
        }
      });
    } else {
      this.customerPOController =
          TextEditingController(text: this.salesOrderSession.getCustomerPO());
    }
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  int _selectedIndex = 0;

  _showSnackBar(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (this.snackBarMessage != null) {
        String msg = this.snackBarMessage!;
        this.snackBarMessage = null;
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(msg),
          duration: Duration(seconds: 6),
        ));
      }
    });
  }

  Widget build(BuildContext context) {
    Widget aBody = this.getBody(context);
    aBody = ADKGlobal.scaffoldFullPage(context, aBody);
    Widget aScaffold = Scaffold(
      key: scaffoldKey,
      appBar: ADKAppBar.buildBar(context, 'Order'),
      body: Stack(children: <Widget>[aBody, this._showCircularProgress()]),
      bottomNavigationBar: ADKBottomNavigationBar.buildBar(context),
    );
    Widget aTop = GestureDetector(
        onTap: // () => FocusManager.instance.primaryFocus?.unfocus(),
            () {
          //FocusScope.of(context).unfocus();
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: aScaffold);
    //return aTop;
    return aScaffold;
  }

  Widget getBody(BuildContext context) {
    if (this.isEdit) {
      Widget aWidget = _salesOrderWidget(this.salesOrderSession);
      aWidget = Form(key: _formKey, child: aWidget);
      return aWidget;
    } else {
      Widget aBody = FutureBuilder<SalesOrderSession?>(
        future: this.myFuture,
        builder: (context, snapshot) {
          this._showSnackBar(context);
          if (snapshot.hasData) {
            SalesOrderSession? data = snapshot.data;
            if (data == null) {
              return MessageCardFunctionalWarning('Order does not exist');
            } else {
              return _salesOrderWidget(data);
            }
          } else if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          return Center(child: CircularProgressIndicator());
        },
      );

      return aBody;
    }
  }

  Widget _salesOrderWidget(SalesOrderSession aSalesOrderSession) {
    //Assign sales order to cart.  FYI - It is not necessarily in progress yet
    if (this.isEdit) {
      Cart aCart = UserData.getUser().getCart();
      aCart.assignSalesOrderSession(aSalesOrderSession);
    }

    void Function() aRefresh = () {
      this.actionRefresh();
      //});
    };

    Widget aShipToCardWidget = OrderTileWidget(
        salesOrderSession: aSalesOrderSession,
        isEdit: this.isEdit,
        isAddProductMode: false,
        actionRefresh: this.actionRefresh,
        key: orderWidgetKey);

    List<Widget> aList = List.empty(growable: true);
    aList.add(
      aShipToCardWidget,
    );

    if (this.isEdit) {
      aList.add(this.customerPOInput());
      aList.add(this.saveButton());
    }

    Widget aLines =
        this._orderLinesListView(aSalesOrderSession.getSalesOrderLines());

    aList.add(
      Expanded(child: aLines),
    );
    return Column(children: aList);
  }

  Widget saveButton() {
    ElevatedButton aButton = ElevatedButton(
        onPressed: () {
          if (!this.placeOrderButtonPressed) {
            this.placeOrderButtonPressed = true;
            saveCart();
          }
        },
        child: Text('Update Order'));
    return aButton;
  }

  void saveCart() async {
    if (_formKey.currentState!.validate()) {
      Cart aCart = UserData.getUser().getCart();
      if (aCart.belowMinAndPromptForConfirmation()) {
        // set up the buttons
        Widget noButton = TextButton(
          child: Text('Return to Order'),
          onPressed: () {
            setState(() {
              this._isLoading = false;
            });
            Navigator.of(context).pop(false);
          },
        );
        Widget yesButton = TextButton(
          child: Text('Continue'),
          onPressed: () async {
            this.setState(() {
              this._isLoading = true;
            });
            Navigator.of(context).pop(true);
            ADKURLBuilder()
                .saveSalesOrderSession(this.salesOrderSession)
                .then((SalesOrderSession aSalesOrder) {
              UserData.getUser().getCart().resetCart();
              setState(() {
                this._isLoading = false;
                this.actionRefresh();
              });
              this.isEdit = false;
              Route route = MaterialPageRoute(
                  builder: (context) => OrderPage(
                      aSalesOrder, false, this.actionRefresh,
                      snackBarMessage: 'Order for ' +
                          UserData.getLocale().displayDateTimeFormat(
                              aSalesOrder.getDeliveryDate(),
                              EnvironmentLocale.DAYMONTHYYYY) +
                          ' was updated'));
              Navigator.of(context).pushAndRemoveUntil(route, (route) => false);
            }).onError((error, stackTrace) {
              this.placeOrderButtonPressed = false;
              setState(() {
                _isLoading = false;
              });
              String aMessageText =
                  'There was a problem saving this order.  Please contact customer service.';
              if (UserData.getUser().isSystemAdminType()) {
                aMessageText = aMessageText +
                    ' ' +
                    error.toString() +
                    '. ' +
                    stackTrace.toString();
              }
              ADKDialog2.showAlertDialogText(context, aMessageText);
            });
          },
        );

        showDialog(
          useRootNavigator: false,
          context: context,
          builder: (context) {
            return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return Cart.buildConfirmSaveDialog(
                    context, noButton, yesButton);
              },
            );
          },
        ).then((value) {
          //Esc or 'No' button pressed
          if (value == null || !value) {
            this.placeOrderButtonPressed = false;
          }
        });
      } else {
        this.setState(() {
          this._isLoading = true;
        });
        ADKURLBuilder()
            .saveSalesOrderSession(this.salesOrderSession)
            .then((SalesOrderSession aSalesOrder) {
          this._isLoading = false;
          UserData.getUser().getCart().resetCart();
          setState(() {
            this._isLoading = false;
            this.actionRefresh();
          });
          this.isEdit = false;
          Route route = MaterialPageRoute(
              builder: (context) => OrderPage(
                  aSalesOrder, false, this.actionRefresh,
                  snackBarMessage: 'Order for ' +
                      UserData.getLocale().displayDateTimeFormat(
                          aSalesOrder.getDeliveryDate(),
                          EnvironmentLocale.DAYMONTHYYYY) +
                      ' was updated'));
          Navigator.of(context).pushAndRemoveUntil(route, (route) => false);
        }).onError((error, stackTrace) {
          this.placeOrderButtonPressed = false;
          setState(() {
            _isLoading = false;
          });
          String aMessageText;
          if (error is FunctionalWarningException) {
            aMessageText = error.toString();
          } else {
            aMessageText =
                'There was a problem saving this order.  Please contact customer service.';
          }
          if (UserData.getUser().isSystemAdminType()) {
            aMessageText = aMessageText +
                ' ' +
                error.toString() +
                '. ' +
                stackTrace.toString();
          }
          ADKDialog2.showAlertDialogText(context, aMessageText);
        });
      }
    } else {
      this.placeOrderButtonPressed = false;
    }
  }

  Future<SalesOrderSession?> _fetchSalesOrderSession() {
    ADKURLBuilder aURLBuilder = new ADKURLBuilder();
    Future<SalesOrderSession?> aSalesOrderSession =
        aURLBuilder.fetchSalesOrderSession(
            this.salesOrderSession.getCompanyKey(),
            this.salesOrderSession.getBranchKey(),
            this.salesOrderSession.getSalesOrderKey(),
            this.salesOrderSession.getCustomerKey(),
            this.salesOrderSession.getShipToKey(),
            this.salesOrderSession.getDeliveryDate());
    return aSalesOrderSession;
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  final ScrollController _controller = ScrollController();

  Widget _orderLinesListView(List<SalesOrderLineSession> aSalesOrderLineList) {
    if (!ADKGlobal.isDesktop()) {
      Widget aList = ListView.builder(
          itemCount: aSalesOrderLineList.length,
          itemBuilder: (context, index) {
            Widget aCard = _orderLineTile(
              context,
              aSalesOrderLineList[index],
            );
            return aCard;
          });
      return aList;
    } else {
      Widget aList = ListView.builder(
          padding: EdgeInsets.fromLTRB(0, 0, 16, 0),
          controller: _controller,
          itemCount: aSalesOrderLineList.length,
          itemBuilder: (context, index) {
            Widget aCard = _orderLineTile(
              context,
              aSalesOrderLineList[index],
            );
            return aCard;
          });
      //aList = Padding(padding: EdgeInsets.fromLTRB(0, 0, 16, 0), child: aList);

      aList = Scrollbar(
          controller: _controller,
          //hoverThickness: 15,
          trackVisibility: false,
          thickness: 15,
          thumbVisibility: true,
          child: aList);
      return aList;
    }
  }

  Widget _orderLineTile(
      BuildContext context, SalesOrderLineSession aSalesOrderLine) {
    void Function() aRemoveFunction;
    void Function() anAddFunction;
    if (this.isEdit) {
      aRemoveFunction = () {
        //setState(() {
        this.actionRefresh();
        //});
      };
      anAddFunction = () {
        //setState(() {
        this.actionRefresh();
        //});
      };
    } else {
      aRemoveFunction = () {
        //setState(() {
        this.actionRefresh();
        //});
      };
      anAddFunction = () {
        //setState(() {
        this.actionRefresh();
        //});
      };
    }

    void Function() aRefreshFunction = () {
      orderWidgetKey.currentState!.setState(() {});
      //setState(() {});
      this.actionRefresh();
    };

    return CartLineItemWidget(
        key: GlobalKey(debugLabel: 'orderPage.cartLineItemWidget'),
        aForOrderEdit: this.isEdit,
        aForOrderInquiry: !this.isEdit,
        anItem: aSalesOrderLine,
        aRemoveFunction: aRefreshFunction,
        anAddFunction: aRefreshFunction,
        anActionRefresh: aRefreshFunction,
        aForChooseProduct: false);
  }

  Widget customerPOInput() {
    return Container(
        margin: EdgeInsets.all(10),
        child: TextFormField(
          //autofocus: true,
          //onFieldSubmitted: (value) {
          //  _submit();
          //},
          textCapitalization: TextCapitalization.characters,
          validator: validateCustomerPO,
          controller: customerPOController,
          //style: TextStyle(fontSize: 30),
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Purchase Order Number',
            errorStyle: TextStyle(
              fontSize: 18.0,
            ),
          ),
          onSaved: (String? val) {
            if (val != null) {
              //this.loadPlanKey = val;
            }
          },
        ));
  }

  String? validateCustomerPO(String? value) {
    bool isRequired = UserData.getUser().getCart().isCustomerPORequired();
    if (isRequired && ADKMiscStringFunctions.isBlankOrWhitespace(value)) {
      setState(() {
        this._isLoading = false;
      });
      return 'Please specify your purchase order number';
    }
    String aCustomerPO = customerPOController.text;
    this.salesOrderSession.setCustomerPO(aCustomerPO);
    return null;
  }
}
