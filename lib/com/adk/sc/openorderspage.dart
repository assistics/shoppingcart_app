import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:flutter/material.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkbottomnavigationbar.dart';
import 'package:shoppingcart_app/com/adk/sc/orderlistview.dart';

class OpenOrdersPage extends StatefulWidget {
  String? snackBarMessage;
  late VoidCallback actionRefresh;

  OpenOrdersPage(VoidCallback anActionRefresh, {String? snackBarMessage}) {
    this.actionRefresh = anActionRefresh;
    this.snackBarMessage = snackBarMessage;
  }

  @override
  _OpenOrdersPageState createState() =>
      _OpenOrdersPageState(this.actionRefresh, this.snackBarMessage);
}

class _OpenOrdersPageState extends State<OpenOrdersPage> {
  bool _isLoading = false;
  String? snackBarMessage;
  late VoidCallback actionRefresh;

  _OpenOrdersPageState(VoidCallback anActionRefresh, this.snackBarMessage) {
    this.actionRefresh = anActionRefresh;
    this.snackBarMessage = snackBarMessage;
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    Widget aBody = this.getBody();
    aBody = ADKGlobal.scaffoldFullPage(context, aBody);
    return Scaffold(
      appBar: ADKAppBar.buildBar(context, 'Open Orders'),
      body: aBody,
      bottomNavigationBar: ADKBottomNavigationBar.buildBar(context),
    );
  }

  Widget getBody() {
    OrderListView aList =
        new OrderListView(this.actionRefresh, this.snackBarMessage);
    return aList;
  }
}
