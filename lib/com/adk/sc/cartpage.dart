import 'package:ast_app/com/adk/ics/business/cart.dart';
import 'package:ast_app/com/adk/ics/business/cbsi.dart';
import 'package:ast_app/com/adk/ics/business/productsession.dart';
import 'package:ast_app/com/adk/ics/business/salesorderlinesession.dart';
import 'package:ast_app/com/adk/ics/business/salesordersession.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkmiscstringfunctions.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkbottomnavigationbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/adktheme.dart';
import 'package:shoppingcart_app/com/adk/sc/cartlineitemwidget.dart';
import 'package:shoppingcart_app/com/adk/sc/orderpage.dart';
import 'package:shoppingcart_app/com/adk/sc/selectcbspage.dart';
import 'package:shoppingcart_app/com/adk/sc/shiptocardwidget.dart';

class CartPage extends StatefulWidget {
  late VoidCallback actionRefresh;

  CartPage(VoidCallback anActionFresh) {
    this.actionRefresh = anActionFresh;
  }

  @override
  _CartPageState createState() => _CartPageState(this.actionRefresh);
}

class _CartPageState extends State<CartPage> {
  bool _isLoading = false;
  late VoidCallback actionRefresh;
  TextEditingController customerPOController = TextEditingController();

  _CartPageState(VoidCallback anActionRefresh) {
    this.actionRefresh = anActionRefresh;
  }

  final ScrollController _controller = ScrollController();

  final _formKey = GlobalKey<FormState>();
  bool placeOrderButtonPressed = false;

  @override
  Widget build(BuildContext context) {
    Cart aCart = UserData.getUser().getCart();

    int aNumberOfProducts = aCart.getNumberOfSalesOrderLinesInCart();
    if (aNumberOfProducts == 0) {
      Widget aButtonBar = OverflowBar(
        children: <Widget>[
          TextButton(
            child: const Text('Start Order'),
            onPressed: () {
              Cart aCart = UserData.getUser().getCart();

              if (aCart.isCartInProgress()) {
                ADKDialog2.showAlertDialogText(
                    context, 'A cart has already been started');
              } else {
                aCart.resetCart();
                Route route = MaterialPageRoute(
                    builder: (context) => SelectCBSPage(this.actionRefresh));
                Navigator.push(context, route);
              }
            },
          ),
        ],
      );
      Widget aMessage =
          ADKGlobal.messageYellow(context, 'Your Cart Is Empty', aButtonBar);

      Widget aBody = aMessage;
      aBody = ADKGlobal.scaffoldThinPage(context, aBody);

      return Scaffold(
          appBar: ADKAppBar.buildBar(context, 'My Cart'),
          body: aBody,
          bottomNavigationBar: ADKBottomNavigationBar.buildBar(context));
    } else {
      GlobalKey shipToCardWidgetKey = new GlobalKey(
          debugLabel: 'ChooseProductScreenState.shipToCardWidgetKey');
      CBSI? aShipToCard = aCart.getCBS();
      Widget aShipToCardWidget;
      if (aShipToCard != null) {
        aShipToCardWidget = new ShipToCardWidget(
            key: shipToCardWidgetKey,
            cbs: aShipToCard,
            isCheckOut: true,
            actionRefresh: this.actionRefresh);
      } else {
        aShipToCardWidget = Text('');
      }
      Widget aListWidget = ListView(
          controller: _controller,
          padding: EdgeInsets.fromLTRB(2, 0, 2, 0),
          children: this.buildCartItems(shipToCardWidgetKey));

      if (ADKGlobal.isDesktop()) {
        aListWidget = Scrollbar(
            controller: _controller,
            //hoverThickness: 15,
            trackVisibility: false,
            thickness: 15,
            thumbVisibility: true,
            child: aListWidget);
      }
      aListWidget = Expanded(child: aListWidget);
      Widget aColumn = Column(children: <Widget>[
        aShipToCardWidget,
        this.customerPOInput(),
        this.proceedToCheckOutButton(),
        aListWidget,
      ]);

      aColumn = Form(key: _formKey, child: aColumn);

      Widget aBody =
          Stack(children: <Widget>[aColumn, this._showCircularProgress()]);
      aBody = ADKGlobal.scaffoldFullPage(context, aBody);

      return Scaffold(
          appBar: ADKAppBar.buildBar(context, 'My Cart'),
          body: aBody,
          bottomNavigationBar: ADKBottomNavigationBar.buildBar(context));
    }
  }

  Widget proceedToCheckOutButton() {
    String aText;
    if (UserData.getUser().getCart().isCartInUpdateMode()) {
      aText = 'Update Order';
    } else {
      aText = 'Place Order';
    }
    Widget aButton = ElevatedButton(
        onPressed: () {
          if (!this.placeOrderButtonPressed) {
            this.placeOrderButtonPressed = true;
            saveCart();
          }
        },
        child: Text(aText));
    aButton = Padding(padding: EdgeInsets.fromLTRB(0, 0, 0, 0), child: aButton);
    return aButton;
  }

  String? validateCustomerPO(String? value) {
    bool isRequired = UserData.getUser().getCart().isCustomerPORequired();
    if (isRequired && ADKMiscStringFunctions.isBlankOrWhitespace(value)) {
      setState(() {
        this._isLoading = false;
      });
      return 'Please specify your purchase order number';
    }
    String aCustomerPO = customerPOController.text;
    UserData.getUser().getCart().setCustomerPO(aCustomerPO);
    return null;
  }

  void initState() {
    super.initState();
    SalesOrderSession? aSalesOrderSession =
        UserData.getUser().getCart().getSalesOrderSession();
    if (aSalesOrderSession != null) {
      this.customerPOController =
          TextEditingController(text: aSalesOrderSession.getCustomerPO());
    }
  }

  Widget customerPOInput() {
    return Container(
        margin: EdgeInsets.fromLTRB(4, 3, 4, 2),
        child: TextFormField(
          autofocus: true,
          //onFieldSubmitted: (value) {
          //  _submit();
          //},
          textCapitalization: TextCapitalization.characters,
          validator: validateCustomerPO,
          controller: customerPOController,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Purchase Order Number',
            errorStyle: TextStyle(
              fontSize: 18.0,
            ),
          ),
          onSaved: (String? val) {
            if (val != null) {
              //this.loadPlanKey = val;
            }
          },
        ));
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  void saveCart() async {
    if (_formKey.currentState!.validate()) {
      Cart aCart = UserData.getUser().getCart();
      if (aCart.belowMinAndPromptForConfirmation()) {
        // set up the buttons
        Widget noButton = TextButton(
          child: Text('Return to Order'),
          onPressed: () {
            setState(() {
              this._isLoading = false;
            });
            Navigator.of(context).pop(false);
          },
        );
        Widget yesButton = TextButton(
          child: Text('Continue'),
          onPressed: () async {
            setState(() {
              this._isLoading = true;
              //this.actionRefresh();
            });
            Navigator.of(context).pop(true);
            ADKURLBuilder()
                .saveSalesOrderCart()
                .then((SalesOrderSession aSalesOrder) {
              this.setState(() {
                this._isLoading = false;
                this.actionRefresh();
              });
              Route route = MaterialPageRoute(
                  builder: (context) =>
                      OrderPage(aSalesOrder, false, this.actionRefresh));
              Navigator.of(context).pushReplacement(route);
            }).onError((error, stackTrace) {
              this.placeOrderButtonPressed = false;
              setState(() {
                _isLoading = false;
              });
              String aMessageText =
                  'There was a problem saving this order.  Please contact customer service.';
              if (UserData.getUser().isSystemAdminType()) {
                aMessageText = aMessageText +
                    ' ' +
                    error.toString() +
                    '. ' +
                    stackTrace.toString();
              }
              ADKDialog2.showAlertDialogText(context, aMessageText);
            });
          },
        );

        showDialog(
          useRootNavigator: false,
          context: context,
          builder: (context) {
            return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return Cart.buildConfirmSaveDialog(
                    context, noButton, yesButton);
              },
            );
          },
        ).then((value) {
          //No button pressed
          if (value == null || !value) {
            this.placeOrderButtonPressed = false;
          }
        });
      } else {
        this.setState(() {
          this._isLoading = true;
        });
        ADKURLBuilder()
            .saveSalesOrderCart()
            .then((SalesOrderSession aSalesOrder) {
          this.setState(() {
            this._isLoading = false;
            this.actionRefresh();
          });
          Route route = MaterialPageRoute(
              builder: (context) =>
                  OrderPage(aSalesOrder, false, this.actionRefresh));
          Navigator.of(context).pushReplacement(route);
        }).onError((error, stackTrace) {
          this.placeOrderButtonPressed = false;
          setState(() {
            _isLoading = false;
          });
          String aMessageText =
              'There was a problem saving this order.  Please contact customer service.';
          if (UserData.getUser().isSystemAdminType()) {
            aMessageText = aMessageText +
                ' ' +
                error.toString() +
                '. ' +
                stackTrace.toString();
          }
          ADKDialog2.showAlertDialogText(context, aMessageText);
        });
      }
    } else {
      this.placeOrderButtonPressed = false;
    }
  }

  List<SalesOrderLineSession> getItemsInCart() {
    Cart aCart = UserData.getUser().getCart();
    List<SalesOrderLineSession> _cart = aCart.getCartList();
    return _cart;
  }

  Cart getCart() {
    return UserData.getUser().getCart();
  }

  List<Widget> buildCartItems(GlobalKey shipToCardWidgetKey) {
    List<SalesOrderLineSession> items = this.getItemsInCart();
    var widgets = <Widget>[];
    if (items.isNotEmpty) {
      void Function() aRemoveFunction = () {
        shipToCardWidgetKey.currentState?.setState(() {});
        this.actionRefresh();
      };
      void Function() anAddFunction = () {
        shipToCardWidgetKey.currentState?.setState(() {});
        this.actionRefresh();
      };
      void Function() aRefreshFunction = () {
        shipToCardWidgetKey.currentState?.setState(() {});
        this.actionRefresh();
      };
      for (var i = 0; i < items.length; i++) {
        Widget aCard = CartLineItemWidget(
          key: GlobalKey(debugLabel: 'cartPage.cartLineItemWidget'),
          aForOrderEdit: false,
          aForOrderInquiry: false,
          anItem: items[i],
          aRemoveFunction: aRemoveFunction,
          anAddFunction: anAddFunction,
          anActionRefresh: aRefreshFunction,
          aForChooseProduct: false,
        );
        widgets.add(aCard);
      }
    }
    return widgets;
  }

  Widget _buildCartCard(ProductSession item) {
    List<SalesOrderLineSession> _cart = this.getItemsInCart();
    return Card(
      elevation: 4.0,
      child: ListTile(
        leading:
            Icon(Icons.local_shipping, color: ADKTheme.CARD_CARTCARD_COLOR),
        title: Text(item.getProductKey()),
        trailing: GestureDetector(
            child: Icon(
              Icons.remove_circle,
              color: Colors.red,
            ),
            onTap: () {
              setState(() {
                _cart.remove(item);
              });
            }),
      ),
    );
  }
}

class OpenFlutterButton extends StatelessWidget {
  final double? width;
  final double? height;
  final Function? onPressed;
  final String? title;
  final IconData? icon;
  final double iconSize;
  final Color backgroundColor;
  final Color textColor;
  final Color borderColor;

  OpenFlutterButton({
    Key? key,
    this.width,
    this.height,
    this.title,
    this.onPressed,
    this.icon,
    this.backgroundColor = AppColors.red,
    this.textColor = AppColors.white,
    this.borderColor = AppColors.red,
    this.iconSize = 18.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _theme = Theme.of(context);
    EdgeInsetsGeometry edgeInsets = EdgeInsets.all(0);
    if (width == null || height == null) {
      edgeInsets = EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0);
    }
    return Padding(
      padding: edgeInsets,
      child: InkWell(
        onTap: () => onPressed!(),
        child: Container(
          width: width,
          height: height,
          padding: edgeInsets,
          decoration: BoxDecoration(
              color: backgroundColor,
              border: Border.all(color: borderColor),
              borderRadius: BorderRadius.circular(AppSizes.buttonRadius),
              boxShadow: [
                BoxShadow(
                    color: backgroundColor.withOpacity(0.3),
                    blurRadius: 4.0,
                    offset: Offset(0.0, 5.0)),
              ]),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _buildIcon(_theme),
                _buildTitle(_theme),
              ],
            ),
          ),
        ),
      ),
    );
    /*RaisedButton(
      onPressed: onPressed,
      shape: new RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(
          AppSizes.buttonRadius
        )
      ),
      child: Container(
        alignment: Alignment.center,
        width: width,
        height: height,
        child: Text(title,
          style: _theme.textTheme.button.copyWith(
            backgroundColor: _theme.textTheme.button.backgroundColor,
            color: _theme.textTheme.button.color
          )
        )
      )
    );*/
  }

  Widget _buildTitle(ThemeData _theme) {
    return Text(
      title!,
      style: _theme.textTheme.labelLarge!.copyWith(
        backgroundColor: _theme.textTheme.labelLarge!.backgroundColor,
        color: textColor,
      ),
    );
  }

  Widget _buildIcon(ThemeData theme) {
    if (icon != null) {
      return Padding(
        padding: const EdgeInsets.only(
          right: 8.0,
        ),
        child: Icon(
          icon,
          size: iconSize,
          color: theme.textTheme.labelLarge!.color,
        ),
      );
    }

    return SizedBox();
  }
}
