import 'dart:io';

import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkbottomnavigationbar.dart';

class PDFPage extends StatefulWidget {
  late File file;

  PDFPage(File aFile) {
    this.file = aFile;
  }

  _PDFPageState createState() => _PDFPageState(this.file);
}

class _PDFPageState extends State<PDFPage> {
  bool _isLoading = false;
  late File file;

  _PDFPageState(File aFile) {
    this.file = aFile;
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    Widget aScaffold = Scaffold(
      key: scaffoldKey,
      appBar: ADKAppBar.buildBar(context, 'Invoice'),
      body: Stack(children: <Widget>[
        Center(child: this.getBody()),
        this._showCircularProgress()
      ]),
      bottomNavigationBar: ADKBottomNavigationBar.buildBar(context), //    )
    );
    return aScaffold;
  }

  Widget getBody() {
    String path = this.file.path;
    return SingleChildScrollView(
      child: Stack(alignment: Alignment.center, children: [
        Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: InteractiveViewer(
              minScale: 0.2,
              maxScale: 10,
              child: PDFView(
                filePath: path,
              ),
            )

//        child: PdfView(
//          path: path,
//        ),
            ),
        this.shareButton(),
      ]),
    );
  }

  Widget shareButton() {
    Icon aShareIcon;
    IconData anIconData;
    bool isIOS = Theme.of(context).platform == TargetPlatform.iOS;
    if (isIOS) {
      anIconData = Icons.ios_share;
    } else {
      anIconData = Icons.share;
    }
    aShareIcon = Icon(anIconData, color: Colors.white);
    return Positioned(
      top: 0.0,
      right: 0.0,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ElevatedButton(
            style: ButtonStyle(
                backgroundColor:
                    WidgetStateProperty.all(Colors.black.withOpacity(0.05))),
            //Colors.black.withOpacity(0.05)
            onPressed: () => this.share(),
            child: aShareIcon),
      ),
    );
  }

  share() {
    String aPath = this.file.path;

    Share.shareXFiles([XFile(aPath)]);
  }

  Widget getBodyX() {
    String path = this.file.path;
    return Center(
        child: Container(
      height: 300.0,
      child: PDFView(
        filePath: path,
      ),
    ));
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }
}
