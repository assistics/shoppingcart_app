import 'package:ast_app/com/adk/ics/business/userpaymentmethod.dart';
import 'package:flutter/material.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkbottomnavigationbar.dart';

class PaymentMethodEdit extends StatefulWidget {
  UserPaymentMethod paymentMethod = UserPaymentMethod();

  PaymentMethodEdit(UserPaymentMethod anObject) {
    this.paymentMethod = anObject;
  }

  @override
  State<StatefulWidget> createState() {
    return PaymentMethodEditState(this.paymentMethod);
  }
}

class PaymentMethodEditState extends State<PaymentMethodEdit> {
  UserPaymentMethod paymentMethod = UserPaymentMethod();
  bool isEdit = false;
  String _cardNumber = '';
  String _expiryDate = '';
  String _cardHolderName = '';
  String _email = '';
  String _cvvCode = '';
  bool isCvvFocused = false;
  bool _isLoading = true;
  bool _rememberCardInfo = false;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  PaymentMethodEditState(UserPaymentMethod anObject) {
    this.paymentMethod = anObject;
  }

  void initState() {
    super.initState();
    this.asyncMethod();
  }

  static final String _PREF_REMEMBER_CARD_INFO = 'remembercreditcardinfo';
  static final String _PREF_CARD_NUMBER = 'cardnumber';
  static final String _PREF_CARD_HOLDER_NAME = 'cardholdername';
  static final String _PREF_EXPIRY_DATE = 'expirydate';
  static final String _PREF_CVVCODE = 'cvvcode';

  void asyncMethod() async {
    _isLoading = true;
    if (this.isEdit) {}
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_isLoading) {
      return Scaffold(
        appBar: ADKAppBar.buildBar(context, 'Payment'),
        body: _showCircularProgress(),
        bottomNavigationBar: ADKBottomNavigationBar.buildBar(context), //    )
      );
    } else {
      TextStyle aStyle = TextStyle(fontSize: 24);

      Widget aCardWidget = CreditCardWidget(
        height: 160,
        cardNumber: this._cardNumber,
        expiryDate: this._expiryDate,
        cardHolderName: this._cardHolderName,
        cvvCode: this._cvvCode,
        showBackView: this.isCvvFocused,
        obscureCardNumber: true,
        obscureCardCvv: true,
        onCreditCardWidgetChange: (CreditCardBrand) {},
      );
      Widget aCardForm = CreditCardForm(
        formKey: formKey,
        obscureCvv: true,
        obscureNumber: true,
        cardNumber: this._cardNumber,
        cvvCode: this._cvvCode,
        cardHolderName: this._cardHolderName,
        expiryDate: this._expiryDate,
        //themeColor: Colors.blue,
        inputConfiguration: const InputConfiguration(
            cardNumberDecoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Number',
              hintText: 'XXXX XXXX XXXX XXXX',
            ),
            expiryDateDecoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Expired Date',
              hintText: 'XX/XX',
            ),
            cvvCodeDecoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'CVV',
              hintText: 'XXX',
            ),
            cardHolderDecoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Card Holder',
            )),
        onCreditCardModelChange: onCreditCardModelChange,
      );

      Widget aStoreCreditCardInfo = CheckboxListTile(
        title: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            'Remember Card Information',
          ),
        ),
        controlAffinity: ListTileControlAffinity.leading,
        value: this._rememberCardInfo,
        onChanged: (bool? newValue) {
          setState(() {
            if (newValue != null) {
              this._rememberCardInfo = newValue;
            }
          });
        },
      );

      return Scaffold(
          appBar: ADKAppBar.buildBar(context, 'Payment'),
          body: SafeArea(
            child: Column(
              children: <Widget>[
                SizedBox(height: 10),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        aCardWidget,
                        aCardForm,
                        aStoreCreditCardInfo,
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          bottomNavigationBar: ADKBottomNavigationBar.buildBar(context));
    }
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  void onCreditCardModelChange(CreditCardModel? creditCardModel) async {
    setState(() {
      _cardNumber = creditCardModel!.cardNumber;
      _expiryDate = creditCardModel.expiryDate;
      _cardHolderName = creditCardModel.cardHolderName;
      _cvvCode = creditCardModel.cvvCode;
      isCvvFocused = creditCardModel.isCvvFocused;
    });
  }
}
