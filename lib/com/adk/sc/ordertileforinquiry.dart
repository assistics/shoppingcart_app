import 'package:ast_app/com/adk/ics/business/cart.dart';
import 'package:ast_app/com/adk/ics/business/salesordersession.dart';
import 'package:ast_app/com/adk/utility/adkcompare.dart';
import 'package:ast_app/com/adk/utility/adkdatetime.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/environmentlocale.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/adktheme.dart';
import 'package:shoppingcart_app/com/adk/sc/chooseproductpage.dart';
import 'package:shoppingcart_app/com/adk/sc/openorderspage.dart';
import 'package:shoppingcart_app/com/adk/sc/orderpage.dart';
import 'package:shoppingcart_app/com/adk/utility/widget/ordertile.dart';

class OrderTileForInquiry extends OrderTile {
  bool isAddProductMode = false;

  OrderTileForInquiry(
      {required SalesOrderSession salesOrderSession,
      required bool isEdittable,
      required this.isAddProductMode,
      required VoidCallback anActionRefresh,
      Key? key})
      : super(
            salesOrderSession: salesOrderSession,
            isEdit: isEdittable,
            actionRefresh: anActionRefresh,
            key: key) {
//    this.isAddProductMode = isAddProductMode;
  }

  Widget orderTile(BuildContext context, SalesOrderSession aSalesOrder) {
    Color? aColor;
    Icon aLeadingIcon = Icon(
      Icons.local_shipping,
      color: ADKTheme.BUTTON_ICONCOLOR,
    );

    Widget lines = this.buildTileTitle(context, aSalesOrder);
    bool allowAddProduct;
    if (ADKGlobal.isWideScreen(context)) {
      allowAddProduct = false;
    } else {
      allowAddProduct = true;
    }
    Card aCard = Card(
        color: aColor,
        child: ListTile(
          title: lines,
          trailing: this.menu(context, allowAddProduct),
        ));
    return aCard;
  }

  Widget menu(BuildContext context, bool allowAddProduct) {
    int CANCEL_ORDER = 0;
    int EDIT_ORDER = 1;
    int ADD_PRODUCT = 2;
    int CANCEL_CHANGES = 3;

    bool isCancelOrderEnabled = this.salesOrderSession.isChangeable();
    bool isEditOrderEnabled = isCancelOrderEnabled;

    bool isAddProductEnabled = isCancelOrderEnabled;

    bool isCancelChangesEnabled = isCancelOrderEnabled;

    List<PopupMenuEntry<int>> aList = List.empty(growable: true);
    if (!this.isEdit) {
      if (!this.isAddProductMode) {
        if (allowAddProduct) {
          aList.add(PopupMenuItem(
            value: EDIT_ORDER,
            enabled: isEditOrderEnabled,
            child: Row(children: [
              Icon(
                Icons.edit,
                color: ADKTheme.BUTTON_ICONCOLOR,
              ),
              Text(
                'Edit Order',
              )
            ]),
          ));
        }
      }
    } else {
      if (allowAddProduct) {
        aList.add(PopupMenuItem(
          value: ADD_PRODUCT,
          enabled: isAddProductEnabled,
          child: Row(children: [
            Icon(
              Icons.add,
              color: ADKTheme.BUTTON_ICONCOLOR,
            ),
            Text(
              'Add Product',
            )
          ]),
        ));
      }
      if (UserData.getUser().getCart().isCartInProgress()) {
        aList.add(PopupMenuItem(
          value: CANCEL_CHANGES,
          enabled: isCancelChangesEnabled,
          child: Row(children: [
            Icon(
              Icons.clear,
              color: ADKTheme.BUTTON_ICONCANCELCOLOR,
            ),
            Text(
              'Cancel Changes',
            )
          ]),
        ));
      }
    }
    aList.add(PopupMenuItem(
      value: CANCEL_ORDER,
      enabled: isCancelOrderEnabled,
      child: Row(children: [
        Icon(
          Icons.cancel,
          color: ADKTheme.BUTTON_ICONCANCELCOLOR,
        ),
        Text(
          'Cancel Order',
        )
      ]),
    ));

    Widget aMenu = PopupMenuButton<int>(
      elevation: 4,
      onSelected: (int result) {
        if (result == CANCEL_ORDER) {
          if (!isCancelOrderEnabled) {
            ADKDialog2.showAlertDialogText(
                context, 'This order can no longer be canceled at this time.');
          } else {
            confirmCancelOrderPopup(context);
          }
        } else if (result == CANCEL_CHANGES) {
          if (!isCancelChangesEnabled) {
            ADKDialog2.showAlertDialogText(
                context, 'This order can no longer be changed at this time.');
          } else {
            showDialog(
              useRootNavigator: false,
              context: context,
              builder: (context) {
                return StatefulBuilder(
                  builder: (BuildContext context, StateSetter setState) {
                    return this.buildCancelChangesDialog(
                        context, this.salesOrderSession, setState);
                  },
                );
              },
            );
          }
        } else if (result == EDIT_ORDER) {
          if (!isEditOrderEnabled) {
            if (this.salesOrderSession.isCBSOnHold()) {
              ADKDialog2.showAlertDialogText(
                  context, ADKGlobal.scContactCustomerService());
            } else {
              ADKDialog2.showAlertDialogText(
                  context, 'This order cannot be changed at this time.');
            }
          } else {
            Cart aCart = UserData.getUser().getCart();
            if (aCart.isCartInProgress()) {
              ADKDialog2.showAlertDialogText(context,
                  'An existing order cannot be changed while a cart is in progress.');
            } else {
              aCart.resetCart();
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => OrderPage(
                      this.salesOrderSession, true, this.actionRefresh)));
            }
          }
        } else if (result == ADD_PRODUCT) {
          if (!isAddProductEnabled) {
            if (this.salesOrderSession.isCBSOnHold()) {
              ADKDialog2.showAlertDialogText(
                  context, ADKGlobal.scContactCustomerService());
            } else {
              ADKDialog2.showAlertDialogText(
                  context, 'This order cannot be changed at this time.');
            }
          } else {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => ChooseProductScreen(this.actionRefresh)));
          }
        }
      },
      itemBuilder: (context) => aList,
      child: Container(
        child: Icon(
          Icons.more_vert,
          color: ADKTheme.BUTTON_ICONCOLOR,
        ),
      ),
    );

    return aMenu;
  }

  AlertDialog buildCancelChangesDialog(BuildContext context,
      SalesOrderSession aSalesOrderSession, StateSetter setState) {
    // set up the buttons
    Widget noButton = TextButton(
      child: Text('No'),
      onPressed: () {
        setState(() {
          this.isLoading = false;
        });
        Navigator.of(context).pop();
      },
    );
    Widget yesButton = TextButton(
      child: Text('Yes'),
      onPressed: () {
        setState(() {
          this.isLoading = true;
          this.actionRefresh();
        });
        //Attempt to cancel the appointment
        try {
          UserData.getUser().getCart().resetCart();
          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => OpenOrdersPage(this.actionRefresh,
                  snackBarMessage: 'Changes were canceled')));
        } catch (all) {
          setState(() {
            this.isLoading = false;
          });
          ADKDialog2.showAlertDialog(context, all, 2);
        }
      },
    );

    AlertDialog aConfirmCancelDialog = AlertDialog(
      title: Text("Confirm"),
      content: Stack(children: [
        Text("Are you sure you want to lose the changes to this order?"),
        _showCircularProgress()
      ]),
      actions: [
        noButton,
        yesButton,
      ],
    );

    return aConfirmCancelDialog;
  }

  void confirmCancelOrderPopup(BuildContext context) {
    showDialog(
      useRootNavigator: false,
      context: context,
      builder: (context) {
        String contentText = "Content of Dialog";
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
            return this
                .buildCancelDialog(context, this.salesOrderSession, setState);
          },
        );
      },
    );
  }

  void orderCannotBeCanceledPopup(BuildContext context) {
    showDialog(
      useRootNavigator: false,
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Text('This order can no longer be canceled.'),
          actions: <Widget>[
            TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('Close')),
          ],
        );
      },
    );
  }

  AlertDialog buildCancelDialog(BuildContext context,
      SalesOrderSession aSalesOrderSession, StateSetter setState) {
    // set up the buttons
    Widget noButton = TextButton(
      child: Text('No'),
      onPressed: () {
        setState(() {
          this.isLoading = false;
        });
        Navigator.of(context).pop();
      },
    );
    Widget yesButton = TextButton(
      child: Text('Yes'),
      onPressed: () {
        setState(() {
          //_errorMessage = "";
          this.isLoading = true;
        });
        //Attempt to cancel the appointment
        try {
          ADKDateTime? aDeliveryDate = aSalesOrderSession.getDeliveryDate();
          ADKURLBuilder()
              .cancelSalesOrder(aSalesOrderSession)
              .then((aSalesOrderSession) {
            setState(() {
              this.isLoading = false;
              Cart aCart = UserData.getUser().getCart();
              if (aCart.isCartInProgress()) {
                SalesOrderSession? anInProgressSalesOrder =
                    aCart.getSalesOrderSession();
                if (anInProgressSalesOrder != null) {
                  if (ADKCompare.equals(aSalesOrderSession.getSalesOrderKey(),
                      anInProgressSalesOrder.getSalesOrderKey())) {
                    //Only reset the cart if its the same order we are canceling
                    aCart.resetCart();
                  }
                }
              }
            });
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => OpenOrdersPage(this.actionRefresh,
                    snackBarMessage: 'Order for ' +
                        UserData.getLocale().displayDateTimeFormat(
                            aDeliveryDate, EnvironmentLocale.DAYMONTHYYYY) +
                        ' has been canceled')));
          }).catchError((all) {
            setState(() {
              this.isLoading = false;
            });
            ADKDialog2.showAlertDialog(context, all, 2);
          });
        } catch (all) {
          setState(() {
            this.isLoading = false;
          });
          ADKDialog2.showAlertDialog(context, all, 2);
        }
      },
    );

    AlertDialog aConfirmCancelDialog = AlertDialog(
      title: Text("Confirm"),
      content: Stack(children: [
        Text("Are you sure you want to cancel this order?"),
        _showCircularProgress()
      ]),
      actions: [
        noButton,
        yesButton,
      ],
    );

    return aConfirmCancelDialog;
  }

  Widget _showCircularProgress() {
    if (isLoading) {
      //return Center(child: CircularProgressIndicator());
      return CircularProgressIndicator();
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }
}
