import 'package:ast_app/com/adk/ics/business/cart.dart';
import 'package:ast_app/com/adk/ics/business/icsuser.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/adkmiscstringfunctions.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shoppingcart_app/com/adk/sc/accountpage.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/cartpage.dart';
import 'package:shoppingcart_app/com/adk/sc/loginpage.dart';
import 'package:shoppingcart_app/com/adk/sc/openinvoicespage.dart';
import 'package:shoppingcart_app/com/adk/sc/openorderspage.dart';
import 'package:shoppingcart_app/com/adk/sc/paymentmethodspage.dart';
import 'package:shoppingcart_app/com/adk/sc/selectcbspage.dart';
import 'package:shoppingcart_app/com/adk/sc/sidemenubadge.dart';
import 'package:shoppingcart_app/com/adk/sc/weekopenorderspage.dart';
import 'package:shoppingcart_app/com/adk/utility/adkscglobal.dart';
import 'package:url_launcher/url_launcher_string.dart';

class MainPage extends StatefulWidget {
  String title = '';

  MainPage(String title);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int _selectedIndex = 0;
  int MYWEEK_IDX = 0;
  int CART_IDX = 0;
  int CREATEORDER_IDX = 0;
  int OPENORDERS_IDX = 0;
  int OPENINVOICES_IDX = 0;
  int PAYMENTMETHODS_IDX = 0;
  int PRICESHEET_IDX = 0;
  int ACCOUNT_IDX = 0;
  int LOGOUT_IDX = 0;
  int _lastIndex = 0;
  final GlobalKey<State<StatefulWidget>> cartBadgeKey =
      GlobalKey<State<StatefulWidget>>(
          debugLabel: '_MainPageState.cartBadgeKey');
  final GlobalKey<State<StatefulWidget>> cartBadgeKey2 =
      GlobalKey<State<StatefulWidget>>(
          debugLabel: '_MainPageState.cartBadgeKey2');
  final GlobalKey<State<StatefulWidget>> cartBadgeKey3 =
      GlobalKey<State<StatefulWidget>>(
          debugLabel: '_MainPageState.cartBadgeKey3');
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    Widget? aBody;
    Widget? aBottomNav;

    TextStyle aStyle = TextStyle(fontSize: 14);
    List<NavigationRailDestination> aDestinationList;
    if (ADKGlobal.useLeftMenu(context)) {
      int i = -1;
      aDestinationList = <NavigationRailDestination>[];

      i++;
      MYWEEK_IDX = i;
      NavigationRailDestination myWeek = NavigationRailDestination(
        icon: Icon(
          Icons.calendar_view_week,
        ),
        selectedIcon: Icon(
          Icons.calendar_view_week_outlined,
          color: Colors.blue,
        ),
        label: Text('My Week', style: aStyle),
      );
      aDestinationList.add(myWeek);
      i++;
      CART_IDX = i;

      Cart aCart = UserData.getUser().getCart();

      Widget aCartWidget = Stack(
        key: cartBadgeKey,
        alignment: Alignment.topCenter,
        children: <Widget>[
          Icon(
            Icons.shopping_cart,
            //size: 36.0,
          ),
          if (aCart.isCartInProgress() &&
              aCart.getNumberOfSalesOrderLinesInCart() > 0)
            Padding(
              padding: const EdgeInsets.only(left: 2.0, top: 0),
              child: CircleAvatar(
                radius: 8.0,
                backgroundColor: Colors.red,
                foregroundColor: Colors.white,
                child: Text(
                  aCart.getNumberOfSalesOrderLinesInCart().toString(),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 12.0,
                  ),
                ),
              ),
            ),
        ],
      );
      NavigationRailDestination cart = NavigationRailDestination(
        icon: SideMenuBadgeWidget(cartBadgeKey2, false),
        selectedIcon: SideMenuBadgeWidget(cartBadgeKey3, true),
        label: Text('Cart', style: aStyle),
      );
      aDestinationList.add(cart);

      i++;
      CREATEORDER_IDX = i;
      NavigationRailDestination createOrder = NavigationRailDestination(
        icon: Icon(
          Icons.add_circle_outline,
        ),
        selectedIcon: Icon(
          Icons.add_circle_outline_outlined,
          color: Colors.blue,
        ),
        label: Text('Create Order', style: aStyle),
      );
      aDestinationList.add(createOrder);

      i++;
      OPENORDERS_IDX = i;
      NavigationRailDestination openOrders = NavigationRailDestination(
        icon: Icon(Icons.shopping_bag),
        selectedIcon: Icon(
          Icons.shopping_bag_outlined,
          color: Colors.blue,
        ),
        label: Text('Open Orders', style: aStyle),
      );
      aDestinationList.add(openOrders);

      i++;
      OPENINVOICES_IDX = i;
      NavigationRailDestination openInvoices = NavigationRailDestination(
        icon: Icon(Icons.payments),
        selectedIcon: Icon(
          Icons.payments_outlined,
          color: Colors.blue,
        ),
        label: Text('Open Invoices', style: aStyle),
      );
      aDestinationList.add(openInvoices);

      if (ADKSCGlobal.scIsPaymentMethodEnabled()) {
        i++;
        PAYMENTMETHODS_IDX = i;
        NavigationRailDestination paymentMethods = NavigationRailDestination(
          icon: Icon(Icons.payment),
          selectedIcon: Icon(
            Icons.payment_outlined,
            color: Colors.blue,
          ),
          label: Text('Payment Methods', style: aStyle),
        );
        aDestinationList.add(paymentMethods);
      } else {
        PAYMENTMETHODS_IDX = -99;
      }

      i++;
      ACCOUNT_IDX = i;
      NavigationRailDestination account = NavigationRailDestination(
        icon: Icon(Icons.account_box),
        selectedIcon: Icon(
          Icons.account_box_outlined,
          color: Colors.blue,
        ),
        label: Text('Account', style: aStyle),
      );
      aDestinationList.add(account);

      if (ADKGlobal.scMenuPriceSheet()) {
        i++;
        PRICESHEET_IDX = i;
        String aLabel = ADKGlobal.scMenuPriceSheetLabel();
        NavigationRailDestination priceSheet = NavigationRailDestination(
          icon: Icon(Icons.reorder),
          selectedIcon: Icon(
            Icons.reorder_outlined,
            color: Colors.blue,
          ),
          label: Text(aLabel, style: aStyle),
        );
        aDestinationList.add(priceSheet);
      } else {
        PRICESHEET_IDX = -99;
      }

      i++;
      LOGOUT_IDX = i;
      NavigationRailDestination logout = NavigationRailDestination(
        icon: Icon(Icons.logout),
        selectedIcon: Icon(
          Icons.logout_outlined,
          color: Colors.blue,
        ),
        label: Text('Log Out', style: aStyle),
      );
      aDestinationList.add(logout);
    } else {
      aDestinationList = <NavigationRailDestination>[];
    }

    List<Widget> aList = <Widget>[];
    if (ADKGlobal.useLeftMenu(context)) {
      NavigationRail aNavRail = NavigationRail(
        backgroundColor: Colors.white,
        selectedIndex: _selectedIndex,
        onDestinationSelected: (index) {
          if (index == LOGOUT_IDX) {
            Route route = MaterialPageRoute(builder: (context) => LoginPage());
            Navigator.pushReplacement(context, route);
          } else if (index == PRICESHEET_IDX) {
            this._lastIndex = this._selectedIndex;
            setState(() => this._selectedIndex = index);
          } else {
            setState(() => this._selectedIndex = index);
          }
        },
        labelType: NavigationRailLabelType.all,
        destinations: aDestinationList,
      );
      aList.add(aNavRail);
    }
    aList.add(const VerticalDivider(thickness: 1, width: 1));
    // This is the main content.
    aList.add(Expanded(child: buildPages()));

    aBody = Row(children: aList);
    PreferredSizeWidget? anAppBar;
    anAppBar = this.buildAppBar(context);

    Widget aScaffold = Scaffold(
      key: scaffoldKey,
      appBar: anAppBar,
      body: aBody,
      bottomNavigationBar: aBottomNav,
    );
    return aScaffold;
  }

  Widget buildPages() {
    ICSUser aUser = UserData.getUser();
    if (!aUser.isPersistentFlag) {
      _selectedIndex = LOGOUT_IDX;
    }

    void Function() aRefreshBadge = () {
      cartBadgeKey.currentState?.setState(() {});
      cartBadgeKey2.currentState?.setState(() {});
      cartBadgeKey3.currentState?.setState(() {});
    };

    if (_selectedIndex == CREATEORDER_IDX) {
      Cart aCart = aUser.getCart();
      if (aCart.isCartInProgress()) {
        _selectedIndex = CART_IDX;
      } else {
        aCart.resetCart();
      }
    }
    if (_selectedIndex == MYWEEK_IDX) {
      widget.title = 'My Week';
      final myWeekKey = GlobalKey<NavigatorState>();
      return ClipRect(
          key: myWeekKey,
          child: Navigator(
            onGenerateRoute: (settings) => MaterialPageRoute(
              builder: (context) => WeekOpenOrdersPage(aRefreshBadge),
            ),
          ));
    } else if (_selectedIndex == CART_IDX) {
      widget.title = 'Cart';
      final openOrdersKey = GlobalKey<NavigatorState>();
      return ClipRect(
          child: Navigator(
        key: openOrdersKey,
        onGenerateRoute: (settings) => MaterialPageRoute(
          builder: (context) => CartPage(aRefreshBadge),
        ),
      ));
    } else if (_selectedIndex == OPENORDERS_IDX) {
      widget.title = 'Open Orders';
      final openOrdersKey = GlobalKey<NavigatorState>();
      return ClipRect(
          child: Navigator(
        key: openOrdersKey,
        onGenerateRoute: (settings) => MaterialPageRoute(
          builder: (context) => OpenOrdersPage(aRefreshBadge),
        ),
      ));
    } else if (_selectedIndex == ACCOUNT_IDX) {
      widget.title = 'Account';
      final openOrdersKey = GlobalKey<NavigatorState>();
      return ClipRect(
          child: Navigator(
        key: openOrdersKey,
        onGenerateRoute: (settings) => MaterialPageRoute(
          builder: (context) => AccountPage(),
        ),
      ));
    } else if (_selectedIndex == CREATEORDER_IDX) {
      widget.title = 'Create Order';
      final createOrderKey = GlobalKey<NavigatorState>();
      return ClipRect(
          child: Navigator(
        key: createOrderKey,
        onGenerateRoute: (settings) => MaterialPageRoute(
          builder: (context) => SelectCBSPage(aRefreshBadge),
        ),
      ));
    } else if (_selectedIndex == PAYMENTMETHODS_IDX) {
      widget.title = 'Payment Methods';
      final paymentMethodsKey = GlobalKey<NavigatorState>();
      return ClipRect(
          child: Navigator(
        key: paymentMethodsKey,
        onGenerateRoute: (settings) => MaterialPageRoute(
          builder: (context) => PaymentMethodsPage(),
        ),
      ));
    } else if (_selectedIndex == OPENINVOICES_IDX) {
      widget.title = 'Open Invoices';
      final openInvoicesKey = GlobalKey<NavigatorState>();
      return ClipRect(
          child: Navigator(
        key: openInvoicesKey,
        onGenerateRoute: (settings) => MaterialPageRoute(
          builder: (context) => OpenInvoicesPage(),
        ),
      ));
    } else if (_selectedIndex == PRICESHEET_IDX) {
      String url = ADKURLBuilder().getSCPriceSheetReportURL();
      if (url == '') {
        setState(() => this._selectedIndex = _lastIndex);
        return Center(child: Text('Your account has not be fully set up'));
      } else {
        launchUrlString(url, webOnlyWindowName: '_self');
        this._selectedIndex = _lastIndex;
        setState(() {
          this._selectedIndex = _lastIndex;
        });
        return buildPages();
      }
    } else if (_selectedIndex == LOGOUT_IDX) {
      Future.microtask(() => Navigator.of(context, rootNavigator: true)
              .pushReplacement(MaterialPageRoute(
            builder: (context) => LoginPage(),
          )));
      return Text('');
    } else {
      widget.title = 'Open Orders';
      return OpenOrdersPage(aRefreshBadge);
    }
  }

  PreferredSizeWidget? buildAppBar(BuildContext context) {
    if (ADKGlobal.useLeftMenu(context)) {
      PreferredSizeWidget? anAppBar = ADKAppBar(
        title: Text(widget.title),
      );

      Cart aCart = UserData.getUser().getCart();
      List<Widget> anActionRow = List.empty(growable: true);

      if (ADKGlobal.getSCAppLabel().isNotEmpty) {
//        anActionRow.add(Text(ADKGlobal.getSCAppLabel(),
        //          style: TextStyle(fontSize: 18, color: Colors.yellow)));
      }
      Widget aRow = Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
                padding: EdgeInsets.fromLTRB(100, 10, 0, 10),
                child: Image.asset(
                  ADKGlobal.getLogoFilename(),
                  fit: BoxFit.fill,
                  height: 125,
                )),
            //Container(padding: const EdgeInsets.all(8.0), child: this.title)
            Container(
                //padding: const EdgeInsets.all(50.0),
                padding: const EdgeInsets.fromLTRB(80, 0, 0, 0),
                child: Text(ADKGlobal.scWideScreenTitle(),
                    style: TextStyle(
                        fontSize: 30,
                        fontStyle: FontStyle.italic,
                        //fontFamily: GoogleFonts.courgette().fontFamily)))
                        fontFamily: GoogleFonts.josefinSans().fontFamily))),
          ]);

      String aQuickStartURL = ADKGlobal.getSCQuickStartHelpDocumentURL();
      if (!ADKMiscStringFunctions.isBlankOrWhitespace(aQuickStartURL)) {
        Widget aHelp = TextButton(
            child: Text('Quick Start'),
            onPressed: () {
              String url = aQuickStartURL;
              launchUrlString(url);
            });
        aHelp = Padding(
            padding: EdgeInsets.only(bottom: 20 /*, right: 50*/), child: aHelp);
        aHelp = Align(alignment: Alignment.bottomRight, child: aHelp);
        anActionRow.add(aHelp);
      }
      Widget aHelp = TextButton(
          child: Text('Help'),
          onPressed: () {
            String url = ADKGlobal.getSCHelpDocumentURL();
            launchUrlString(url);
          });
      aHelp = Padding(
          padding: EdgeInsets.only(bottom: 20 /*, right: 50*/), child: aHelp);
      aHelp = Align(alignment: Alignment.bottomRight, child: aHelp);

      anActionRow.add(aHelp);
      if (UserData.getUser().isSystemAdminType()) {
        Widget anAbout = TextButton(
            child: Text('About'),
            onPressed: () {
              showAboutDialog(
                  context: context,
                  applicationLegalese: ADKGlobal.getApplicationLegalese(),
                  applicationName: 'ASSISTics Cart',
                  applicationVersion: ADKGlobal.getPackageInfo().version +
                      ' (' +
                      ADKGlobal.getPackageInfo().buildNumber +
                      ')',
                  applicationIcon: Image.asset(
                    ADKGlobal.getLogoFilename(),
                    fit: BoxFit.contain,
                    //scale: bo,
                    height: 43,
                  ));
            });
        anAbout = Padding(
            padding: EdgeInsets.only(bottom: 20, right: 50), child: anAbout);
        anAbout = Align(alignment: Alignment.bottomRight, child: anAbout);
        anActionRow.add(anAbout);
      }

      aRow = Stack(children: [
        aRow, /*anAbout*/
      ]);

      Color? aBackgroundColor = Colors.white;

      PreferredSizeWidget aCartAppBar;
      aCartAppBar = AppBar(
        elevation: 0,
        bottom: PreferredSize(
            preferredSize: const Size.fromHeight(1.0),
            child: Container(
              color: Colors.grey,
              height: 1.0,
            )),
        //toolbarHeight: 125,
        toolbarHeight: 100,
        flexibleSpace: aRow,
        //backgroundColor: aBackgroundColor,
        actions: anActionRow,
      );
      return aCartAppBar;
    } else {
      return null;
    }
  }
}
