import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkbottomnavigationbar.dart';

class PasswordChangedPage extends StatefulWidget {
  PasswordChangedPage() {}

  @override
  _PasswordChangedPageAppState createState() => _PasswordChangedPageAppState();
}

class _PasswordChangedPageAppState extends State<PasswordChangedPage> {
  final logger = Logger();
  GlobalKey<FormState> _formKey = new GlobalKey();
  bool _isLoading = false;

  _PasswordChangedPageAppState() {}

  @override
  Widget build(BuildContext context) {
    String aTitle;
    aTitle = 'Password Changed';
    Widget aBody = this.getBody();
    aBody = ADKGlobal.scaffoldThinPage(context, aBody);
    return Scaffold(
      key: _formKey,
      appBar: ADKAppBar.buildBar(context, aTitle),
      body: Stack(children: [aBody, _showCircularProgress()]),
      bottomNavigationBar: ADKBottomNavigationBar.buildBar(context),
    );
  }

  Widget getBody() {
    Column aColumn = Column(children: [
      ADKGlobal.messageGreen(
          context, 'Your password was successfully changed.'),
    ]);
    return aColumn;
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }
}
