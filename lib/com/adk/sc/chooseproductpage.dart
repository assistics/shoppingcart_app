import 'package:ast_app/com/adk/ics/business/cart.dart';
import 'package:ast_app/com/adk/ics/business/cbsi.dart';
import 'package:ast_app/com/adk/ics/business/icsuser.dart';
import 'package:ast_app/com/adk/ics/business/productsession.dart';
import 'package:ast_app/com/adk/ics/business/salesordersession.dart';
import 'package:ast_app/com/adk/utility/adkcompare.dart';
import 'package:ast_app/com/adk/utility/adkdatetime.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkmiscstringfunctions.dart';
import 'package:ast_app/com/adk/utility/environmentlocale.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkbottomnavigationbar.dart';
import 'package:shoppingcart_app/com/adk/sc/cartlineitemwidget.dart';
import 'package:shoppingcart_app/com/adk/sc/ordertilewidget.dart';
import 'package:shoppingcart_app/com/adk/sc/selectcbspage.dart';
import 'package:shoppingcart_app/com/adk/sc/shiptocardwidget.dart';

class ChooseProductScreen extends StatefulWidget {
  late VoidCallback actionRefresh;

  ChooseProductScreen(VoidCallback anActionRefresh) {
    this.actionRefresh = anActionRefresh;
  }

  @override
  ChooseProductScreenState createState() =>
      ChooseProductScreenState(this.actionRefresh);
}

class ChooseProductScreenState extends State<ChooseProductScreen> {
  final logger = Logger();
  final GlobalKey<FormState> _formKey = new GlobalKey();

  //static GlobalKey shipToCardWidgetKey = new GlobalKey(debugLabel: 'ChooseProductScreenState.shipToCardWidgetKey');
  GlobalKey orderWidgetKey =
      new GlobalKey(debugLabel: 'ChooseProductScreenState.orderWidgetKey');
  String customerPO = "";
  String messageText = "";
  bool customerPOInvalid = false;
  String _searchText = '';
  static final String SORT_DESCRIPTION = "1";
  static final String SORT_PRODUCT = "2";
  static final String SORT_RECENTLYPURCHASED = "3";
  String _sortSetting = SORT_PRODUCT;

  static final String FILTER_ALL = "1";
  static final String FILTER_ONLYPURCHASED = "2";
  static final String FILTER_NOTPURCHASED = "3";
  String _filterSetting = FILTER_ALL;
  late VoidCallback actionRefresh;

  ChooseProductScreenState(VoidCallback anActionRefresh) {
    this.actionRefresh = anActionRefresh;
  }

  List<ProductSession> _products = List.empty(growable: true);

  @override
  Widget build(BuildContext context) {
    Cart aCart = UserData.getUser().getCart();
    CBSI? aCBS = aCart.getCBS();
    GlobalKey shipToCardWidgetKey = GlobalKey();
    if (aCBS != null) {
      if (aCBS.isOneCategoryPerOrder() && !aCart.hasCategoryBeenChosen()) {
        Widget aBody = this.chooseCategory(context);
        aBody = ADKGlobal.scaffoldThinPage(context, aBody);
        return aBody;
      }
    }
    Widget aBody = this._getBody(shipToCardWidgetKey);
    aBody = ADKGlobal.scaffoldFullPage(context, aBody);

    Widget aScaffold = Scaffold(
      key: _formKey,
      appBar: ADKAppBar.buildBar(context, 'Choose Products'),
      body: aBody,
      bottomNavigationBar: ADKBottomNavigationBar.buildBar(context), //    )
    );
    Widget aTop = GestureDetector(
        onTap: // () => FocusManager.instance.primaryFocus?.unfocus(),
            () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: aScaffold);
    return aScaffold;
  }

  Widget chooseCategory(BuildContext context) {
    Cart aCart = UserData.getUser().getCart();
    Widget aRow = Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          this.categoryWidget(new Icon(Icons.water, size: 90), 'Fluid'),
          SizedBox(
            width: 50,
          ),
          this.categoryWidget(
            new Icon(
              Icons.icecream,
              size: 90,
            ),
            'Frozen',
          ),
        ]);
    Widget aBody = ListView(children: <Widget>[
      SizedBox(
        height: 5,
      ),
      /*Padding(
            padding: EdgeInsets.only(left: 0),
            child:*/
      ADKGlobal.messageGreen(context, 'Choose a Product Category'),
      SizedBox(
        height: 10,
      ),
      aRow
    ]);

    return SizedBox(width: 300, child: aBody);
  }

  Widget categoryWidgetX(Widget aWidget, String aProductCategoryKey) {
    return Text(aProductCategoryKey);
  }

  Widget categoryWidget(Widget aWidget, String aProductCategoryKey) {
    Cart aCart = UserData.getUser().getCart();
    return ElevatedButton(
        onPressed: () {
          aCart.setProductCategory(aProductCategoryKey);
          setState(() {});
        },
        style: ElevatedButton.styleFrom(
            shape: const RoundedRectangleBorder(),
            backgroundColor: Colors.white,
            fixedSize: const Size(150, 200)),
        //TODO i18n
        child: Column(children: [aWidget, Text(aProductCategoryKey)]));
  }

  Widget _buildShipToCard(CBSI aShipToCard, GlobalKey shipToCardWidgetKey) {
    return new ShipToCardWidget(
        key: shipToCardWidgetKey,
        cbs: aShipToCard,
        isCheckOut: false,
        actionRefresh: this.actionRefresh);
  }

  Widget _getBody(GlobalKey shipToCardWidgetKey) {
    ICSUser aUser = UserData.getUser();
    Cart aCart = aUser.getCart();
    bool isReadyForProducts = aCart.cartReadyForProducts();
    if (isReadyForProducts) {
      return this.getList(shipToCardWidgetKey);
    } else {
      bool isShipToSpecified = aCart.hasCBS();
      Widget aShipToWidget = Center();
      if (isShipToSpecified) {
        CBSI? aShipToCard = aCart.getCBS();
        if (aShipToCard != null) {
          aShipToWidget =
              this._buildShipToCard(aShipToCard, shipToCardWidgetKey);
        }
      }
      List<Widget> children = List.empty(growable: true);
      children.add(aShipToWidget);
      double spacerHeight;
      if (aCart.isShipToMissingProducts()) {
        Widget aLeading = Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[Icon(Icons.info_outline, color: Colors.black)],
        );

        children.add(ADKGlobal.messageYellow(
            context,
            'Your product list is not yet setup. Please contact customer service.',
            null));
        spacerHeight = 100;
      } else {
        spacerHeight = 200;
      }
      children.add(SizedBox(
        height: spacerHeight,
      ));

      Widget aStartButton = ElevatedButton(
        child: Column(children: [
          Text(
            "Choose A Different Ship To",
          ),
        ]),
        onPressed: () {
          aCart.resetCart();
          Route route = MaterialPageRoute(
              builder: (context) => SelectCBSPage(this.actionRefresh));
          Navigator.of(context).push(route);
        },
      );

      children.add(aStartButton);
      Column aColumn = Column(
        children: children,
      );
      return aColumn;
    }
  }


  EnvironmentLocale getLocale() {
    return UserData.getLocale();
  }

  Cart getCart() {
    return UserData.getUser().getCart();
  }

  void _populateProducts() {
    ICSUser aUser = UserData.getUser();
    var list = aUser.getCart().getProductSessionArray();
    setState(() {
      _products = list;
    });
  }

  List<ProductSession> filterAndSortList() {
    Cart aCart = UserData.getUser().getCart();
    String aCategoryKey = aCart.getProductCategory();
    List<ProductSession> aFullListOfProducts = this._products;
    List<ProductSession> aFilteredList;
    bool checkCategory;
    if (ADKMiscStringFunctions.isBlankOrWhitespace(aCategoryKey)) {
      checkCategory = false;
    } else {
      checkCategory = true;
    }
    bool checkSearch;
    if (_searchText.isNotEmpty) {
      checkSearch = true;
    } else {
      checkSearch = false;
    }
    bool checkFilter;
    if (_filterSetting != FILTER_ALL) {
      checkFilter = true;
    } else {
      checkFilter = false;
    }

    if (checkSearch || checkFilter || checkCategory) {
      aFilteredList = List.empty(growable: true);
      for (ProductSession aProduct in aFullListOfProducts) {
        bool isAdd = true;
        if (isAdd && checkSearch) {
          String aDescription = aProduct.getProductDescription();
          String aDescriptionUPPER = aDescription.toUpperCase();
          String aSearchTextUPPER = _searchText.toUpperCase();
          if (!aDescriptionUPPER.contains(aSearchTextUPPER)) {
            String aProductKey = aProduct.getProductKey();
            String aProductKeyUPPER = aProductKey.toUpperCase();
            if (!aProductKeyUPPER.contains(aSearchTextUPPER)) {
              isAdd = false;
            }
          }
        }
        if (isAdd && checkFilter) {
          if (_filterSetting == FILTER_ONLYPURCHASED) {
            if (aProduct.getLastOrderDate() == null) {
              isAdd = false;
            }
          } else if (_filterSetting == FILTER_NOTPURCHASED) {
            if (aProduct.getLastOrderDate() != null) {
              isAdd = false;
            }
          }
        }
        if (isAdd && checkCategory) {
          if (!ADKCompare.equals(
              aCategoryKey, aProduct.getProductCategoryKey())) {
            isAdd = false;
          }
        }
        if (isAdd) {
          aFilteredList.add(aProduct);
        }
      }
    } else {
      aFilteredList = aFullListOfProducts;
    }

    //Sort
    if (this._isSortByDescription()) {
      aFilteredList.sort((a, b) =>
          a.getProductDescription().compareTo(b.getProductDescription()));
    } else if (_isSortByProduct()) {
      aFilteredList.sort((a, b) {
        String aProductKey1 = a.getProductKey();
        String aProductKey2 = b.getProductKey();
        int comp = aProductKey1.length.compareTo(aProductKey2.length);
        if (comp != 0) {
          return comp;
        }
        comp = a.getProductKey().compareTo(b.getProductKey());
        return comp;
      });
    } else if (_isSortByRecentlyPurchased()) {
      aFilteredList.sort((a, b) {
        ADKDateTime? aDate = a.getLastOrderDate();
        ADKDateTime? bDate = b.getLastOrderDate();
        if (aDate == null && bDate == null) {
          return a.getProductSessionId().compareTo(b.getProductSessionId());
        } else if (aDate == null) {
          return 1;
        } else if (bDate == null) {
          return -1;
        } else {
          //Descending
          int aC = -aDate.getUTCTime().compareTo(bDate.getUTCTime());
          if (aC == 0) {
            aC = a.getProductKey().compareTo(b.getProductKey());
          }
          return aC;
        }
      });
    }

    return aFilteredList;
  }

  bool _isSortByDescription() {
    return (this._sortSetting == SORT_DESCRIPTION);
  }

  bool _isSortByProduct() {
    return (this._sortSetting == SORT_PRODUCT);
  }

  bool _isSortByRecentlyPurchased() {
    return (this._sortSetting == SORT_RECENTLYPURCHASED);
  }

  Widget getList(GlobalKey shipToCardWidgetKey) {
    _populateProducts();

    Cart aCart = this.getCart();
    List<ProductSession> aListOfProducts = aCart.getProductSessionArray();
    if (aListOfProducts.isEmpty) {
      return ADKGlobal.messageYellow(
          context,
          'Your product list is not yet setup. Please contact customer service.',
          null);
    } else {
      Widget aColumn = Column();
      CBSI? aShipToCard = aCart.getCBS();
      Widget aShipToCardWidget = Text('');
      if (aShipToCard != null) {
        aShipToCardWidget = new ShipToCardWidget(
            key: shipToCardWidgetKey,
            cbs: aShipToCard,
            isCheckOut: false,
            actionRefresh: this.actionRefresh);
      } else {
        SalesOrderSession? aSalesOrderSession = aCart.getSalesOrderSession();
        if (aSalesOrderSession != null) {
          aShipToCardWidget = OrderTileWidget(
              salesOrderSession: aSalesOrderSession,
              isEdit: false,
              isAddProductMode: true,
              actionRefresh: this.actionRefresh,
              key: orderWidgetKey);
        }
      }
      aColumn = Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        aShipToCardWidget,
        searchBar(),
        Expanded(
            child: Row(children: [
          Expanded(child: _buildGridView(context, shipToCardWidgetKey))
        ]))
      ]);
      return Center(child: Container(child: aColumn));
    }
  }

  Widget searchBar() {
    Cart aCart = UserData.getUser().getCart();
    List<Widget> aList = List.empty(growable: true);
    aList.add(Expanded(child: this._searchField()));
    if (ADKGlobal.isWideScreen(context) && aCart.hasCategoryBeenChosen()) {
      aList.add(this._filterCategoryWidgetDesktop());
    }
    aList.add(Padding(
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        child: _filterWidget()));
    aList.add(Padding(
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        child: _sortWidget()));
    Widget aRow =
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: aList);
    return aRow;
  }

  Widget _searchField() {
    String aHintText;
    Cart aCart = UserData.getUser().getCart();
/*
    if (aCart.hasCategoryBeenChosen()) {
      aHintText = 'Search ' + aCart.getProductCategory() + ' Category...';
    } else {
      aHintText = 'Search...';
    }
 */
    aHintText = 'Search...';

    //this._searchIcon = new Icon(Icons.close);
    return new TextField(
      decoration: new InputDecoration(
          prefixIcon: new Icon(Icons.search), hintText: aHintText),
      onChanged: (value) {
        setState(() {
          this._searchText = value;
        });
      },
    );
  }

  final ScrollController _controller = ScrollController();
  double calculateAspectRatio() {
    double aChildAspectRatio;
    if (ADKGlobal.isWideScreen(context)) {
      aChildAspectRatio = 3;
    } else {
      //!!! Works well for choose product
      aChildAspectRatio = 1;

      var size = MediaQuery.of(context).size;

      /*24 is for notification bar on Android*/
      //final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
      final double itemHeight;
      if (ADKGlobal.scShowPrices()) {
        itemHeight = (size.height - kToolbarHeight - 100) / 2;
      } else {
        itemHeight = (size.height - kToolbarHeight - 150) / 2;
      }
      final double itemWidth = size.width / 2;
      aChildAspectRatio = itemWidth / itemHeight;
    }
    return aChildAspectRatio;
  }

  Widget _buildGridView(BuildContext context, GlobalKey shipToCardWidgetKey) {
    int xAxis = ADKGlobal.numberOfProductCardsInGrid(context);

    List<ProductSession> aListOfProducts = this.filterAndSortList();

    Cart aCart = this.getCart();
    List<Widget> aList = List.empty(growable: true);
    aListOfProducts.forEach((anItem) {
      void Function() aRemoveFunction = () {
        setState(() {
          this.actionRefresh();
        });
      };
      void Function() anAddFunction = () {
        setState(() {
          this.actionRefresh();
        });
      };
      void Function() aRefreshFunction = () {
        setState(() {
          this.actionRefresh();
        });
      };

      Widget aWidget = new CartLineItemWidget(
          //Each widget must have a unique key. Otherwise it breaks the updating of the quantity
          key: GlobalKey(debugLabel: 'chooseProductPage.cartLineItemWidget'),
          aForOrderEdit: false,
          aForOrderInquiry: false,
          anItem: anItem,
          aRemoveFunction: aRemoveFunction,
          anAddFunction: anAddFunction,
          anActionRefresh: aRefreshFunction,
          aForChooseProduct: true);
      aList.add(aWidget);
    });
    double aChildAspectRatio = calculateAspectRatio();
    EdgeInsetsGeometry aPadding;
    ScrollController? aController;
    if (ADKGlobal.isDesktop()) {
      aPadding = EdgeInsets.fromLTRB(0, 0, 16, 0);
      aController = _controller;
    } else {
      aPadding = EdgeInsets.all(4.0);
      aController = null;
    }
    Widget aGrid = GridView.count(
      controller: aController,
      childAspectRatio: aChildAspectRatio,
      //This was removed because it causes the screen to scroll back to the top on +/- press
      //key: GlobalKey(),
      padding: aPadding,
      crossAxisCount: xAxis,
      children: aList,
    );
    if (ADKGlobal.isDesktop()) {
      aGrid = Scrollbar(
          controller: _controller,
          //hoverThickness: 15,
          trackVisibility: false,
          thickness: 15,
          thumbVisibility: true,
          child: aGrid);
    }
    return aGrid;
  }

  Widget sortMenuItem(String aKey, String aLabel) {
    if (this._sortSetting == aKey) {
      return Row(children: [Icon(Icons.check), Text(aLabel)]);
    } else {
      return Padding(
          padding: EdgeInsets.fromLTRB(20, 0, 0, 0), child: Text(aLabel));
    }
  }

  Widget filterMenuItem(String aKey, String aLabel) {
    if (this._filterSetting == aKey) {
      return Row(children: [Icon(Icons.check), Text(aLabel)]);
    } else {
      return Padding(
          padding: EdgeInsets.fromLTRB(20, 0, 0, 0), child: Text(aLabel));
    }
  }

  Widget _sortWidget() {
    if (ADKGlobal.isWideScreen(context)) {
      return this._sortWidgetDesktop();
    } else {
      return this._sortWidgetMobile();
    }
  }

  Widget styleDropDownItem(String aText) {
    return Container(
      padding: EdgeInsets.all(4),
      decoration: BoxDecoration(
        border: Border.all(width: 1, color: Colors.lightBlueAccent),
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
      child: Text(
        aText,
        style: TextStyle(fontSize: 12),
      ),
    );
  }

  Widget _sortWidgetDesktop() {
    List<DropdownMenuItem<String>> aList = List.empty(growable: true);
    aList.add(DropdownMenuItem(
        value: SORT_DESCRIPTION,
        child: styleDropDownItem('Sort by: Description')));
    aList.add(DropdownMenuItem(
        value: SORT_PRODUCT,
        child: styleDropDownItem('Sort by: Product Number')));
    aList.add(DropdownMenuItem(
        value: SORT_RECENTLYPURCHASED,
        child: styleDropDownItem('Sort by: Recently Purchased')));
    Widget aMenu = DropdownButton(
      alignment: Alignment.center,
      isDense: true,
      underline: SizedBox(),
      // Initial Value
      value: this._sortSetting,
      // Down Arrow Icon
      icon: const Icon(Icons.keyboard_arrow_down),
      // Array list of items
      items: aList,
      // After selecting the desired option,it will
      // change button value to selected value
      onChanged: (String? newValue) {
        setState(() {
          this._sortSetting = newValue!;
        });
      },
    );
    return aMenu;
  }

  Widget _filterCategoryWidgetDesktop() {
    Cart aCart = UserData.getUser().getCart();
    return styleDropDownItem('Category: ' + aCart.getProductCategory());
  }

  Widget _filterWidgetDesktop() {
    List<DropdownMenuItem<String>> aList = List.empty(growable: true);
    aList.add(DropdownMenuItem(
        value: FILTER_ALL, child: styleDropDownItem('Show: All Products')));
    aList.add(DropdownMenuItem(
        value: FILTER_ONLYPURCHASED,
        child: styleDropDownItem('Show: Recently Purchased')));
    aList.add(DropdownMenuItem(
        value: FILTER_NOTPURCHASED,
        child: styleDropDownItem('Show: Not Purchased Recently')));
    Widget aMenu = DropdownButton(
      alignment: Alignment.center,
      isDense: true,
      underline: SizedBox(),
      // Initial Value
      value: this._filterSetting,
      // Down Arrow Icon
      icon: const Icon(Icons.keyboard_arrow_down),
      // Array list of items
      items: aList,
      // After selecting the desired option,it will
      // change button value to selected value
      onChanged: (String? newValue) {
        setState(() {
          this._filterSetting = newValue!;
        });
      },
    );
    return aMenu;
  }

  Widget _sortWidgetMobile() {
    List<PopupMenuEntry<String>> aList = List.empty(growable: true);

    aList.add(PopupMenuItem<String>(
      value: SORT_DESCRIPTION,
      child: sortMenuItem(SORT_DESCRIPTION, 'Sort By Description'),
    ));

    aList.add(PopupMenuItem<String>(
      value: SORT_PRODUCT,
      child: sortMenuItem(SORT_PRODUCT, 'Sort By Product Number'),
    ));

    aList.add(PopupMenuItem<String>(
      value: SORT_RECENTLYPURCHASED,
      child: sortMenuItem(SORT_RECENTLYPURCHASED, 'Sort By Recently Purchased'),
    ));

    Widget aSortList = PopupMenuButton<String>(
      icon: Icon(Icons.sort),
      itemBuilder: (BuildContext bc) => aList,
      onSelected: (String? newValueSelected) {
        setState(() {
          if (newValueSelected != null) {
            this._sortSetting = newValueSelected;
          }
        });
      },
    );
    return aSortList;
  }

  Widget _filterWidget() {
    if (ADKGlobal.isWideScreen(context)) {
      return this._filterWidgetDesktop();
    } else {
      return this._filterWidgetMobile();
    }
  }

  Widget _filterWidgetMobile() {
    List<PopupMenuEntry<String>> aList = List.empty(growable: true);

    aList.add(PopupMenuItem<String>(
      value: FILTER_ALL,
      child: filterMenuItem(FILTER_ALL, 'Show All'),
    ));

    aList.add(PopupMenuItem<String>(
      value: FILTER_ONLYPURCHASED,
      child: filterMenuItem(FILTER_ONLYPURCHASED, 'Show Recently Purchased'),
    ));

    aList.add(PopupMenuItem<String>(
      value: FILTER_NOTPURCHASED,
      child: filterMenuItem(FILTER_NOTPURCHASED, 'Show Not Recently Purchased'),
    ));

    Widget aSortList = PopupMenuButton<String>(
      icon: Icon(Icons.filter_alt),
      itemBuilder: (BuildContext bc) => aList,
      onSelected: (String? newValueSelected) {
        setState(() {
          if (newValueSelected != null) {
            this._filterSetting = newValueSelected;
          }
        });
      },
    );
    return aSortList;
  }
}

class RoundedIconButton extends StatelessWidget {
  RoundedIconButton(
      {required this.icon,
      required this.onPress,
      required this.iconSize,
      required this.fillColor,
      required this.color});

  final IconData icon;
  final Function onPress;
  final double iconSize;
  final Color fillColor;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      constraints: BoxConstraints.tightFor(width: iconSize, height: iconSize),
      elevation: 4.0,
      onPressed: () => onPress(),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(iconSize * 0.8)),
      //fillColor: Colors.white,
      fillColor: this.fillColor,
      child: Icon(
        icon,
        //color: Colors.black,
        color: this.color,
        size: iconSize * 0.8,
      ),
    );
  }
}
