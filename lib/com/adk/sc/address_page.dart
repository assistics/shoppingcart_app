import 'package:ast_app/com/adk/ics/business/cbsi.dart';
import 'package:ast_app/com/adk/ics/business/cbssession.dart';
import 'package:ast_app/com/adk/ics/business/icsuser.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/us_number_text_input_formatter.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';
import 'package:shoppingcart_app/com/adk/sc/accountpage.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkbottomnavigationbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adktheme.dart';
//import 'package:web_browser/web_browser.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';

class AddressPage extends StatefulWidget {
  late CBSI cbs;

  AddressPage(CBSI aCBS) {
    this.cbs = aCBS;
  }

  @override
  State<StatefulWidget> createState() => new _AddressPageState(this.cbs);
}

class _AddressPageState extends State<AddressPage> {
  CBSSession newCBS = CBSSession();
  CBSI oldCBS = CBSSession();
  final logger = Logger();
  final _formKey = new GlobalKey<FormState>();

  String _email = "";

  bool _isLoginForm = false;
  bool _isLoading = false;

  _AddressPageState(CBSI aCBS) {
    this.oldCBS = aCBS;
    this.newCBS.setCustomerKey(aCBS.getCustomerKey());
    this.newCBS.setBillToKey(aCBS.getBillToKey());
    this.newCBS.setShipToKey(aCBS.getShipToKey());
    this.newCBS.setShipToName(aCBS.getShipToName());
    this.newCBS.setShipToAddressLine1(aCBS.getAddressLine1());
    this.newCBS.setShipToAddressLine2(aCBS.getAddressLine2());
    this.newCBS.setShipToCity(aCBS.getCity());
    this.newCBS.setShipToState(aCBS.getState());
    this.newCBS.setShipToPostalCode(aCBS.getPostalCode());
    this.newCBS.setShipToTelephone(aCBS.getTelephone());
  }

  // Check if form is valid before perform login or signup
  bool validateEmail() {
    final FormState? form = _formKey.currentState;
    if (form != null) {
      if (form.validate()) {
        form.save();
        return true;
      }
    }
    return false;
  }

  bool initRan = false;
  bool saveButton = false;

  @override
  void initState() {
    //_errorMessage = "";
    this._isLoading = false;
    _isLoginForm = true;

    super.initState();
  }

  void resetForm() {
    FormState? aState = _formKey.currentState;
    if (aState != null) {
      aState.reset();
    }
    //_errorMessage = "";
  }

  @override
  Widget build(BuildContext context) {
    Widget aBody = this.getBody();
    aBody = ADKGlobal.scaffoldFullPage(context, aBody);
    return Scaffold(
      appBar: ADKAppBar.buildBar(context, 'Change Address'),
      body: SingleChildScrollView(
          child: Stack(
        children: <Widget>[
          aBody,
          _showCircularProgress(),
        ],
      )),
      bottomNavigationBar: ADKBottomNavigationBar.buildBar(context),
    );
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget getBody() {
    ICSUser aUser = UserData.getUser();
    List<Widget> aList = List.empty(growable: true);
    Widget aBody;
    if (!aUser.hasShipTos()) {
      aList.add(ADKGlobal.messageYellow(
          context,
          'Your ship to address is not yet setup. Please contact customer service.',
          null));
    } else {
      aList.add(ADKGlobal.messageGreen(
          context, 'Account #:' + this.newCBS.getShipToKey()));
      aList.add(_buildShipToCard());
    }
    aBody = Container(
        //height: 50, //
        width: 600, //MediaQuery.of(context).size.width ,//- 100,
        child: Column(children: aList));
    aBody = Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 8.0,
        vertical: 2.0,
      ),
      child: aBody,
    );

    if (ADKGlobal.isDesktop()) {
      //double aWidth = ADKGlobal.desktopCenterPaneFullWidth(context);
      aBody =
          Container(child: aBody, constraints: BoxConstraints(maxWidth: 600));
    }

    ADKGlobal.deviceWidth(context);
    return aBody;
  }

  Widget _buildShipToCard() {
    List<Widget> aColumn = List.empty(growable: true);

    Icon anIcon;
    Color? aTileColor;
    anIcon = Icon(
      Icons.location_on_outlined,
    );
    aTileColor = ADKTheme.CARD_CARTCARD_COLOR;
    ICSUser aUser = UserData.getUser();

    aColumn.add(Text('Account #: ' + this.newCBS.getShipToKey()));

    Widget aForm = Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          this._shipToNameField(),
          //this.spacer(),
          this._addressLine1Field(),
          //this.spacer(),
          this._addressLine2Field(),
          //this.spacer(),
          this._cityField(),
          //this.spacer(),
          this._stateField(),
          //this.spacer(),
          this._postalCodeField(),
          //this.spacer(),
          this._telephoneField(),
          //this.spacer(),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: ElevatedButton(
              onPressed: () {
                if (!this.saveButton) {
                  // Validate returns true if the form is valid, or false otherwise.
                  if (_formKey.currentState!.validate()) {
                    saveButton = true;
                    setState(() {
                      this._isLoading = true;
                    });
                    //Attempt to change password
                    ADKURLBuilder()
                        .requestShipToAddressChange(this.oldCBS, this.newCBS)
                        .then((value) {
                      ADKDialog2.showDialogText(
                          context,
                          '',
                          value.messageText,
                          () => {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => AccountPage()))
                              });
                    }).catchError((all) {
                      saveButton = false;
                      setState(() {
                        this._isLoading = false;
                      });
                      ADKDialog2.showAlertDialog(context, all, 1);
                    });
                  }
                }
              },
              child: const Text('Submit'),
            ),
          ),
        ],
      ),
    );
    return aForm;
  }

  Widget _spacer() {
    return SizedBox(
      //Use of SizedBox
      height: 1,
    );
  }

  Widget _shipToNameField() {
    String? Function(String?) aValidator = this.buildValidator(true, 0);
    String? Function(String?) anOnChanged = (String? aValue) {
      if (aValue == null) {
        aValue = "";
      }
      this.newCBS.setShipToName(aValue);
      return null;
    };
    return this.createField(
        true,
        //AppLocalizations.of(context)!.trailerNumber,
        'Account Name',
        TextInputType.name,
        TextCapitalization.words,
        aValidator,
        this.newCBS.getShipToName(),
        anOnChanged);
  }

  Widget _addressLine1Field() {
    String? Function(String?) aValidator = this.buildValidator(true, 0);
    String? Function(String?) anOnChanged = (String? aValue) {
      if (aValue == null) {
        aValue = "";
      }
      this.newCBS.setShipToAddressLine1(aValue);
      return null;
    };
    return this.createField(
        false,
        //AppLocalizations.of(context)!.trailerNumber,
        'Address Line 1',
        TextInputType.name,
        TextCapitalization.words,
        aValidator,
        this.newCBS.getAddressLine1(),
        anOnChanged);
  }

  Widget _addressLine2Field() {
    String? Function(String?) aValidator = this.buildValidator(false, 0);
    String? Function(String?) anOnChanged = (String? aValue) {
      if (aValue == null) {
        aValue = "";
      }
      this.newCBS.setShipToAddressLine2(aValue);
      return null;
    };
    return this.createField(
        false,
        //AppLocalizations.of(context)!.trailerNumber,
        'Address Line 2',
        TextInputType.name,
        TextCapitalization.words,
        aValidator,
        this.newCBS.getAddressLine2(),
        anOnChanged);
  }

  Widget _cityField() {
    String? Function(String?) aValidator = this.buildValidator(true, 0);
    String? Function(String?) anOnChanged = (String? aValue) {
      if (aValue == null) {
        aValue = "";
      }
      this.newCBS.setShipToCity(aValue);
      return null;
    };
    return this.createField(
        false,
        //AppLocalizations.of(context)!.trailerNumber,
        'City',
        TextInputType.name,
        TextCapitalization.words,
        aValidator,
        this.newCBS.getCity(),
        anOnChanged);
  }

  Widget _stateField() {
    String? Function(String?) aValidator = this.buildValidator(true, 0);
    String? Function(String?) anOnChanged = (String? aValue) {
      if (aValue == null) {
        aValue = "";
      }
      this.newCBS.setShipToState(aValue);
      return null;
    };
    return this.createField(
        false,
        //AppLocalizations.of(context)!.trailerNumber,
        'State',
        TextInputType.name,
        TextCapitalization.words,
        aValidator,
        this.newCBS.getState(),
        anOnChanged);
  }

  Widget _postalCodeField() {
    String? Function(String?) aValidator = this.buildValidator(true, 0);
    String? Function(String?) anOnChanged = (String? aValue) {
      if (aValue == null) {
        aValue = "";
      }
      this.newCBS.setShipToPostalCode(aValue);
      return null;
    };
    return this.createField(
        false,
        //AppLocalizations.of(context)!.trailerNumber,
        'Postal Code',
        TextInputType.name,
        TextCapitalization.words,
        aValidator,
        this.newCBS.getPostalCode(),
        anOnChanged);
  }

  Widget _telephoneField() {
    String? Function(String?) aValidator = this.buildValidator(true, 0);
    String? Function(String?) anOnChanged = (String? aValue) {
      if (aValue == null) {
        aValue = "";
      }
      this.newCBS.setShipToTelephone(aValue);
      return null;
    };
    return this.createField(
        false,
        //AppLocalizations.of(context)!.trailerNumber,
        'Telephone',
        TextInputType.phone,
        TextCapitalization.words,
        aValidator,
        this.newCBS.getTelephone(),
        anOnChanged);
  }

  Widget createField(
      bool hasfocus,
      String aLabelText,
      TextInputType anInputType,
      TextCapitalization aTextCapitalization,
      String? Function(String?) aValidator,
      String aDefault,
      String? Function(String?) onChanged) {
    return createField2(hasfocus, aLabelText, 16, anInputType,
        aTextCapitalization, aValidator, aDefault, onChanged);
  }

  Widget createField2(
      bool hasfocus,
      String aLabelText,
      double fontSize,
      TextInputType anInputType,
      TextCapitalization aTextCapitalization,
      String? Function(String?) aValidator,
      String aDefault,
      String? Function(String?) onChanged) {
    List<TextInputFormatter> inputFormatters;

    if (anInputType == TextInputType.phone) {
      inputFormatters = [USNumberTextInputFormatter()];
    } else {
      inputFormatters = List.empty();
    }
    return Container(
        margin: EdgeInsets.all(4),
        child: TextFormField(
          initialValue: aDefault,
          autofocus: hasfocus,
          keyboardType: anInputType,
          inputFormatters: inputFormatters,
          textCapitalization: aTextCapitalization,
          validator: aValidator,
          onChanged: onChanged,
          style: TextStyle(fontSize: fontSize),
          decoration: InputDecoration(contentPadding: EdgeInsets.fromLTRB(15, 1, 3, 1),
            enabledBorder:
                OutlineInputBorder(borderSide: BorderSide(color: Colors.black)),
            border: OutlineInputBorder(),
            labelText: aLabelText,
            errorBorder:
                OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
            focusedErrorBorder:
                OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
            errorStyle: TextStyle(
              fontSize: 16.0,
            ),
          ),
        ));
  }

  String? Function(String?) buildValidator(bool isRequired, int minChars) {
    String? Function(String?) aValidator = (String? aValue) {
      if (aValue == null) {
        return null;
      }
      if (isRequired && aValue.isEmpty) {
        //return AppLocalizations.of(context)!.valueIsRequired;
        return 'Value Is Required';
      }

      if (minChars > 0 && aValue.length < minChars) {
        //TODO i18n
        return 'Value must be at least ' + minChars.toString() + ' digits long';
      }
      return null;
    };
    return aValidator;
  }
}
