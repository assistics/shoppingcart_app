import 'package:ast_app/com/adk/ics/business/addressi.dart';
import 'package:ast_app/com/adk/ics/business/cart.dart';
import 'package:ast_app/com/adk/ics/business/cbsi.dart';
import 'package:ast_app/com/adk/ics/business/cbssession.dart';
import 'package:ast_app/com/adk/ics/business/icsuser.dart';
import 'package:ast_app/com/adk/ics/business/productsession.dart';
import 'package:ast_app/com/adk/ics/business/salesordersession.dart';
import 'package:ast_app/com/adk/utility/adkdatetime.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/environmentlocale.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/adktheme.dart';
import 'package:shoppingcart_app/com/adk/sc/cartpage.dart';
import 'package:shoppingcart_app/com/adk/sc/chooseproductpage.dart';
import 'package:shoppingcart_app/com/adk/sc/openorderspage.dart';
import 'package:shoppingcart_app/com/adk/sc/selectcbspage.dart';
import 'package:shoppingcart_app/com/adk/utility/adkscglobal.dart';

class ShipToCardWidget extends StatefulWidget {
  CBSI cbs = new CBSSession();
  bool isCheckOut = false;
  late VoidCallback actionRefresh;

  ShipToCardWidget(
      {Key? key,
      required this.cbs,
      required this.isCheckOut,
      required this.actionRefresh})
      : super(key: key);

  @override
  ShipToCardWidgetState createState() => ShipToCardWidgetState();
}

class ShipToCardWidgetState extends State<ShipToCardWidget> {
  final logger = Logger();
  String messageText = "";

  //CBSI cbs = new CBSSession();
  //bool isCheckOut = false;
  //late VoidCallback actionRefresh;

  ShipToCardWidgetState() {
    //this.cbs = aShipToCard;
    //this.isCheckOut = anIsCheckOut;
    //this.actionRefresh = anActionRefresh;
  }

  List<ProductSession> _products = List.empty(growable: true);

  @override
  Widget build(BuildContext context) {
    if (ADKGlobal.isWideScreen(context)) {
      return this.buildShipToWidgetWide(context);
    } else {
      return this.buildShipToWidgetSmall(context);
    }
  }

  Widget buildShipToWidgetSmall(BuildContext context) {
    CBSI aShipToCard = widget.cbs;
    ICSUser aUser = UserData.getUser();
    Cart aCart = aUser.getCart();
    ADKDateTime? aDeliveryDate = aCart.getDeliveryDate();
    Text? aSubText;
    if (aDeliveryDate != null) {
      aSubText = new Text(
        UserData.getLocale().displayDateTimeFormat(
            aDeliveryDate, EnvironmentLocale.DAYMONTHYYYY),
        style: TextStyle(fontWeight: FontWeight.w900, fontSize: 18),
      );
    }
    Widget aChangeWidgetShipTo;
    if (widget.isCheckOut) {
      aChangeWidgetShipTo = this.menu(context, true);
    } else {
      String aText;
      if (ADKGlobal.isWideScreen(context)) {
        aText = 'Change Ship To';
      } else {
        aText = 'Change';
      }
      aChangeWidgetShipTo = TextButton(
        child: Text(aText),
        onPressed: () {
          if (aCart.isCartInProgress()) {
            this.showAlertDialog(context);
          } else {
            aCart.resetCart();
            Route route = MaterialPageRoute(
                builder: (context) => SelectCBSPage(widget.actionRefresh));
            Navigator.of(context).push(route);
          }
        },
      );
    }

    CBSI anAddress = aShipToCard; //.getAddress();
    String anAddressText = anAddress.getAddressLine1() +
        ' ' +
        anAddress.getCity() +
        ' ' +
        anAddress.getPostalCode();
    ListTile aTile = ListTile(
        leading: Icon(Icons.location_on_outlined),
        trailing: aChangeWidgetShipTo,
        contentPadding: EdgeInsets.all(5.0),
        //dense:true,
        title:
            Text('Deliver to ' + aUser.getFirstName() + ' - ' + anAddressText),
        subtitle: aSubText);

    Widget aShipToWidget = Center(
        child: Card(
      shadowColor: Colors.blue,
      child: Stack(
        children: <Widget>[
          aTile,
          Align(
              alignment: Alignment.bottomRight,
              child: Expanded(child: aChangeWidgetShipTo)),
        ],
      ),
    ));
    aShipToWidget = Center(child: Card(shadowColor: Colors.blue, child: aTile));
    return aShipToWidget;
  }

  Widget buildShipToWidgetWide(BuildContext context) {
    CBSI aShipToCard = widget.cbs;
    ICSUser aUser = UserData.getUser();
    Cart aCart = aUser.getCart();
    SalesOrderSession? aSalesOrder = aCart.getSalesOrderSession();
    ADKDateTime? aDeliveryDate = aCart.getDeliveryDate();

    Widget aDeliveryDateWidget;
    if (aDeliveryDate != null) {
      aDeliveryDateWidget = Row(children: [
        Text('Delivery Date: ', style: TextStyle(fontWeight: FontWeight.bold)),
        Text(
          UserData.getLocale().displayDateTimeFormat(
              aDeliveryDate, EnvironmentLocale.DAYMONTHYYYY),
        ),
      ]);
    } else {
      aDeliveryDateWidget = Row(children: [
        Text('Delivery Date: ', style: TextStyle(fontWeight: FontWeight.bold)),
        Text(
          '',
        ),
      ]);
    }
    AddressI anAddress = aShipToCard.getShipToAddress();
    String anAddressLine1 = anAddress.getAddressLine1();
    if (anAddressLine1.trim().isEmpty) {
      anAddressLine1 = anAddress.getAddressLine2();
    }
    String anAddressLine = "";
    String? anAddressCity = anAddress.getCity();
    String? anAddressState = anAddress.getState();
    String? anAddressPostalCode = anAddress.getPostalCode();
    if ((anAddressCity.isNotEmpty ||
            anAddressState.isNotEmpty ||
            anAddressPostalCode.isNotEmpty)) {
      anAddressLine =
          // ", " +
          anAddressCity + ", " + anAddressState + " " + anAddressPostalCode;
    }
    Widget aPriceWidget;
    Widget aSpace = Text('');
    if (ADKGlobal.scShowPrices()) {
      SalesOrderSession aPriceSO;
      if (aSalesOrder == null) {
        aPriceSO = new SalesOrderSession();
      } else {
        aPriceSO = aSalesOrder;
      }
      aPriceWidget = Text('Total: ' +
          ADKSCGlobal.scDisplayTotalPrice(UserData.getLocale(), aPriceSO));
    } else {
      aPriceWidget = aSpace;
    }
    Map<int, TableColumnWidth> aMap = <int, TableColumnWidth>{
      //0: IntrinsicColumnWidth(),
      //1: FlexColumnWidth(),
      0: IntrinsicColumnWidth(),
      1: IntrinsicColumnWidth(),
      2: IntrinsicColumnWidth(),
    };

    String aText;
    if (ADKGlobal.isWideScreen(context)) {
      aText = 'Change Ship To';
    } else {
      aText = 'Change';
    }
    Widget aChangeWidgetShipTo;
    Widget aTrailingWidget;
    if (widget.isCheckOut) {
      //TODO Only use menu if mobile
      aTrailingWidget = this.menu(context, false);
      aChangeWidgetShipTo = aSpace;
    } else {
      aTrailingWidget = aSpace;
      if (aUser.hasMultipleShipTos()) {
        aChangeWidgetShipTo = TextButton(
          child: Text(aText),
          onPressed: () {
            if (aCart.isCartInProgress()) {
              this.showAlertDialog(context);
            } else {
              aCart.resetCart();
              Route route = MaterialPageRoute(
                  builder: (context) => SelectCBSPage(widget.actionRefresh));
              Navigator.of(context).push(route);
            }
          },
        );
      } else {
        aChangeWidgetShipTo = aSpace;
      }
    }
    Widget x1y1 = aDeliveryDateWidget;
    Widget x1y2;
    Widget x1y3;
    if (aSalesOrder != null && aSalesOrder.isPersistent()) {
      x1y2 = Row(children: [
        Text('Purchase Order: ', style: TextStyle(fontWeight: FontWeight.bold)),
        Text(
          aSalesOrder.getCustomerPO(),
        ),
      ]);
      x1y3 = Row(children: [
        Text('Sales Order: ', style: TextStyle(fontWeight: FontWeight.bold)),
        Text(
          aSalesOrder.getSalesOrderKey(),
        ),
      ]);
    } else {
      x1y2 = aSpace;
      x1y3 = aSpace;
    }
    Widget x1y4 = aSpace;
    Widget x2y1 = Row(children: [
      Text('Account #: ', style: TextStyle(fontWeight: FontWeight.bold)),
      Text(
        aShipToCard.getShipToKey(),
      ),
    ]);
    Widget x2y2 = Text(aShipToCard.getShipToName());
    Widget x2y3 = Text(anAddressLine1);
    Widget x2y4 = Text(anAddressLine);
    Widget x3y1 = aPriceWidget;
    Widget x3y2;
    if (aCart.isCartInProgress() && !widget.isCheckOut) {
      x3y2 = TextButton(
        child: Text('Proceed to Checkout'),
        onPressed: () {
          Route route = MaterialPageRoute(
              builder: (context) => CartPage(widget.actionRefresh));
          Navigator.of(context).push(route);
        },
      );
    } else {
      x3y2 = aSpace;
    }
    Widget x3y3;
    if (widget.isCheckOut) {
      x3y3 = TextButton(
        child: Text('Add Product'),
        onPressed: () {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => ChooseProductScreen(widget.actionRefresh)));
        },
      );
    } else {
      x3y3 = aSpace;
    }
    Widget x3y4 = aChangeWidgetShipTo;
    Widget aTable = Table(
      //border: TableBorder.all(),
      //border:TableBorder.lerp(0,0,0,0),
      columnWidths: aMap,
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: <TableRow>[
        TableRow(
          children: <Widget>[
            x1y1,
            x2y1,
            x3y1,
          ],
        ),
        TableRow(
          children: <Widget>[
            x1y2, //
            x2y2, //
            x3y2
          ],
        ),
        TableRow(
          children: <Widget>[x1y3, x2y3, x3y3],
        ),
        TableRow(
          children: <Widget>[x1y4, x2y4, x3y4],
        ),
      ],
    );

    Card aCard = Card(
        //color: aColor,
        child: ListTile(
      title: aTable,
      trailing: aTrailingWidget,
    ));
    return aCard;
  }

  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text('No'),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = TextButton(
      child: Text('Yes'),
      onPressed: () {
        ICSUser aUser = UserData.getUser();
        Cart aCart = aUser.getCart();
        aCart.resetCart();
        widget.actionRefresh();

        Navigator.of(context).pop();
        Route route = MaterialPageRoute(
            builder: (context) => SelectCBSPage(widget.actionRefresh));
        Navigator.of(context).push(route);
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Confirm"),
      content: Text(
          "Are you sure you want to switch the ship to? The items in your cart will be lost."),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      useRootNavigator: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Widget menu(BuildContext context, bool allowAdd) {
    int CANCEL_ORDER = 0;
    int CANCEL_CHANGES = 1;
    int ADD_PRODUCT = 2;
    Cart aCart = UserData.getUser().getCart();
    bool isCancelEnabled;
    bool isChangeOrderEnabled;
    SalesOrderSession? aSalesOrder = aCart.getSalesOrderSession();
    if (aSalesOrder != null && aSalesOrder.isPersistent()) {
      isCancelEnabled = aSalesOrder.isChangeable();
      isChangeOrderEnabled = aSalesOrder.isChangeable();
    } else {
      isCancelEnabled = true;
      isChangeOrderEnabled = true;
    }
    List<PopupMenuEntry<int>> aList = List.empty(growable: true);
    if (allowAdd) {
      aList.add(PopupMenuItem(
        value: ADD_PRODUCT,
        enabled: isCancelEnabled,
        child: Row(children: [
          Icon(
            Icons.add,
            color: ADKTheme.BUTTON_ICONCOLOR,
          ),
          Text(
            "Add Product",
          )
        ]),
      ));
    }
    if (UserData.getUser().getCart().isCartInUpdateMode()) {
      aList.add(PopupMenuItem(
        value: CANCEL_CHANGES,
        enabled: true,
        child: Row(children: [
          Icon(
            Icons.clear,
            color: ADKTheme.BUTTON_ICONCANCELCOLOR,
          ),
          Text(
            'Cancel Changes',
          )
        ]),
      ));
    }

    String aCancelOrderText;
    if (aCart.isCartInUpdateMode()) {
      aCancelOrderText = 'Cancel Order';
    } else {
      aCancelOrderText = 'Cancel Cart';
    }
    aList.add(PopupMenuItem(
      value: CANCEL_ORDER,
      enabled: isCancelEnabled,
      child: Row(children: [
        Icon(
          Icons.cancel,
          color: ADKTheme.BUTTON_ICONCANCELCOLOR,
        ),
        Text(
          aCancelOrderText,
        )
      ]),
    ));

    Widget aMenu = PopupMenuButton<int>(
      elevation: 4,
      onSelected: (int result) {
        if (result == CANCEL_ORDER) {
          showDialog(
            useRootNavigator: false,
            context: context,
            builder: (context) {
              return StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
                  return this.buildCancelDialog(context, setState);
                },
              );
            },
          );
        } else if (result == CANCEL_CHANGES) {
          showDialog(
            useRootNavigator: false,
            context: context,
            builder: (context) {
              return StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
                  return this.buildCancelChangesDialog(
                      context, aCart.getSalesOrderSession(), setState);
                },
              );
            },
          );
        } else if (result == ADD_PRODUCT) {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => ChooseProductScreen(widget.actionRefresh)));
        }
      },
      itemBuilder: (context) => aList,
      child: Container(
        child: Icon(
          Icons.more_vert,
          color: ADKTheme.BUTTON_ICONCOLOR,
        ),
      ),
    );
    return aMenu;
  }

  AlertDialog buildCancelChangesDialog(BuildContext context,
      SalesOrderSession? aSalesOrderSession, StateSetter setState) {
    // set up the buttons
    Widget noButton = TextButton(
      child: Text('No'),
      onPressed: () {
        setState(() {
          //this.isLoading = false;
        });
        Navigator.of(context).pop();
      },
    );
    Widget yesButton = TextButton(
      child: Text('Yes'),
      onPressed: () {
        setState(() {
          widget.actionRefresh();
        });
        //Attempt to cancel the appointment
        try {
          UserData.getUser().getCart().resetCart();
          //TODO Retrieve order and navigate to OrderPage
          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => OpenOrdersPage(widget.actionRefresh,
                  snackBarMessage: 'Changes were canceled')));
        } catch (all) {
          setState(() {
            widget.actionRefresh();
          });
          ADKDialog2.showAlertDialog(context, all, 2);
        }
      },
    );

    AlertDialog aConfirmCancelDialog = AlertDialog(
      title: Text("Confirm"),
      content: Stack(children: [
        Text("Are you sure you want to lose the changes to this order?"),
        //_showCircularProgress()
      ]),
      actions: [
        noButton,
        yesButton,
      ],
    );

    return aConfirmCancelDialog;
  }

  AlertDialog buildCancelDialog(BuildContext context, StateSetter setState) {
    // set up the buttons
    Widget noButton = TextButton(
      child: Text('No'),
      onPressed: () {
        setState(() {
          widget.actionRefresh();
        });
        Navigator.of(context).pop();
      },
    );
    Widget yesButton = TextButton(
      child: Text('Yes'),
      onPressed: () {
        setState(() {
          widget.actionRefresh();
        });
        //Attempt to cancel the appointment
        UserData.getUser().getCart().resetCart();
        Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => OpenOrdersPage(widget.actionRefresh,
                snackBarMessage: 'Cart was cleared')));
      },
    );

    String aMessageText;
    if (UserData.getUser().getCart().isCartInUpdateMode()) {
      aMessageText = 'Are you sure you want to cancel this order?';
    } else {
      aMessageText = 'Are you sure you want to cancel this cart?';
    }
    AlertDialog aConfirmCancelDialog = AlertDialog(
      title: Text("Confirm"),
      content: Stack(children: [
        Text(aMessageText),
        //_showCircularProgress()
      ]),
      actions: [
        noButton,
        yesButton,
      ],
    );

    return aConfirmCancelDialog;
  }
}
