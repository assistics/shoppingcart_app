import 'package:ast_app/com/adk/ics/business/salesordersession.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:flutter/material.dart';
import 'package:shoppingcart_app/com/adk/sc/adktheme.dart';
import 'package:shoppingcart_app/com/adk/sc/orderpage.dart';
import 'package:shoppingcart_app/com/adk/utility/widget/ordertile.dart';

class OrderTileForList extends OrderTile {
  OrderTileForList(
      {required SalesOrderSession salesOrderSession,
      required VoidCallback anActionRefresh,
      Key? key})
      : super(
            salesOrderSession: salesOrderSession,
            isEdit: false,
            actionRefresh: anActionRefresh,
            key: key) {}

  Widget orderTile(BuildContext context, SalesOrderSession aSalesOrder) {
    Icon aLeadingIcon = Icon(
      Icons.local_shipping,
      color: ADKTheme.BUTTON_ICONCOLOR,
    );

    Widget lines = this.buildTileTitle(context, aSalesOrder);

    Widget aCard = Card(
        child: ListTile(
      title: lines,
      leading: aLeadingIcon,
      trailing: Icon(Icons.chevron_right),
      onTap: () {
        Route route = MaterialPageRoute(
            builder: (context) =>
                OrderPage(aSalesOrder, false, this.actionRefresh));
        Navigator.of(context).push(route);
      },
    ));
    if (ADKGlobal.isWideScreen(context)) {
      double aWidth = ADKGlobal.desktopCenterPaneFullWidth(context);
      aCard = SizedBox(
          width: aWidth,
          //height: 300.0,
          child: aCard);
    }
    return aCard;
  }

  bool isListOrderMode() {
    return true;
  }
}
