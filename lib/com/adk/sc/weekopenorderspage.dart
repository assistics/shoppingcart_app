import 'package:ast_app/com/adk/ics/business/calendardatesession.dart';
import 'package:ast_app/com/adk/ics/business/cbsi.dart';
import 'package:ast_app/com/adk/ics/business/icsuser.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkbottomnavigationbar.dart';
import 'package:shoppingcart_app/com/adk/sc/weekorderlistview.dart';

class WeekOpenOrdersPage extends StatefulWidget {
  String? snackBarMessage;
  late VoidCallback actionRefresh;

  WeekOpenOrdersPage(VoidCallback anActionRefresh, {String? snackBarMessage}) {
    this.actionRefresh = anActionRefresh;
    this.snackBarMessage = snackBarMessage;
  }

  @override
  _WeekOpenOrdersPageState createState() =>
      _WeekOpenOrdersPageState(this.actionRefresh, this.snackBarMessage);
}

class _WeekOpenOrdersPageState extends State<WeekOpenOrdersPage> {
  bool _isLoading = false;
  String? snackBarMessage;
  late VoidCallback actionRefresh;
  late Future<List<CalendarDateSession>> myFuture;
  List<CalendarDateSession> availableDates = List.empty();

  _WeekOpenOrdersPageState(VoidCallback anActionRefresh, this.snackBarMessage) {
    this.actionRefresh = anActionRefresh;
    this.snackBarMessage = snackBarMessage;
  }

  void initState() {
    super.initState();
    ICSUser aUser = UserData.getUser();
    // Assign that variable your Future.
    myFuture = _fetchAvailableDates();
    myFuture.then((value) {
      this.availableDates = value;
      this.setState(() {});
        }).catchError((onError) {
      //Swallow for now
    });
    }

  Future<List<CalendarDateSession>> _fetchAvailableDates() {
    ICSUser aUser = UserData.getUser();
    List<CBSI> aCBSList = UserData.getUser().getCBSArray();
    if (aCBSList.isNotEmpty) {
      CBSI? aDefaultCBS = aUser.getDefaultCBS();
      CBSI aCBS;
      if (aDefaultCBS != null) {
        aCBS = aDefaultCBS;
      } else {
        aCBS = aCBSList.first;
      }
      ADKURLBuilder aURLBuilder = new ADKURLBuilder();
      Future<List<CalendarDateSession>> aList =
          aURLBuilder.fetchAvailableRequiredDates(
              aCBS.getCustomerKey(), aCBS.getBillToKey(), aCBS.getShipToKey());
      return aList;
    } else {
      return emptyList();
    }
  }

  Future<List<CalendarDateSession>> emptyList() async {
    List<CalendarDateSession> aList = List.empty();
    return aList;
  }

  @override
  Widget build(BuildContext context) {
    Widget aBody = this.getBody();
    aBody = ADKGlobal.scaffoldFullPage(context, aBody);
    aBody = Stack(
      children: [aBody, _showCircularProgress()],
    );
    return Scaffold(
      appBar: ADKAppBar.buildBar(context, 'My Week'),
      body: aBody,
      bottomNavigationBar: ADKBottomNavigationBar.buildBar(context),
    );
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget getBody() {
    WeekOrderListView aList = new WeekOrderListView(this.snackBarMessage,
        _manageStateForChildWidget, this.actionRefresh, this.availableDates);
    return aList;
  }

  void _manageStateForChildWidget(bool newValue) {
    setState(() {
      _isLoading = newValue;
    });
  }
}
