import 'package:ast_app/com/adk/ics/business/cbsi.dart';
import 'package:ast_app/com/adk/ics/business/icsuser.dart';
import 'package:ast_app/com/adk/ics/business/userpaymentmethod.dart';
import 'package:ast_app/com/adk/utility/adkcompare.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkjsonresponse.dart';
import 'package:ast_app/com/adk/utility/adkmiscstringfunctions.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:logger/logger.dart';
import 'package:shoppingcart_app/com/adk/sc/address_page.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkbottomnavigationbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adktheme.dart';
import 'package:shoppingcart_app/com/adk/sc/change_email_page.dart';
import 'package:shoppingcart_app/com/adk/sc/changepasswordpage.dart';
import 'package:shoppingcart_app/com/adk/sc/loginpage.dart';
//import 'package:web_browser/web_browser.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';

class AccountPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  final logger = Logger();
  final _formKey = new GlobalKey<FormState>();

  String _email = "";

  bool _isLoginForm = false;
  bool _isLoading = false;

  // Check if form is valid before perform login or signup
  bool validateEmail() {
    final FormState? form = _formKey.currentState;
    if (form != null) {
      if (form.validate()) {
        form.save();
        return true;
      }
    }
    return false;
  }

  // Perform login or signup
  void validateAndSubmit() async {
    setState(() {
      //_errorMessage = "";
      _isLoading = true;
    });
    if (validateEmail()) {
      String userId = "";
      try {
        this.sendPassword(_email);

        _showLoginScreenWithMessage(_email);
        if (userId.length > 0 && _isLoginForm) {
          //widget.loginCallback();
        }
      } catch (e) {
        print('Error: $e');
        setState(() {
          this._isLoading = false;
          //_errorMessage = e.toString();
          FormState? aState = _formKey.currentState;
          if (aState != null) {
            aState.reset();
          }
        });
      }
    } else {
      _isLoading = false;
    }
  }

  void _showLoginScreen() {
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => LoginPage()));
  }

  void _showLoginScreenWithMessage(String anEmailAddress) {
    Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => LoginPage.LoginPageWithMessage(
            'Password has been send to ' + anEmailAddress)));
  }

  void sendPassword(String anEmail) async {
    anEmail = anEmail.trim();

    ADKURLBuilder aURLBuilder = ADKURLBuilder.forLogin();
    ADKJSONResponse aJSON = await aURLBuilder.sendPassword(anEmail, "sc");
    if (aJSON.isError() || aJSON.isFunctionalWarning()) {
      print('Failed login for ' + anEmail);
    }
  }

  UserPaymentMethod? defaultUserPaymentMethod;

  bool initRan = false;

  @override
  void initState() {
    //_errorMessage = "";
    this._isLoading = false;
    this.loadDefaulPaymentMethod();
    _isLoginForm = true;

    super.initState();
  }

  void resetForm() {
    FormState? aState = _formKey.currentState;
    if (aState != null) {
      aState.reset();
    }
    //_errorMessage = "";
  }

  void toggleFormMode() {
    resetForm();
    setState(() {
      _isLoginForm = !_isLoginForm;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget aBody = this.getBody();
    aBody = ADKGlobal.scaffoldFullPage(context, aBody);
    return Scaffold(
      appBar: ADKAppBar.buildBar(context, 'Account'),
      body: Stack(
        children: <Widget>[
          aBody,
          _showCircularProgress(),
        ],
      ),
      bottomNavigationBar: ADKBottomNavigationBar.buildBar(context),
    );
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget getBody() {
    ICSUser aUser = UserData.getUser();
    List<Widget> aList = List.empty(growable: true);
    Widget aBody;
    Widget aChangeEmailLink = TextButton(
      child: Text('Change Email'),
      onPressed: () {
        Route route =
        MaterialPageRoute(builder: (context) => ChangeEmailPage());
        Navigator.of(context).push(route);
      },
    );

    Widget aChangePasswordLink = TextButton(
      child: Text('Change Password'),
      onPressed: () {
        Route route =
            MaterialPageRoute(builder: (context) => ChangePasswordPage());
        Navigator.of(context).push(route);
      },
    );
    Column aChangePasswordColumn = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [Text(aUser.getEmailAddress()),aChangeEmailLink, aChangePasswordLink],
    );
    Widget anAccount = Card(
        color: Colors.white,
        child: ListTile(
          leading: Icon(Icons.account_box),
          title: Text(aUser.getFirstName() +
              ' ' +
              aUser.getLastName() +
              ' (' +
              aUser.getLogin() +
              ')'),
          subtitle: aChangePasswordColumn,
        ));
    aList.add(ADKGlobal.messageGreen(context, 'Account'));
    aList.add(anAccount);
    if (ADKGlobal.scIsAutoPaymentMethodEnabled()) {
      if (aUser.isAutoPaymentEnabled()) {
        Widget noButton = TextButton(
          child: Text("No"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        );
        Widget aTurnOffLink = TextButton(
          child: Text('Disable Automatic Payments'),
          onPressed: () {
            Widget yesButton = TextButton(
              child: Text("Yes"),
              onPressed: () {
                setState(() {
                  this._isLoading = true;
                });
                Navigator.of(context).pop();
                ADKURLBuilder().setAutoPaymentStatus(false).then((value) {
                  this.loadDefaulPaymentMethod();
                  setState(() {
                    this._isLoading = false;
                  });
                }).catchError((all) {
                  this.loadDefaulPaymentMethod();
                  setState(() {
                    this._isLoading = false;
                  });
                  ADKDialog2.showAlertDialog(context, all, 1);
                });
              },
            );

            Widget aHTML;
            aHTML =
                Text('Are you sure you want to disable automatic payments?');
            AlertDialog alert = AlertDialog(
              //title: Text("My title"),
              content: aHTML,
              actions: [
                noButton,
                yesButton,
              ],
            );

            showDialog(
                useRootNavigator: false,
                context: context,
                builder: (BuildContext context) {
                  return alert;
                });
          },
        );
        Widget aRow;
        Column aTurnOffColumn = Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [aTurnOffLink],
        );
        String aURL = ADKGlobal.getSCAutoPaymentTermsAndConditionsURL();
        if (!ADKMiscStringFunctions.isBlankOrWhitespace(aURL)) {
          Widget aTermsLink = this.viewTermsButton();
          Column aTermsColumn = Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [aTermsLink],
          );
          aRow = Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [aTurnOffColumn, aTermsColumn]);
        } else {
          aRow = aTurnOffColumn;
        }
        Widget aTurnOffAutoPay = Card(
            color: Colors.white,
            child: ListTile(
              leading: Icon(Icons.payment),
              title: Text('You are currently enrolled for automatic payments'),
              subtitle: aRow,
            ));
        aList.add(aTurnOffAutoPay);
      } else {
        Widget aTurnOnLink = TextButton(
          child: Text('Enable Automatic Payments'),
          onPressed: () {
            Widget declineButton = TextButton(
              child: Text("Decline"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
            Widget iAgreeButton = TextButton(
              child: Text("I Agree"),
              onPressed: () {
                setState(() {
                  this._isLoading = true;
                });
                Navigator.of(context).pop();

                ADKURLBuilder().setAutoPaymentStatus(true).then((value) {
                  this.loadDefaulPaymentMethod();
                  setState(() {
                    this._isLoading = false;
                  });
                }).catchError((all) {
                  setState(() {
                    this._isLoading = false;
                  });
                  ADKDialog2.showAlertDialog(context, all, 1);
                });
              },
            );

            String aURL = ADKGlobal.getSCAutoPaymentTermsAndConditionsURL();
            Widget aHTML;
            if (!ADKMiscStringFunctions.isBlankOrWhitespace(aURL)) {
              aHTML = this.terms(aURL);
            } else {
              aHTML = Text('');
            }
            AlertDialog alert = AlertDialog(
              //title: Text("My title"),
              content: aHTML,
              actions: [
                declineButton,
                iAgreeButton,
              ],
            );

            showDialog(
                useRootNavigator: false,
                context: context,
                builder: (BuildContext context) {
                  return alert;
                });
          },
        );
        Column aTurnOnColumn = Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [aTurnOnLink],
        );

        Widget aSetupAutoPay = Card(
            color: Colors.white,
            child: ListTile(
              leading: Icon(Icons.payment),
              title: Text('You are not enrolled in automatic payments'),
              subtitle: aTurnOnColumn,
            ));
        aList.add(aSetupAutoPay);
      }
    }
    if (this.initRan && aUser.isAutoPaymentEnabled()) {
      UserPaymentMethod? aDefaultPaymentMethod =
          aUser.getDefaultPaymentMethod();
      if (aDefaultPaymentMethod == null) {
        aList.add(ADKGlobal.messageYellow(
            context,
            'You must assign a default payment method.  Automatic payments will not be processed until this is resolved.',
            null));
      } else if (aDefaultPaymentMethod.isExpired(UserData.getLocale())) {
        aList.add(ADKGlobal.messageYellow(
            context,
            'Your default payment method is expired.  Automatic payments will not be processed until this is resolved.',
            null));
      }
    }
    if (!aUser.hasShipTos()) {
      aList.add(ADKGlobal.messageYellow(
          context,
          'Your ship to address is not yet setup. Please contact customer service.',
          null));
    } else {
      aList.add(SizedBox(
        height: 5,
      ));
      if (aUser.hasMultipleShipTos()) {
        aList.add(
          ADKGlobal.messageGreen(context,
              'Ship Tos - Click on a card below to mark the default account'),
        );
      }
      aList.add(SizedBox(
        height: 20,
      ));
      aList.add(Expanded(child: this._buildListView()));
    }
    aBody = Container(
        //height: 50, //
        width: 600, //MediaQuery.of(context).size.width ,//- 100,
        child: Column(children: aList));
    aBody = Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 8.0,
        vertical: 2.0,
      ),
      child: aBody,
    );

    if (ADKGlobal.isDesktop()) {
      //double aWidth = ADKGlobal.desktopCenterPaneFullWidth(context);
      aBody =
          Container(child: aBody, constraints: BoxConstraints(maxWidth: 600));
    }

    ADKGlobal.deviceWidth(context);
    return aBody;
  }

  ListView _buildListView() {
    List<CBSI> aList = UserData.getUser().getCBSArray();
    return ListView.builder(
      itemCount: aList.length,
      itemBuilder: (context, index) {
        CBSI item = aList[index];
        return this._buildShipToCard(true, item);
      },
    );
  }

  loadDefaulPaymentMethod() {
    ICSUser aUser = UserData.getUser();
    int aDefaultPaymentMethodId = aUser.getDefaultPaymentMethodId();
    if (aDefaultPaymentMethodId > 0) {
      ADKURLBuilder().fetchPaymentMethod(aDefaultPaymentMethodId).then((value) {
        this.initRan = true;
        aUser.setDefaultPaymentMethod(value);
        setState(() {});
      }).catchError((all) {
        this.initRan = true;
        aUser.setDefaultPaymentMethod(null);
        setState(() {});
      });
    }
  }

  Widget viewTermsButton() {
    Widget aTurnOnLink = TextButton(
      child: Text('View Terms & Conditions'),
      onPressed: () {
        Widget okButton = TextButton(
          child: Text("Ok"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        );
        String aURL = ADKGlobal.getSCAutoPaymentTermsAndConditionsURL();
        Widget aHTML;
        if (!ADKMiscStringFunctions.isBlankOrWhitespace(aURL)) {
          aHTML = this.terms(aURL);
        } else {
          aHTML = Text('');
        }
        AlertDialog alert = AlertDialog(
          //title: Text("My title"),
          content: aHTML,
          actions: [
            okButton,
          ],
        );

        showDialog(
            useRootNavigator: false,
            context: context,
            builder: (BuildContext context) {
              return alert;
            });
      },
    );
    return aTurnOnLink;
  }

  Widget terms(String aURL) {
    Widget aWidget = InAppWebView(
      initialUrlRequest: URLRequest(url: WebUri(aURL)),
      /*onWebViewCreated: (InAppWebViewController controller) {
        _webViewController = controller;
      },*/
    );

    return Container(
        height: ADKGlobal.scTermAndConditionsHeight(context),
        width: ADKGlobal.scTermAndConditionsWidth(context),
        child: SafeArea(child: aWidget));
  }

  Widget _buildShipToCard(bool inList, CBSI aCBS) {
    CBSI anAddress = aCBS;
    List<Widget> aColumn = List.empty(growable: true);
    String anAddressLine1 = anAddress.getAddressLine1();
    String aLine1 = anAddressLine1;
    if (ADKMiscStringFunctions.isBlankOrWhitespace(aLine1)) {
      aLine1 = anAddress.getAddressLine2();
    }
    String anAddressCity = anAddress.getCity();
    String anAddressState = anAddress.getState();
    String anAddressPostalCode = anAddress.getPostalCode();
    String aLine2 =
        anAddressCity + ", " + anAddressState + " " + anAddressPostalCode;

    Icon anIcon;
    Color? aTileColor;
    if (inList) {
      Color aColor;
      if (aCBS.isCBSOnHold()) {
        aColor = Colors.yellow;
      } else {
        aColor = Colors.blue;
      }

      anIcon = Icon(Icons.where_to_vote_outlined, color: aColor);
    } else {
      anIcon = Icon(
        Icons.location_on_outlined,
      );
      aTileColor = ADKTheme.CARD_CARTCARD_COLOR;
    }
    ICSUser aUser = UserData.getUser();
    if (aUser.hasMultipleShipTos()) {
      CBSI? aDefaultCBS = aUser.getDefaultCBS();
      String aDefaultShipToKey = '';
      if (aDefaultCBS != null) {
        aDefaultShipToKey = aDefaultCBS.getShipToKey();
      }
      if (ADKCompare.equals(anAddress.getShipToKey(), aDefaultShipToKey)) {
        aTileColor = Colors.blue[50];
      }
    }
    aColumn.add(Text('Account #: ' + anAddress.getShipToKey()));
    aColumn.add(Text(anAddress.getShipToName()));
    if (!ADKMiscStringFunctions.isBlankOrWhitespace(aLine1)) {
      aColumn.add(Text(aLine1));
    }
    if (!ADKMiscStringFunctions.isBlankOrWhitespace(aLine2)) {
      aColumn.add(Text(aLine2));
    }
    Widget? aTrailing = null;
    if (ADKGlobal.scAddressAllowEdit()){
      aTrailing = TextButton(
        child: Text('Request Address Change'),
        onPressed: () {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => AddressPage(aCBS)));
        },
      );

    }
    Widget aDisplay = Card(
      elevation: 6.0,
      child: ListTile(
        trailing: aTrailing,
        leading: anIcon,
        tileColor: aTileColor,
        title: Column(
            crossAxisAlignment: CrossAxisAlignment.start, children: aColumn),
        onTap: () {
          aUser.setDefaultCBS(anAddress);
          setState(() {});
        },
      ),
    );
    return aDisplay;
  }
}
