import 'package:ast_app/com/adk/ics/business/userpaymentmethod.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkjsonresponse.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:flutter/material.dart';
import 'package:shoppingcart_app/com/adk/config/routes.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkbottomnavigationbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/paymentmethodnew.dart';

class PaymentMethodsPage extends StatefulWidget {
  PaymentMethodsPage();

  @override
  State<StatefulWidget> createState() {
    return PaymentMethodsPageState();
  }
}

class PaymentMethodsPageState extends State<PaymentMethodsPage> {
  bool _isLoading = true;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  PaymentMethodsPageState() {}

  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    FutureBuilder<List<UserPaymentMethod>?> aList =
        FutureBuilder<List<UserPaymentMethod>?>(
      future: _fetchPaymentMethods(context), // function where you call your api
      builder: (BuildContext context,
          AsyncSnapshot<List<UserPaymentMethod>?> snapshot) {
        if (snapshot.hasData) {
          _isLoading = false;
          List<UserPaymentMethod>? data = snapshot.data;
          return _paymentsListView(context, data);
        } else if (snapshot.hasError) {
          //return Text("${snapshot.error}");

          return Scaffold(
            appBar: ADKAppBar.buildBar(context, 'Payment Methods'),
            body: Text(
                "System is down for maintenance.  Please try again in a few minutes."),
            bottomNavigationBar:
                ADKBottomNavigationBar.buildBar(context), //    )
          );
        }
        return Scaffold(
          appBar: ADKAppBar.buildBar(context, 'Payment Methods'),
          body: _showCircularProgress(),
          bottomNavigationBar: ADKBottomNavigationBar.buildBar(context), //    )
        );
      },
    );
    return aList;
  }

  Future<List<UserPaymentMethod>> _fetchPaymentMethods(
      BuildContext context) async {
    ADKURLBuilder aURLBuilder = new ADKURLBuilder();
    Future<List<UserPaymentMethod>> aList = aURLBuilder.fetchPaymentMethods();
    return aList;
  }

  Widget _paymentsListView(
      BuildContext context, List<UserPaymentMethod>? aPaymentMethodList) {
    List<Widget> aList = List.empty(growable: true);
    Widget aButton = ElevatedButton.icon(
        onPressed: () {
          Route route =
              MaterialPageRoute(builder: (context) => PaymentMethodNew());
          Navigator.push(context, route);
        },
        icon: new Icon(Icons.add_circle_outline, size: 30, color: Colors.green),
        label: Text('Add Payment Method'));
    aList.add(aButton);
    aList.add(SizedBox(height: 20));
    if (aPaymentMethodList == null || aPaymentMethodList.isEmpty) {
      aList.add(Center(
          key: GlobalKey(),
          child: Card(
              child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Text.rich(
                    TextSpan(
                      children: <TextSpan>[
                        TextSpan(
                            text: 'You do not have any payment methods setup.',
                            style: TextStyle(fontSize: 18)),
                      ],
                    ),
                  )))));
    } else {
      for (var i = 0; i < aPaymentMethodList.length; i++) {
        aList.add(this.card(aPaymentMethodList[i]));
      }
    }

    Widget aBody;
    if (ADKGlobal.isDesktop()) {
      aBody = Scrollbar(
          //hoverThickness: 15,
          trackVisibility: false,
          thickness: 15,
          thumbVisibility: true,
          child: ListView(
            children: aList,
            padding: EdgeInsets.fromLTRB(0, 0, 16, 0),
          ));
    } else {
      aBody = SingleChildScrollView(child: Column(children: aList));
    }

    aBody = ADKGlobal.scaffoldThinPage(context, aBody);

    return Scaffold(
        appBar: ADKAppBar.buildBar(context, 'Payment Methods'),
        bottomNavigationBar: ADKBottomNavigationBar.buildBar(context), //    )
        body: SafeArea(
            child: Column(children: <Widget>[
          //SizedBox(height: 10),
          Expanded(child: aBody)
        ])));
  }

  Widget _showCircularProgress() {
    return Center(child: CircularProgressIndicator());
  }

  Widget card(UserPaymentMethod anObject) {
    Column aColumn;
    Text aTitleText;
    if (anObject.isCreditCardPaymentType()) {
      aTitleText = Text(
          anObject.getCreditCardType() +
              ' - ************ ' +
              anObject.getCreditCardLastFour(),
          style: TextStyle(fontSize: 20));
      Text aSubTitleText =
      Text(anObject.getCreditCardName(), style: TextStyle(fontSize: 18));
      Text anExpDate = Text(
          'Exp: ' +
              anObject.getCreditCardExpirationMonth() +
              '/' +
              anObject.getCreditCardExpirationYear(),
          style: TextStyle(fontSize: 18));
      aColumn = Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [aSubTitleText, anExpDate]);
    }else if (anObject.isACHPaymentType()) {
      aTitleText = Text(
          anObject.getAccountDescription(),
          style: TextStyle(fontSize: 20));
      Text aSubTitleText =
      Text(anObject.getAccountName(), style: TextStyle(fontSize: 18));
      aColumn = Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [aSubTitleText]);

    }else {
      aTitleText=Text('');
      aColumn = Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [Text('Unknown Payment Type')]);
    }
    Widget aLeftButton = Text('');
    if (this.isPrimaryMethod(anObject)) {
      aLeftButton = Text('(Default)', style: TextStyle(fontSize: 18));
    } else {
      aLeftButton = TextButton(
          child: const Text('Make Default', style: TextStyle(fontSize: 18)),
          onPressed: () {
            showDialog(
              useRootNavigator: false,
              context: context,
              builder: (context) {
                String contentText = "Content of Dialog";
                return StatefulBuilder(
                  builder: (BuildContext context, StateSetter setState) {
                    return this
                        .confirmMakeDefaultDialog(context, anObject, setState);
                  },
                );
              },
            );
          });
    }

    Widget aCard = Card(
        color: Colors.white,
        elevation: 5,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              onTap: () {},
              isThreeLine: false,
              leading: this.getCardIcon(anObject),
              title: aTitleText,
              subtitle: aColumn,
            ),
            OverflowBar(
              alignment: MainAxisAlignment.spaceBetween,
              //alignment: MainAxisAlignment.end,
              children: <Widget>[
                aLeftButton,
                //TODO Enable edit again once we know how to edit card info in slimcd
                /*
                TextButton(
                  child: const Text('Edit', style: TextStyle(fontSize: 18)),
                  onPressed: () {
                    Route route = MaterialPageRoute(
                        builder: (context) =>
                            PaymentMethodEdit(anObject));
                    Navigator.push(context, route);
                  },
                ),*/
                TextButton(
                  child: const Text('Delete', style: TextStyle(fontSize: 18)),
                  onPressed: () {
                    showDialog(
                      useRootNavigator: false,
                      context: context,
                      builder: (context) {
                        String contentText = "Content of Dialog";
                        return StatefulBuilder(
                          builder:
                              (BuildContext context, StateSetter setState) {
                            return this.confirmDeleteDialog(
                                context, anObject, setState);
                          },
                        );
                      },
                    );
                  },
                ),
              ],
            ),
          ],
        ));

    return aCard;
  }

  Widget getCardIcon(UserPaymentMethod anObject) {
    if (anObject.isCreditCardPaymentType()) {
      if (anObject.isVisa()) {
        return Image.asset(
          'icons/visa.png',
          height: 48,
          width: 48,
          package: 'flutter_credit_card',
        );
      } else if (anObject.isAmericanExpress()) {
        return Image.asset(
          'icons/amex.png',
          height: 48,
          width: 48,
          package: 'flutter_credit_card',
        );
      } else if (anObject.isMasterCard()) {
        return Image.asset(
          'icons/mastercard.png',
          height: 48,
          width: 48,
          package: 'flutter_credit_card',
        );
      } else if (anObject.isDiscover()) {
        return Image.asset(
          'icons/discover.png',
          height: 48,
          width: 48,
          package: 'flutter_credit_card',
        );
      } else {
        return Icon(Icons.credit_card);
      }
    }else if (anObject.isACHPaymentType()) {
      return Icon(Icons.account_balance);
    } else {
      return Text('');
    }
  }

  AlertDialog confirmDeleteDialog(
      BuildContext context, UserPaymentMethod anObject, StateSetter setState) {
    // set up the buttons
    Widget noButton = TextButton(
      child: Text('No'),
      onPressed: () {
        setState(() {
          //_errorMessage = "";
          this._isLoading = false;
        });
        Navigator.of(context).pop();
      },
    );
    Widget yesButton = TextButton(
      child: Text('Yes'),
      onPressed: () {
        setState(() {
          //_errorMessage = "";
          this._isLoading = true;
        });
        //Attempt to cancel the appointment
        new ADKURLBuilder()
            .deletePaymentMethod(anObject)
            .then((ADKJSONResponse aResponse) {
          setState(() {
            //_errorMessage = "";
            this._isLoading = false;
          });

          if (aResponse.isSuccess()) {
            ADKDialog2.showDialogText(
                context,
                '',
                'Payment method deleted',
                () => {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          ADKSCRoutes.PAGE_PAYMENTMETHODS, (route) => false)
                    });
          } else {
            this.setState(() {
              this._isLoading = false;
            });
            ADKDialog2.showAlertDialogText(context, aResponse.getMessageText());
          }
        }).catchError((onError) {
          setState(() {
            //_errorMessage = "";
            this._isLoading = false;
          });
          ADKDialog2.showAlertDialogText(context, onError.getMessageText());
        });
      },
    );

    // set up the AlertDialog
    AlertDialog aConfirmCancelDialog = AlertDialog(
      title: Text("Confirm"),
      content: Stack(children: [
        Text("Are you sure you want to delete this payment method?"),
        _showCircularProgress2()
      ]),
      actions: [
        noButton,
        yesButton,
      ],
    );

    return aConfirmCancelDialog;
  }

  AlertDialog confirmMakeDefaultDialog(
      BuildContext context, UserPaymentMethod anObject, StateSetter setState) {
    // set up the buttons
    Widget noButton = TextButton(
      child: Text('No'),
      onPressed: () {
        setState(() {
          this._isLoading = false;
        });
        Navigator.of(context).pop();
      },
    );
    Widget yesButton = TextButton(
      child: Text('Yes'),
      onPressed: () {
        setState(() {
          this._isLoading = true;
        });
        //Attempt to cancel the appointment
        new ADKURLBuilder()
            .makePaymentMethodDefault(anObject)
            .then((ADKJSONResponse aResponse) {
          setState(() {
            //_errorMessage = "";
            this._isLoading = false;
          });

          if (aResponse.isSuccess()) {
            Navigator.of(context).pushNamedAndRemoveUntil(
                ADKSCRoutes.PAGE_PAYMENTMETHODS, (route) => false);
          } else {
            this.setState(() {
              this._isLoading = false;
            });
            ADKDialog2.showAlertDialogText(context, aResponse.getMessageText());
          }
        }).catchError((onError) {
          setState(() {
            //_errorMessage = "";
            this._isLoading = false;
          });
          ADKDialog2.showAlertDialogText(context, onError.getMessageText());
        });
      },
    );

    // set up the AlertDialog
    AlertDialog aConfirmCancelDialog = AlertDialog(
      title: Text("Confirm"),
      content: Stack(children: [
        Text("Are you sure you want to make this payment method your default?"),
        _showCircularProgress2()
      ]),
      actions: [
        noButton,
        yesButton,
      ],
    );

    return aConfirmCancelDialog;
  }

  bool isPrimaryMethod(UserPaymentMethod anObject) {
    return anObject.isDefault();
  }

  Widget _showCircularProgress2() {
    if (_isLoading) {
      return CircularProgressIndicator();
      //return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }
}
