import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkjsonresponse.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adktheme.dart';
import 'package:shoppingcart_app/com/adk/sc/loginpage.dart';

//import 'package:flutter_login_demo/services/authentication.dart';

class ForgotPasswordPage extends StatefulWidget {
  //ForgotPasswordPage({required this.loginCallback});

  //final BaseAuth auth;
  //final VoidCallback loginCallback;

  @override
  State<StatefulWidget> createState() => new _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  final logger = Logger();
  final _formKey = new GlobalKey<FormState>();

  String _email = "";
  String _password = "";
  String _errorMessage = "";

  bool _isLoginForm = false;
  bool _isLoading = false;

  // Check if form is valid before perform login or signup
  bool validateEmail() {
    final FormState? form = _formKey.currentState;
    if (form != null) {
      if (form.validate()) {
        form.save();
        return true;
      }
    }
    return false;
  }

  // Perform login or signup
  void validateAndSubmit() async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateEmail()) {
      String userId = "";
      try {
        this.sendPassword(_email);

        _showLoginScreenWithMessage(_email);
        if (userId.length > 0 && _isLoginForm) {
          //widget.loginCallback();
        }
      } catch (e) {
        print('Error: $e');
        setState(() {
          this._isLoading = false;
          _errorMessage = e.toString();
          FormState? aState = _formKey.currentState;
          if (aState != null) {
            aState.reset();
          }
        });
      }
    } else {
      _isLoading = false;
    }
  }

  void _showLoginScreen() {
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => LoginPage()));
  }

  void _showLoginScreenWithMessage(String anEmailAddress) {
    Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => LoginPage.LoginPageWithMessage(
            'Password has been send to ' + anEmailAddress)));
  }

  void sendPassword(String anEmail) async {
    anEmail = anEmail.trim();

    ADKURLBuilder aURLBuilder = ADKURLBuilder.forLogin();
    ADKJSONResponse aJSON = await aURLBuilder.sendPassword(anEmail, "sc");
    if (aJSON.isError() || aJSON.isFunctionalWarning()) {
      print('Failed login for ' + anEmail);
    }
  }

  @override
  void initState() {
    _errorMessage = "";
    this._isLoading = false;
    _isLoginForm = true;
    super.initState();
  }

  void resetForm() {
    FormState? aState = _formKey.currentState;
    if (aState != null) {
      aState.reset();
    }
    _errorMessage = "";
  }

  void toggleFormMode() {
    resetForm();
    setState(() {
      _isLoginForm = !_isLoginForm;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: ADKAppBar.loginAppBar(),
        body: Stack(
          children: <Widget>[
            _showForm(),
            _showCircularProgress(),
          ],
        ));
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget _showForm() {
    if (ADKGlobal.isWideScreen(context)) {
      return Center(
          child: Align(
              alignment: Alignment(0, -.5),
              child: Material(
                  elevation: 10,
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  child: Container(
                      decoration: BoxDecoration(
                        //color: Colors.grey[100],
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                      ),
                      width: 600,
                      padding: EdgeInsets.all(16.0),
                      child: new Form(
                        key: _formKey,
                        child: new ListView(
                          shrinkWrap: true,
                          children: <Widget>[
                            showLogo(),
                            showEmailInput(),
                            _showInstructions(),
                            showPrimaryButton(),
                            showErrorMessage(),
                            showSecondaryButton(),
                          ],
                        ),
                      )))));
    } else {
      return new Container(
          padding: EdgeInsets.all(16.0),
          child: new Form(
            key: _formKey,
            child: new ListView(
              shrinkWrap: true,
              children: <Widget>[
                showLogo(),
                showEmailInput(),
                _showInstructions(),
                showPrimaryButton(),
                showErrorMessage(),
                showSecondaryButton(),
              ],
            ),
          ));
    }
  }

  Widget showErrorMessage() {
    if (_errorMessage.length > 0) {
      return new Text(
        _errorMessage,
        style: TextStyle(
            fontSize: 13.0,
            color: Colors.red,
            height: 1.0,
            fontWeight: FontWeight.w300),
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }

  Widget showLogo() {
    return new Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
        child: CircleAvatar(
          backgroundColor: Colors.white,
          radius: 150,
          child: Image.asset(
            ADKGlobal.getLogoFilename(),
            width: 250,
            height: 250,
          ),
        ),
      ),
    );
  }

  Widget showEmailInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Email',
            icon: new Icon(
              Icons.mail,
              color: Colors.grey,
            )),
        validator: (value) =>
            (value == null || value.isEmpty) ? 'Email can\'t be empty' : null,
        onSaved: (value) => {
          if (value != null) {_email = value.trim()}
        },
      ),
    );
  }

  Widget _showSecondaryButton() {
    return new TextButton(
      child: _textSecondaryButton(),
      onPressed: toggleLogin,
    );
  }

  Widget _showInstructions() {
    //return new Text('An email will be sent allowing you to reset your password',
    return new Text('An email will be sent with your password',
        style: new TextStyle(fontSize: 14.0, fontWeight: FontWeight.w300));
  }

  Widget _textSecondaryButton() {
    return new Text('Enter your email address or ... Cancel',
        style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300));
  }

  Widget showSecondaryButton() {
    return new TextButton(
        child: new Text('Enter your email address or ... Cancel',
            style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300)),
        onPressed: toggleLogin);
  }

  void toggleLogin() {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    String userId = "";
    _showLoginScreen();
    //widget.loginCallback();
  }

  Widget showPrimaryButton() {
    return new Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      //child: SizedBox(
      //height: 40.0,
      child: new ElevatedButton(
        //elevation: 5.0,
        style: ElevatedButton.styleFrom(
          elevation: 5, padding: EdgeInsets.symmetric(vertical: 16),
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(30.0)),
          //primary: Colors.white,
          backgroundColor: ADKTheme.BUTTON_BACKGROUNDCOLOR,
          //fixedSize: const Size(150, 200)
        ),
        //shape: new RoundedRectangleBorder(
        //    borderRadius: new BorderRadius.circular(30.0)),
        //color: ADKTheme.BUTTON_BACKGROUNDCOLOR,
        child: new Text('Send Password',
            style: new TextStyle(fontSize: 20.0, color: Colors.white)),
        onPressed: validateAndSubmit,
      ),
    );
  }

  showAlertDialog(BuildContext context, String aMessageText) {
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      //title: Text("My title"),
      content: Text(aMessageText),
      actions: [
        okButton,
      ],
    );
    // show the dialog
    showDialog(
      useRootNavigator: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
