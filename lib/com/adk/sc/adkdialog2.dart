import 'package:ast_app/com/adk/utility/exception/fatalwarningexception.dart';
import 'package:ast_app/com/adk/utility/exception/functionalwarningexception.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:shoppingcart_app/com/adk/sc/applocalizations.dart';

class ADKDialog2 {
  static void showLoginAlertDialog(BuildContext context, Object? anException) {
    String aMessageText;
    if (anException == null) {
      aMessageText = AppLocalizations.of(context).translate("Invalid login");
    } else if (anException is FunctionalWarningException) {
      FunctionalWarningException aFunc = anException;
      //aMessageText = AppLocalizations.of(context).translate("Invalid login");
      //TODO i18n Should this always be returned already translated?
      aMessageText = aFunc.getMessageText();
    } else if (anException is ClientException ||
        anException is FatalWarningException) {
      aMessageText = AppLocalizations.of(context).translate(
          "System is down for maintenance.  Please try again in a few minutes.");
    } else {
      aMessageText = anException.toString();
    }
    // set up the button
    Widget okButton = TextButton(
      child: Text(AppLocalizations.of(context).translate("OK")),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    // set up the AlertDialog
    Widget anAlert = AlertDialog(
      content: Text(aMessageText),
      actions: [
        okButton,
      ],
    );
    // show the dialog

    anAlert = RawKeyboardListener(
        // its better to initialize and dispose of the focus node only for this alert dialog
        focusNode: FocusNode(),
        autofocus: true,
        onKey: (v) {
          if (v.logicalKey == LogicalKeyboardKey.enter) {
            Navigator.pop(context);
          }
        },
        child: anAlert);

    showDialog(
      useRootNavigator: false,
      context: context,
      builder: (BuildContext context) {
        return anAlert;
      },
    );
  }

  static void showAlertDialog(
      BuildContext context, Object? anException, int aPopDeep) {
    String aMessageText;
    if (anException is FunctionalWarningException) {
      aMessageText =
          AppLocalizations.of(context).translate(anException.getMessageText());
    } else if (anException is ClientException) {
      aMessageText = AppLocalizations.of(context).translate(
          "System is down for maintenance.  Please try again in a few minutes.");
    } else {
      aMessageText = anException.toString();
    }
    // set up the button
    Widget okButton = TextButton(
      child: Text(AppLocalizations.of(context).translate("OK")),
      onPressed: () {
        for (var i = 0; i < aPopDeep; i++) {
          Navigator.of(context).pop();
        }
      },
    );
    // set up the AlertDialog
    Widget alert = AlertDialog(
      content: Text(aMessageText),
      actions: [
        okButton,
      ],
    );
    alert = RawKeyboardListener(
// its better to initialize and dispose of the focus node only for this alert dialog
        focusNode: FocusNode(),
        autofocus: true,
        onKey: (v) {
          if (v.logicalKey == LogicalKeyboardKey.enter) {
            Navigator.pop(context);
          }
        },
        child: alert);

    // show the dialog
    showDialog(
      useRootNavigator: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  VoidCallback? closeDialog(BuildContext context) {
    Navigator.of(context).pop();
    return null;
  }

  static void showAlertDialogText(BuildContext context, String aText) {
    showDialog(
      useRootNavigator: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          //title: new Text("Alert!!"),
          content: new Text(aText),
          actions: <Widget>[
            new TextButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );

    /*
    showDialogText(
    useRootNavigator: false,
        context, 'Alert', aText, () => {Navigator.of(context).pop()});
  */
  }

  static void showDialogText(
      BuildContext context, String aTitle, String aText, Function x) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        x();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(aTitle),
      content: Text(aText),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      useRootNavigator: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static void showDialogTextXXX(
      BuildContext context, String aTitle, String aText, VoidCallback? x) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: x
      //(){
      //Navigator.of(context).pop();
      //}
      ,
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(aTitle),
      content: Text(aText),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      useRootNavigator: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
