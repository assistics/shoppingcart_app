import 'package:ast_app/com/adk/utility/adkcompare.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/business/passwordpolicy.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkbottomnavigationbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/passwordchangedpage.dart';

class ChangePasswordPage extends StatefulWidget {
  ChangePasswordPage() {}

  @override
  _ChangePasswordPageAppState createState() => _ChangePasswordPageAppState();
}

class _ChangePasswordPageAppState extends State<ChangePasswordPage> {
  final logger = Logger();
  GlobalKey<FormState> _formKey = new GlobalKey();
  String customerPO = "";
  String messageText = "";
  bool customerPOInvalid = false;
  bool _isLoading = false;
  bool placeOrderButtonPressed = false;

  _ChangePasswordPageAppState() {}

  @override
  Widget build(BuildContext context) {
    String aTitle;
    aTitle = 'Change Password';
    Widget aBody = this.getBody();
    aBody = ADKGlobal.scaffoldThinPage(context, aBody);
    return Scaffold(
      key: _formKey,
      appBar: ADKAppBar.buildBar(context, aTitle),
      body: Stack(children: [aBody, _showCircularProgress()]),
      bottomNavigationBar: ADKBottomNavigationBar.buildBar(context),
    );
  }

  Widget getBody() {
    // Form
    final TextEditingController _pass = TextEditingController();
    final TextEditingController _confirmPass = TextEditingController();
    GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    Widget aForm = Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 15),
          TextFormField(
            obscureText: true,
            controller: _pass,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'New Password',
            ),
            // The validator receives the text that the user has entered.
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please type in a new password';
              }
              return null;
            },
          ),
          SizedBox(height: 15),
          TextFormField(
            obscureText: true,
            controller: _confirmPass,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Confirm Password',
            ),
            // The validator receives the text that the user has entered.
            validator: (aConfirmPassword) {
              String aPassword = _pass.text;
              if (aPassword.isEmpty) {
                //FYI Validation method will be above on password field
                return null;
              }
              if (!ADKCompare.equals(aPassword, aConfirmPassword)) {
                return 'Confirmation password does not match the password.';
              }
              return null;
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: ElevatedButton(
              onPressed: () {
                if (!this.placeOrderButtonPressed) {
                  // Validate returns true if the form is valid, or false otherwise.
                  if (_formKey.currentState!.validate()) {
                    placeOrderButtonPressed = true;
                    setState(() {
                      this._isLoading = true;
                    });
                    //Attempt to change password
                    ADKURLBuilder().changedPassword(_pass.text).then((value) {
                      //UserData.setUser(value);
                      Route route = MaterialPageRoute(
                          builder: (context) => PasswordChangedPage());
                      Navigator.of(context).push(route);
                    }).catchError((all) {
                      placeOrderButtonPressed = false;
                      setState(() {
                        this._isLoading = false;
                      });
                      ADKDialog2.showAlertDialog(context, all, 1);
                    });
                  }
                }
              },
              child: const Text('Submit'),
            ),
          ),
        ],
      ),
    );

    // To validate call
    //_form.currentState.validate();
    Column aColumn = Column(children: [
      ADKGlobal.messageGreenWithSubtitle(
          context, 'Change Password', this.buildPolicyRules()),
      aForm
    ]);
    return aColumn;
  }

  Widget buildPolicyRules() {
    List<Widget> aList = List.empty(growable: true);
    List<String> aStringList = List.empty(growable: true);
    PasswordPolicy aPolicy = UserData.getUser().getPasswordPolicy();
    int aMinCharacters = aPolicy.getMinCharacters();
    if (aMinCharacters > 0) {
      aStringList.add('Minimum ' + aMinCharacters.toString() + ' characters');
    }
    int aMinNumericCharacters = aPolicy.getMinNumericCharacters();
    if (aMinNumericCharacters > 0) {
      String aText;
      if (aMinNumericCharacters == 1) {
        aText = 'Must contain at least one numeric character.';
      } else {
        aText = 'Must contain at least ' +
            aMinNumericCharacters.toString() +
            ' numeric characters.';
      }
      aStringList.add(aText);
    }
    int aMinSpecialCharacters = aPolicy.getMinSpecialCharacters();
    if (aMinSpecialCharacters > 0) {
      String aText;
      if (aMinSpecialCharacters == 1) {
        aText = 'Must contain at least one special character.';
      } else {
        aText = 'Must contain at least ' +
            aMinSpecialCharacters.toString() +
            ' special characters.';
      }
      aStringList.add(aText);
    }
    int aMinUppercaseCharacters = aPolicy.getMinUppercaseCharacters();
    if (aMinUppercaseCharacters > 0) {
      String aText;
      if (aMinUppercaseCharacters == 1) {
        aText = 'Must contain at least one uppercase character.';
      } else {
        aText = 'Must contain at least ' +
            aMinUppercaseCharacters.toString() +
            ' uppercase characters.';
      }
      aStringList.add(aText);
    }

    for (int i = 0; i < aStringList.length; i++) {
      String aText = aStringList[i];
      aList.add(Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("• "),
          Expanded(
            child: Text(aText),
          ),
        ],
      ));
    }
    Widget aP = Column(children: aList);
    return aP;
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }
}
