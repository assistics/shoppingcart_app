import 'package:ast_app/com/adk/ics/business/cart.dart';
import 'package:ast_app/com/adk/ics/business/cbsi.dart';
import 'package:ast_app/com/adk/ics/business/icsuser.dart';
import 'package:ast_app/com/adk/ics/business/salesordersession.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkmiscstringfunctions.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:shoppingcart_app/com/adk/sc/accountpage.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/ordertileforlist.dart';
import 'package:shoppingcart_app/com/adk/sc/selectcbspage.dart';

class OrderListView extends StatelessWidget {
  String? snackBarMessage;
  late VoidCallback actionRefresh;

  OrderListView(VoidCallback anActionRefresh, String? snackBarMessage) {
    this.actionRefresh = anActionRefresh;
    this.snackBarMessage = snackBarMessage;
  }

  bool isShowOpen() {
    return true;
  }

  @override
  Widget build(BuildContext context) {
    FutureBuilder<List<SalesOrderSession>?> aList =
        FutureBuilder<List<SalesOrderSession>?>(
      future: _fetchOpenOrders(),
      builder: (context, snapshot) {
        this._showSnackBar(context);

        if (snapshot.hasData) {
          List<SalesOrderSession>? data = snapshot.data;
          if (data == null || data.length == 0) {
            String aTitleText;
            if (this.isShowOpen()) {
              aTitleText = 'You don\'t have any open orders';
            } else {
              aTitleText = 'You don\'t have any past orders';
            }
            Widget aButtonBar = OverflowBar(
              children: <Widget>[
                TextButton(
                  child: const Text('Start Order'),
                  onPressed: () {
                    Cart aCart = UserData.getUser().getCart();

                    if (aCart.isCartInProgress()) {
                      ADKDialog2.showAlertDialogText(
                          context, 'A cart has already been started');
                    } else {
                      aCart.resetCart();
                      Route route = MaterialPageRoute(
                          builder: (context) =>
                              SelectCBSPage(this.actionRefresh));
                      Navigator.push(context, route);
                    }
                  },
                ),
              ],
            );

            ICSUser aUser = UserData.getUser();
            CBSI? aDefaultCBS = aUser.getDefaultCBS();
            if (aDefaultCBS != null && aUser.hasMultipleShipTos()) {
              Widget aColumn = Column(children: [
                _buildShipToCard(context, aDefaultCBS),
                ADKGlobal.messageYellow(context, aTitleText, aButtonBar)
              ]);
              return aColumn;
            } else {
              return ADKGlobal.messageYellow(context, aTitleText, aButtonBar);
            }
          } else {
            return _openOrdersListView(context, data);
          }
        } else if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        }
        return Center(child: CircularProgressIndicator());
      },
    );
    return aList;
  }

  Future<List<SalesOrderSession>?> _fetchOpenOrders() async {
    ADKURLBuilder aURLBuilder = new ADKURLBuilder();
    ICSUser aUser = UserData.getUser();
    String aShipToKey = '';
    CBSI? aDefaultCBS = aUser.getDefaultCBS();
    if (aDefaultCBS == null) {
      if (aUser.hasShipTos()) {
        aDefaultCBS = aUser.getCBSArray().first;
        aUser.setDefaultCBS(aDefaultCBS);
        aShipToKey = aDefaultCBS.getShipToKey();
      }
    } else {
      aShipToKey = aDefaultCBS.getShipToKey();
    }
    Future<List<SalesOrderSession>?> aList =
        aURLBuilder.fetchOpenSalesOrdersByShipTo(aShipToKey);
    return aList;
  }

  final ScrollController _controller = ScrollController();

  Widget _openOrdersListView(
      BuildContext context, List<SalesOrderSession> anAppointmentList) {
    Widget aWidget = ListView.builder(
        controller: _controller,
        itemCount: anAppointmentList.length,
        itemBuilder: (context, index) {
          return OrderTileForList(
              salesOrderSession: anAppointmentList[index],
              anActionRefresh: this.actionRefresh);
        });
    if (ADKGlobal.isWideScreen(context)) {
      List<Widget> aList = List.empty(growable: true);
      aList.add(ADKGlobal.messageGreen(context, 'Open Orders'));

      ICSUser aUser = UserData.getUser();
      CBSI? aDefaultCBS = aUser.getDefaultCBS();
      if (aDefaultCBS != null && aUser.hasMultipleShipTos()) {
        aList.add(_buildShipToCard(context, aDefaultCBS));
      }

      aList.add(Expanded(child: aWidget));
      Column aColumn = Column(
        children: aList,
      );
      aWidget =
          Padding(padding: EdgeInsets.fromLTRB(0, 0, 16, 0), child: aColumn);

      aWidget = Scrollbar(
          controller: _controller,
          //hoverThickness: 15,
          trackVisibility: false,
          thickness: 15,
          thumbVisibility: true,
          child: aWidget);
      return aWidget;
    } else {
      return aWidget;
    }
  }

  Widget _buildShipToCard(BuildContext context, CBSI aCBS) {
    CBSI anAddress = aCBS;
    List<Widget> aColumn = List.empty(growable: true);
    String anAddressLine1 = anAddress.getAddressLine1();
    String aLine1 = anAddressLine1;
    if (ADKMiscStringFunctions.isBlankOrWhitespace(aLine1)) {
      aLine1 = anAddress.getAddressLine2();
    }
    String anAddressCity = anAddress.getCity();
    String anAddressState = anAddress.getState();
    String anAddressPostalCode = anAddress.getPostalCode();
    String aLine2 =
        anAddressCity + ", " + anAddressState + " " + anAddressPostalCode;

    Icon anIcon;
    Color? aTileColor;
    aTileColor = Colors.blue[50];
    Color aColor;
    if (aCBS.isCBSOnHold()) {
      aColor = Colors.yellow;
    } else {
      aColor = Colors.blue;
    }

    anIcon = Icon(Icons.where_to_vote_outlined, color: aColor);
    aColumn.add(Text('Account #: ' + anAddress.getShipToKey()));
    aColumn.add(Text(anAddress.getShipToName()));
    if (!ADKMiscStringFunctions.isBlankOrWhitespace(aLine1)) {
      aColumn.add(Text(aLine1));
    }
    if (!ADKMiscStringFunctions.isBlankOrWhitespace(aLine2)) {
      aColumn.add(Text(aLine2));
    }
    ICSUser aUser = UserData.getUser();
    Widget aChangeWidgetShipTo;
    if (aUser.hasMultipleShipTos()) {
      aChangeWidgetShipTo = Align(
          alignment: Alignment.bottomRight,
          child: // Expanded(child:
              TextButton(
            child: Text('Switch Ship To'),
            onPressed: () {
              Route route =
                  MaterialPageRoute(builder: (context) => AccountPage());
              Navigator.of(context).push(route);
            },
          ));
    } else {
      aChangeWidgetShipTo = Text('');
    }
    Widget aDisplay = Card(
        elevation: 6.0,
        child: Stack(children: [
          ListTile(
            leading: anIcon,
            tileColor: aTileColor,
            title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: aColumn),
          ),
          aChangeWidgetShipTo,
        ]));

    return aDisplay;
  }

  _showSnackBar(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (this.snackBarMessage != null) {
        String msg = this.snackBarMessage!;
        this.snackBarMessage = null;
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(msg),
          duration: Duration(seconds: 6),
        ));
      }
    });
  }
}
