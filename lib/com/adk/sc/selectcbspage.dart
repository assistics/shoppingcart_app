import 'package:ast_app/com/adk/ics/business/calendardatesession.dart';
import 'package:ast_app/com/adk/ics/business/cart.dart';
import 'package:ast_app/com/adk/ics/business/cbsi.dart';
import 'package:ast_app/com/adk/ics/business/cbssession.dart';
import 'package:ast_app/com/adk/ics/business/icsuser.dart';
import 'package:ast_app/com/adk/utility/adkdatetime.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkmiscdatefunctions.dart';
import 'package:ast_app/com/adk/utility/adkmiscstringfunctions.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkbottomnavigationbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/adktheme.dart';
import 'package:shoppingcart_app/com/adk/sc/chooseproductpage.dart';
import 'package:shoppingcart_app/com/adk/sc/openorderspage.dart';

class SelectCBSPage extends StatefulWidget {
  late VoidCallback actionRefresh;

  SelectCBSPage(VoidCallback anActionRefresh) {
    this.actionRefresh = anActionRefresh;
  }

  @override
  _SelectCBSPageAppState createState() =>
      _SelectCBSPageAppState(this.actionRefresh);
}

class _SelectCBSPageAppState extends State<SelectCBSPage> {
  final logger = Logger();
  String _timezone = "";
  GlobalKey<FormState> _formKey = new GlobalKey();
  bool _validate = false;
  String customerPO = "";
  String messageText = "";
  bool customerPOInvalid = false;
  bool _isLoading = false;
  late VoidCallback actionRefresh;

  _SelectCBSPageAppState(VoidCallback anActionRefresh) {
    this.actionRefresh = anActionRefresh;
  }

  @override
  Widget build(BuildContext context) {
    String aTitle;
    Cart aCart = UserData.getUser().getCart();
    if (aCart.hasCBS()) {
      aTitle = 'Choose Delivery Date';
    } else {
      aTitle = 'Choose Your Location';
    }
    aTitle = '';
    Widget aBody = this.getBody();
    aBody = ADKGlobal.scaffoldThinPage(context, aBody);
    return Scaffold(
      key: _formKey,
      appBar: ADKAppBar.buildBar(context, aTitle),
      body: Stack(children: [aBody, _showCircularProgress()]),
      bottomNavigationBar: ADKBottomNavigationBar.buildBar(context),
    );
  }

  Widget getBody() {
    ICSUser aUser = UserData.getUser();
    Cart aCart = aUser.getCart();
    bool isShipToSpecified = aCart.hasCBS();
    if (!isShipToSpecified) {
      bool hasMultipleShipTos = aUser.hasMultipleShipTos();
      if (!hasMultipleShipTos) {
        List<CBSI> aCBSList = aUser.getCBSArray();
        if (aCBSList.isNotEmpty) {
          setState(() {
            this._isLoading = true;
          });
          CBSI aCBS = aCBSList.first;
          if (aCBS.isCBSOnHold()) {
            setState(() {
              this._isLoading = false;
              //sc.customerservice.telephone=
              Future.delayed(
                  Duration.zero,
                  () => ADKDialog2.showAlertDialogText(
                      context, ADKGlobal.scContactCustomerService()));
            });
          } else {
            aCart.setCBS(aCBS);
            Widget aSelector = this.getSelectedCBS(aCBS);

            ADKURLBuilder()
                .fetchAvailableRequiredDates(aCBS.getCustomerKey(),
                    aCBS.getBillToKey(), aCBS.getShipToKey())
                .then((List<CalendarDateSession> aList) {
              aCBS.setAvailableRequiredDates(aList);
              aCart.setCBS(aCBS);
              setState(() {
                this._isLoading = false;
                this._selectDate(context, aCBS);
              });
            }).catchError((all) {
              setState(() {
                this._isLoading = false;
              });
              ADKDialog2.showAlertDialog(context, all, 1);
            });
            return aSelector;
          }
        }
      }

      return this.getSelectShipTo();
    } else {
      return this.getSelectedCBS(aCart.getCBS());
    }
  }

  Widget getSelectShipTo() {
    ICSUser aUser = UserData.getUser();
    if (!aUser.hasShipTos()) {
      return Card(
        color: ADKTheme.CARD_WARNINGMESSAGE_COLOR,
        child: ListTile(
            leading: Icon(Icons.announcement_outlined, color: Colors.black),
            title: Text(
                'Your ship to address is not yet setup. Please contact customer service.')),
      );
    } else {
      Widget aBody = Column(children: <Widget>[
        SizedBox(
          height: 5,
        ),
        /*Padding(
            padding: EdgeInsets.only(left: 0),
            child:*/
        ADKGlobal.messageGreen(context, 'Choose a Ship To'),
        SizedBox(
          height: 20,
        ),
        Expanded(child: this._buildListView())
      ]);

      aBody = Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 8.0,
          vertical: 2.0,
        ),
        child: aBody,
      );

      if (ADKGlobal.isDesktop()) {
        //double aWidth = ADKGlobal.desktopCenterPaneWidth(context);
        aBody =
            Container(child: aBody, constraints: BoxConstraints(maxWidth: 500));
      }

      ADKGlobal.deviceWidth(context);
      return aBody;
    }
  }

  ListView _buildListView() {
    List<CBSI> aList = UserData.getUser().getCBSArray();
    return ListView.builder(
      itemCount: aList.length,
      itemBuilder: (context, index) {
        CBSI item = aList[index];
        return this._buildShipToCard(true, item);
      },
    );
  }

  Widget _buildShipToCard(bool inList, CBSI aCBS) {
    CBSI anAddress = aCBS;
    List<Widget> aColumn = List.empty(growable: true);
    String anAddressLine1 = anAddress.getAddressLine1();
    String aLine1 = anAddressLine1;
    if (ADKMiscStringFunctions.isBlankOrWhitespace(aLine1)) {
      aLine1 = anAddress.getAddressLine2();
    }
    String anAddressCity = anAddress.getCity();
    String anAddressState = anAddress.getState();
    String anAddressPostalCode = anAddress.getPostalCode();
    String aLine2 =
        anAddressCity + ", " + anAddressState + " " + anAddressPostalCode;

    Icon anIcon;
    Color? aTileColor;
    if (inList) {
      Color aColor;
      if (aCBS.isCBSOnHold()) {
        aColor = Colors.yellow;
      } else {
        aColor = Colors.blue;
      }
      anIcon = Icon(Icons.where_to_vote_outlined, color: aColor);
    } else {
      anIcon = Icon(
        Icons.location_on_outlined,
      );
      aTileColor = ADKTheme.CARD_CARTCARD_COLOR;
    }
    aColumn.add(Text("Account #: " + anAddress.getShipToKey()));
    aColumn.add(Text(anAddress.getShipToName()));
    if (!ADKMiscStringFunctions.isBlankOrWhitespace(aLine1)) {
      aColumn.add(Text(aLine1));
    }
    if (!ADKMiscStringFunctions.isBlankOrWhitespace(aLine2)) {
      aColumn.add(Text(aLine2));
    }
    Widget aListTile = ListTile(
        leading: anIcon,
        tileColor: aTileColor,
        title: Column(
            crossAxisAlignment: CrossAxisAlignment.start, children: aColumn),
        onTap: () {
          ICSUser aUser = UserData.getUser();
          //if (!aUser.hasMultipleShipTos() && 1 ==0) {
          //Disable select if only one ship to
          //  return;
          //}
          if (aCBS.isCBSOnHold()) {
            ADKDialog2.showAlertDialogText(
                context, ADKGlobal.scContactCustomerService());
          } else {
            Cart aCart = aUser.getCart();
            ADKURLBuilder()
                .fetchAvailableRequiredDates(aCBS.getCustomerKey(),
                    aCBS.getBillToKey(), aCBS.getShipToKey())
                .then((List<CalendarDateSession> aList) {
              aCBS.setAvailableRequiredDates(aList);
              aCart.setCBS(aCBS);
              setState(() {
                this._selectDate(context, aCBS);
              });
            }).catchError((all) {
              setState(() {
                this._isLoading = false;
              });
              ADKDialog2.showAlertDialog(context, all, 1);
            });
          }
        });
    Widget aDisplay;
    aDisplay = Card(
      elevation: 6.0,
      child: aListTile,
    );
    return aDisplay;
  }

  Widget getSelectedCBS(CBSI? aShipToCard) {
    CBSI aCard;
    if (aShipToCard == null) {
      aCard = new CBSSession();
    } else {
      aCard = aShipToCard;
    }
    List<Widget> aList = List.empty(growable: true);
    if (ADKGlobal.isWideScreen(context)) {
      aList.add(ADKGlobal.messageGreen(context, "Create Order"));
    }
    aList.add(this._buildShipToCard(false, aCard));
    return Column(children: aList);
  }

  Cart getCart() {
    Cart aCart = UserData.getUser().getCart();
    return aCart;
  }

  Future<Null> _selectDate(BuildContext context, CBSI aShipToCard) async {
    Cart aCart = this.getCart();
    CBSI? aCBS = aCart.getCBS();
    ADKDateTime? anInitialDate;
    ADKDateTime? aMinDate;
    ADKDateTime? aMaxDate;
    DateTime aDTInitialDate;
    DateTime aDTMinDate;
    DateTime aDTMaxDate;

    if (aCBS != null) {
      CBSI aNonNullCBS = aCBS;
      anInitialDate = aNonNullCBS.getDefaultDate();
      aMinDate = aNonNullCBS.getMinimumDate();
      aMaxDate = aNonNullCBS.getMaximumDate();
    }
    if (anInitialDate == null || aMinDate == null || aMaxDate == null) {
      ADKDialog2.showAlertDialogText(context,
          'Please contact customer service to finish setting up the routing for your account.  ');
      return null;
    } else {
      aDTInitialDate = anInitialDate.toDateTime();
      aDTMinDate = aMinDate.toDateTime();
      aDTMaxDate = aMaxDate.toDateTime();
    }
    List<CalendarDateSession> aCalendar;
    if (aCBS != null) {
      aCalendar = aCBS.getAvailableRequiredDates();
    } else {
      aCalendar = List.empty();
    }

    final DateTime? aPickedDate = await showDatePicker(
        context: context,
        selectableDayPredicate: (DateTime val) {
          return checkDate(val);
        },
        initialDate: aDTInitialDate,
        firstDate: aDTMinDate,
        lastDate: aDTMaxDate);

    if (aPickedDate != null) {
      this.setState(() {
        this._isLoading = true;
      });
      ADKDateTime aDeliveryDate = new ADKDateTime.fromDartDateTime(aPickedDate);
      aCart.setDeliveryDate(aDeliveryDate);

      //Retrieve products for date
      ADKURLBuilder aBuilder = new ADKURLBuilder();

      aBuilder.validateDeliveryDate().then((value) {
        Route route = MaterialPageRoute(
            builder: (context) => ChooseProductScreen(this.actionRefresh));
        Navigator.push(context, route);
      }).onError((error, stackTrace) {
        aCart.resetCart();
        ADKDialog2.showAlertDialog(context, error, 1);
        setState(() {
          this._isLoading = false;
        });
      });
    } else {
      aCart.resetCart();
      if (UserData.getUser().hasMultipleShipTos()) {
        //If more than one CBS, go back to select cbs screen
        Route route = MaterialPageRoute(
            builder: (context) => SelectCBSPage(this.actionRefresh));
        Navigator.push(context, route);
      } else {
        //Goto home screen
        Route route = MaterialPageRoute(
            builder: (context) => OpenOrdersPage(this.actionRefresh));
        Navigator.push(context, route);
      }
    }
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  bool checkDate(DateTime aPickerDartDate) {
    Cart aCart = this.getCart();
    CBSI? aCBS = aCart.getCBS();
    List<CalendarDateSession> aCalendar;
    if (aCBS != null) {
      aCalendar = aCBS.getAvailableRequiredDates();
      for (CalendarDateSession item in aCalendar) {
        ADKDateTime? aDate = item.getCalendarDate();
        if (aDate != null) {
          ADKDateTime aPickerDate =
              ADKDateTime.fromDartDateTime(aPickerDartDate);
          if (ADKMiscDateFunctions.equals(aDate, aPickerDate)) {
            return item.isAWorkDay();
          }
        }
      }
      ;
    } else {
      aCalendar = List.empty();
      return false;
    }
    return false;
  }
}
