import 'dart:io';
import 'dart:typed_data';

import 'package:ast_app/com/adk/ics/business/invoicesession.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/adktheme.dart';
import 'package:shoppingcart_app/com/adk/sc/pdfpage.dart';
import 'package:shoppingcart_app/com/adk/utility/adkscglobal.dart';
import 'package:shoppingcart_app/com/adk/utility/widget/invoicetile.dart';
import 'package:url_launcher/url_launcher_string.dart';

class InvoiceTileForList extends StatefulWidget {
  InvoiceSession invoiceSession = new InvoiceSession();

  InvoiceTileForList(InvoiceSession anInvoiceSession) {
    this.invoiceSession = anInvoiceSession;
  }

  @override
  _InvoiceTileForList createState() => _InvoiceTileForList(this.invoiceSession);
}

class _InvoiceTileForList extends State<InvoiceTileForList> {
  bool _isLoading = false;
  InvoiceSession invoiceSession = new InvoiceSession();

  _InvoiceTileForList(InvoiceSession anInvoiceSession) {
    this.invoiceSession = anInvoiceSession;
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget build(BuildContext context) {
    Widget aCheckBox = buildListTile(context);

    Card aCard = Card(
      child: aCheckBox,
    );
    Widget aStack =
        Stack(children: <Widget>[aCard, this._showCircularProgress()]);
    return aStack;
  }

  Widget buildListTile(BuildContext context) {
    InvoiceSession anInvoiceSession = this.invoiceSession;
    List<Widget> lines = InvoiceTile.buildTileTitle(anInvoiceSession);
    Widget? aLink;

    Widget aListTile;
    Widget? aThreeDots;
    if (ADKGlobal.isDesktop()) {
      aLink = GestureDetector(
        child: Padding(
            padding: const EdgeInsets.only(right: 20),
            child: Text('View',
                style: TextStyle(
                    decoration: TextDecoration.underline, color: Colors.blue))),
        onTap: () async {
          String url = ADKURLBuilder().getInvoiceReportURL(anInvoiceSession);
          launchUrlString(url, webOnlyWindowName: '_self');
        },
      );
      if (ADKSCGlobal.scAllowPayments()) {
        aThreeDots = aLink;
      } //lines.add(aLink);
    } else {
      aThreeDots = GestureDetector(
        onTapDown: (TapDownDetails tapDownDetails) =>
            onTapDown(context, tapDownDetails, anInvoiceSession),
        child: Icon(
          Icons.more_vert,
        ),
      );
    }
    if (ADKSCGlobal.scAllowPayments()) {
      aListTile = CheckboxListTile(
        contentPadding: EdgeInsets.only(left: 0),
        title: Column(
          children: lines,
        ),
        onChanged: (bool? aValue) {
          setState(() {
            if (aValue == null) {
              anInvoiceSession.setIsSelected(false);
            } else {
              anInvoiceSession.setIsSelected(aValue);
            }
          });
        },
        value: anInvoiceSession.isSelected(),
        secondary: aThreeDots,
        //leading: aLeadingIcon,
      );
    } else {
      aListTile = ListTile(
        onTap: () {},
        trailing: aLink,
        leading: aThreeDots,
        contentPadding: EdgeInsets.only(left: 20, top: 5, bottom: 5),
        title: Column(
          children: lines,
        ),
        //leading: aLeadingIcon,
      );
    }
    return aListTile;
  }

  void onTapDown(BuildContext context, TapDownDetails tapDownDetailsDetails,
      InvoiceSession anInvoiceSession) {
    double pressX = tapDownDetailsDetails.globalPosition.dx;
    double pressY = tapDownDetailsDetails.globalPosition.dy;
    if (ADKGlobal.isDesktop()) {
      pressX = tapDownDetailsDetails.globalPosition.dx - 150;
      pressY = tapDownDetailsDetails.globalPosition.dy - 70;
    } else {
      pressX = tapDownDetailsDetails.globalPosition.dx;
      pressY = tapDownDetailsDetails.globalPosition.dy;
    }
    showMenu(
      context: context,
      position: RelativeRect.fromLTRB(pressX, pressY, pressX, pressY),
      items: getMenuItems(anInvoiceSession),
    ).then(
      (value) => performSelectedAction(context, value, anInvoiceSession),
    );
  }

  List<PopupMenuEntry<dynamic>> getMenuItems(InvoiceSession anInvoiceSession) =>
      [
        PopupMenuItem(
          value: 'invoiceReport',
          child: Row(
            children: <Widget>[
              Icon(
                Icons.picture_as_pdf,
                color: ADKTheme.BUTTON_ICONCOLOR,
              ),
              (anInvoiceSession.isDocumentTypeInvoice() ||
                      anInvoiceSession.isDocumentTypeMiscInvoice())
                  ? Text("View Invoice Details")
                  : anInvoiceSession.isDocumentTypeCreditMemo()
                      ? Text("View Credit Memo Details")
                      : anInvoiceSession.isDocumentTypeCreditMemo()
                          ? Text("View Debit Memo Details")
                          : Text("View Details"),
            ],
          ),
        ),
      ];

  void performSelectedAction(
      BuildContext context, dynamic value, InvoiceSession anInvoiceSession) {
    switch (value) {
      case 'invoiceReport':
        setState(() {
          _isLoading = true;
        });
        ADKURLBuilder aBuilder = ADKURLBuilder();

        aBuilder.invoiceReport(anInvoiceSession).then((Uint8List value) {
          writePDFFile(value).then((File aFile) {
            setState(() {
              _isLoading = false;
            });
            Route route =
                MaterialPageRoute(builder: (context) => PDFPage(aFile));
            Navigator.of(context).push(route);
          }).onError((error, stackTrace) {
            setState(() {
              _isLoading = false;
            });
            String aMessageText = 'Invoice was unable to load';
            if (UserData.getUser().isSystemAdminType()) {
              aMessageText = aMessageText +
                  ' ' +
                  error.toString() +
                  '. ' +
                  stackTrace.toString();
            }
            ADKDialog2.showAlertDialogText(context, aMessageText);
          });
        }).onError((error, stackTrace) {
          setState(() {
            _isLoading = false;
          });
        });
        break;
      case 'option 3':
        print('option 3');
        break;
      default:
        break;
    }

    //return value;
  }

  Future<String> get _localPath async {
    //final Directory directory = await getApplicationDocumentsDirectory();
    final Directory directory = await getTemporaryDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final String path = await _localPath;
    String aDocumentKey = this.invoiceSession.getInvoiceKey();
    String aFilename;
    if (this.invoiceSession.isDocumentTypeInvoice() ||
        this.invoiceSession.isDocumentTypeMiscInvoice()) {
      aFilename = "Invoice " + aDocumentKey;
    } else if (this.invoiceSession.isDocumentTypeCreditMemo()) {
      aFilename = "Credit Memo " + aDocumentKey;
    } else {
      aFilename = aDocumentKey;
    }
    return File('$path/' + aFilename + '.pdf');
  }

  Future<File> writePDFFile(Uint8List stream) async {
    File aFile = await _localFile.onError((error, stackTrace) {
      setState(() {
        _isLoading = false;
      });
      throw Exception('PDF Failed to load');
    });

    // Write the file
    return aFile.writeAsBytes(stream);
  }

  Future<File> buildPDFFile(Uint8List stream) async {
    File aFile = await File.fromRawPath(stream);
    return aFile;
  }
}
