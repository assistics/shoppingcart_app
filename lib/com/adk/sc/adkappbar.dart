import 'package:ast_app/com/adk/ics/business/appointmentcart.dart';
import 'package:ast_app/com/adk/ics/business/cart.dart';
import 'package:ast_app/com/adk/ics/business/salesordersession.dart';
import 'package:ast_app/com/adk/ics/cg/salesorderlinesessioncg.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:shoppingcart_app/com/adk/sc/adktheme.dart';
import 'package:shoppingcart_app/com/adk/sc/cartpage.dart';
import 'package:shoppingcart_app/com/adk/sc/orderpage.dart';
import 'package:url_launcher/url_launcher.dart';

class ADKAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Color? backgroundColor = ADKTheme.APPBAR_BACKGROUNDCOLOR;
  final Text title;
  PreferredSizeWidget appBar = AppBar();
  List<Widget> widgets = List.empty(growable: true);

  static AppBar loginAppBar() {
    return new AppBar(
      title: new Text(ADKGlobal.loginPageTitle()),
      backgroundColor: ADKTheme.APPBAR_BACKGROUNDCOLOR,
    );
  }

  ADKAppBar({required this.title});

  static PreferredSizeWidget? buildBar(BuildContext context, String aTitle) {
    bool isWide = ADKGlobal.useLeftMenu(context);

    if (isWide) {
      return null;
    } else {
      return ADKAppBar(
        title: Text(aTitle),
      );
    }
  }

  @override
  PreferredSizeWidget build(BuildContext context) {
    List<Widget> anActionRow = List.empty(growable: true);

    if (ADKGlobal.getSCAppLabel().isNotEmpty ||
        !ADKGlobal.useLeftMenu(context)) {
      if (ADKGlobal.getSCAppLabel().isNotEmpty) {
        anActionRow.add(Text(ADKGlobal.getSCAppLabel(),
            style: TextStyle(fontSize: 18, color: Colors.yellow)));
      }
      if (!ADKGlobal.useLeftMenu(context)) {
        anActionRow.add(this.getCartWidget(context));
      }
    }
    Color? aBackgroundColor;
    PreferredSizeWidget aCartAppBar;
    if (ADKGlobal.useLeftMenu(context)) {
      aBackgroundColor = Colors.white;
      aCartAppBar = AppBar(
        toolbarHeight: 500,
        flexibleSpace: this.buildTitle(context),
        backgroundColor: aBackgroundColor,
        actions: anActionRow,
      );
    } else {
      aBackgroundColor = ADKTheme.APPBAR_BACKGROUNDCOLOR;
      aCartAppBar = AppBar(
        foregroundColor: Colors.white,
        title: this.buildTitle(context),
        backgroundColor: aBackgroundColor,
        actions: anActionRow,
      );

      aCartAppBar = PreferredSize(
          preferredSize: Size.fromHeight(150.0), child: aCartAppBar);
    }
    this.appBar = aCartAppBar;
    return aCartAppBar;
  }

  Widget getHamburgerMenu(BuildContext context) {
    String settingsVal = "settings";
    String logoutVal = "logout";
    String contactVal = "contact";
    String helpVal = "help";
    String homeVal = "home";
    String cartVal = "cart";
    return PopupMenuButton<String>(
      icon: Icon(Icons.menu),
      onSelected: (value) {
        if (value == logoutVal) {
          UserData.logout();
          Navigator.of(context).pushNamedAndRemoveUntil(
              ADKGlobal.PAGE_LOGIN, (Route<dynamic> route) => false);
        } else if (value == homeVal) {
          Navigator.of(context).pushNamedAndRemoveUntil(
              ADKGlobal.PAGE_HOME, (Route<dynamic> route) => false);
        } else if (value == cartVal) {
          Navigator.of(context).pushNamedAndRemoveUntil(
              ADKGlobal.PAGE_BOOK, (Route<dynamic> route) => false);
        } else if (value == settingsVal) {
          Navigator.of(context).pushNamedAndRemoveUntil(
              ADKGlobal.PAGE_SETTINGS, (Route<dynamic> route) => false);
        } else if (value == contactVal) {
          Navigator.of(context).pushNamedAndRemoveUntil(
              ADKGlobal.PAGE_CONTACT, (Route<dynamic> route) => false);
        } else if (value == helpVal) {
          String url = ADKGlobal.getHelpDocumentURL();
          //if (await canLaunch(url)) {
          launch(url);
          //} else {
          //  throw 'Could not launch $url';
          //}
        }
      },
      itemBuilder: (BuildContext context) {
        List<PopupMenuItem<String>> aList = List.empty(growable: true);
        bool isCartEnabled;
        AppointmentCart aCart = AppointmentCart.getCart();
        int numInCart = aCart.getOrderList().length;
        if (aCart.hasItem()) {
          isCartEnabled = true;
        } else {
          isCartEnabled = false;
        }
        aList.add(new PopupMenuItem<String>(
          value: cartVal,
          enabled: isCartEnabled,
          child: Row(
            children: <Widget>[
              Icon(
                Icons.shopping_cart,
                color: ADKTheme.BUTTON_ICONCOLOR,
              ),
              Text("Appointment Cart (" + numInCart.toString() + ")"),
            ],
          ),
        ));

        aList.add(new PopupMenuItem<String>(
          value: settingsVal,
          enabled: true,
          child: Row(
            children: <Widget>[
              Icon(
                Icons.settings,
                color: ADKTheme.BUTTON_ICONCOLOR,
              ),
              Text("Settings"),
            ],
          ),
        ));

        String url = ADKGlobal.getHelpDocumentURL();
        if (url.length > 0) {
          aList.add(new PopupMenuItem<String>(
            value: helpVal,
            enabled: true,
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.help,
                  color: ADKTheme.BUTTON_ICONCOLOR,
                ),
                Text("Help"),
              ],
            ),
          ));
        }
        aList.add(new PopupMenuItem<String>(
          value: contactVal,
          enabled: true,
          child: Row(
            children: <Widget>[
              Icon(
                Icons.contact_phone,
                color: ADKTheme.BUTTON_ICONCOLOR,
              ),
              Text("Contact Us"),
            ],
          ),
        ));

        aList.add(new PopupMenuItem<String>(
          value: logoutVal,
          child: Row(
            children: <Widget>[
              Icon(
                Icons.exit_to_app,
                color: ADKTheme.BUTTON_ICONCOLOR,
              ),
              Text("Logout"),
            ],
          ),
        ));
        return aList;
      },
    );
  }

  Widget getCartWidget(BuildContext context) {
    Cart aCart = UserData.getUser().getCart();
    List<SalesOrderLineSessionCG> aCartList = aCart.getCartList();
    return Padding(
      padding: const EdgeInsets.only(right: 16.0, top: 8.0),
      child: GestureDetector(
        child: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            Icon(
              Icons.shopping_cart,
              size: 36.0,
            ),
            if (aCart.isCartInProgress() && aCartList.length > 0)
              Padding(
                padding: const EdgeInsets.only(left: 2.0, top: 0),
                child: CircleAvatar(
                  radius: 8.0,
                  backgroundColor: Colors.red,
                  foregroundColor: Colors.white,
                  child: Text(
                    aCartList.length.toString(),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 12.0,
                    ),
                  ),
                ),
              ),
          ],
        ),
        onTap: () {
          if (aCart.isCartInProgress()) if (aCart.isCartInUpdateMode()) {
            SalesOrderSession? aSalesOrder = aCart.getSalesOrderSession();
            if (aSalesOrder != null) {
              void Function() aRefreshFunction = () {
                //setState(() {});
              };

              Route route = MaterialPageRoute(
                  builder: (context) =>
                      OrderPage(aSalesOrder, true, aRefreshFunction));
              Navigator.of(context).push(route);
            }
          } else {
            void Function() aRefreshFunction = () {
              //setState(() {});
            };
            Route route = MaterialPageRoute(
                builder: (context) => CartPage(aRefreshFunction));
            Navigator.of(context).push(route);

            //Navigator.of(context).pushNamed(
            //    ADKSCRoutes.PAGE_CART, (Route<dynamic> route) => false);
          }
        },
      ),
    );
  }

  Widget buildTitle(BuildContext context) {
    if (ADKGlobal.useLeftMenu(context)) {
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          CircleAvatar(
            radius: 35.0,
            child: ClipOval(
                child: Image.asset(
              ADKGlobal.getLogoFilename(),
              fit: BoxFit.contain,
              height: 43,
            )),
            backgroundColor: Colors.white,
          ),
          Container(padding: const EdgeInsets.all(8.0), child: this.title)
        ],
      );
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            ADKGlobal.getLogoFilename(),
            fit: BoxFit.contain,
            height: 100,
          ),
          //Container(padding: const EdgeInsets.all(8.0), child: this.title)
          Container(padding: const EdgeInsets.all(50.0), child: this.title)
        ],
      );
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          CircleAvatar(
            radius: 35.0,
            child: ClipOval(
                child: Image.asset(
              ADKGlobal.getLogoFilename(),
              fit: BoxFit.contain,
              height: 43,
            )),
            backgroundColor: Colors.white,
          ),
          Container(padding: const EdgeInsets.all(8.0), child: this.title)
        ],
      );
    }
  }

  // @override
  Size get preferredSize => new Size.fromHeight(appBar.preferredSize.height);
//Size get preferredSize => new Size.fromHeight(150);
}
