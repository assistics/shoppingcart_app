import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:flutter/material.dart';
import 'package:shoppingcart_app/com/adk/sc/adkappbar.dart';
import 'package:shoppingcart_app/com/adk/sc/adkbottomnavigationbar.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class PDFPageFromPath extends StatefulWidget {
  late String file;

  PDFPageFromPath(String aFile) {
    this.file = aFile;
  }

  _PDFPageFromPathState createState() => _PDFPageFromPathState(this.file);
}

class _PDFPageFromPathState extends State<PDFPageFromPath> {
  bool _isLoading = false;
  late String file;

  _PDFPageFromPathState(String aFile) {
    this.file = aFile;
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();

  @override
  Widget build2(BuildContext context) {
    Widget aScaffold = Scaffold(
      key: scaffoldKey,
      appBar: ADKAppBar.buildBar(context, 'Invoice'),
      body: Stack(children: <Widget>[
        Center(child: this.getBody()),
        this._showCircularProgress()
      ]),
      bottomNavigationBar: ADKBottomNavigationBar.buildBar(context), //    )
    );
    return aScaffold;
  }

  Widget build(BuildContext context) {
    print(this.file);
    String aPath = ADKURLBuilder().baseURL() + this.file;
    print(aPath);
    return Scaffold(
        body: SfPdfViewer.network(
      aPath,
      key: _pdfViewerKey,
    ));
  }

  Widget getBody() {
    //String path = this.file.path;
    return SingleChildScrollView(
      child: Stack(alignment: Alignment.center, children: [
        Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: InteractiveViewer(
              minScale: 0.2,
              maxScale: 10,
              child: SfPdfViewer.network(
                this.file,
                onDocumentLoaded: (PdfDocumentLoadedDetails details) {
                  print(details.document.pages.count);
                },
                onDocumentLoadFailed: (PdfDocumentLoadFailedDetails details) {
                  print(details.description);
                  AlertDialog(
                    title: Text(details.error),
                    content: Text(details.description),
                    actions: <Widget>[
                      TextButton(
                        child: Text('OK'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
                key: _pdfViewerKey,
              ),
            )

//        child: PdfView(
//          path: path,
//        ),
            ),
        this.shareButton(),
      ]),
    );
  }

  Widget shareButton() {
    Icon aShareIcon;
    IconData anIconData;
    bool isIOS = Theme.of(context).platform == TargetPlatform.iOS;
    if (isIOS) {
      anIconData = Icons.ios_share;
    } else {
      anIconData = Icons.share;
    }
    aShareIcon = Icon(anIconData, color: Colors.white);
    return Positioned(
      top: 0.0,
      right: 0.0,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ElevatedButton(
            style: ButtonStyle(
                backgroundColor:
                    WidgetStateProperty.all(Colors.black.withOpacity(0.05))),
            //Colors.black.withOpacity(0.05)
            onPressed: () => this.share(),
            child: aShareIcon),
      ),
    );
  }

  share() {
    //String aPath = this.file.path;
    //Share.shareFiles([aPath]);
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }
}
