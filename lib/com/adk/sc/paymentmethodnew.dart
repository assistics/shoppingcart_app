import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkjsonresponse.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/aliaswireachinfo.dart';
import 'package:ast_app/com/adk/utility/aliaswireachresponse.dart';
import 'package:ast_app/com/adk/utility/creditcard.dart';
import 'package:ast_app/com/adk/utility/creditcardresponse.dart';
import 'package:flutter/material.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/paymentmethodspage.dart';
import 'package:shoppingcart_app/com/adk/utility/slimcdjs.dart'
    if (dart.library.html) 'package:ast_web/com/adk/utility/slimcdjs.dart'
    if (dart.library.io) 'package:shoppingcart_app/com/adk/utility/slimcdjs.dart';
import 'package:shoppingcart_app/com/adk/utility/aliaswirejs.dart'
    if (dart.library.html) 'package:ast_web/com/adk/utility/aliaswirejs.dart'
    if (dart.library.io) 'package:shoppingcart_app/com/adk/utility/aliaswirejs.dart';

class PaymentMethodNew extends StatefulWidget {
  PaymentMethodNew();

  @override
  State<StatefulWidget> createState() {
    return PaymentMethodNewState();
  }
}

class PaymentMethodNewState extends State<PaymentMethodNew>
    with SingleTickerProviderStateMixin {
  String _cardNumber = '';
  String _expiryDate = '';
  String _cardHolderName = '';
  String _email = '';
  String _cvvCode = '';
  bool isCvvFocused = false;
  bool _isLoading = true;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> achFormKey = GlobalKey<FormState>();
  final TextEditingController postalCodeController = TextEditingController();

  final TextEditingController fullName = TextEditingController();
  final TextEditingController bankAccountNumber = TextEditingController();
  final TextEditingController accountNumberConfirmation =
      TextEditingController();
  final TextEditingController routingNumber = TextEditingController();

  TabController? _tabController;
  String? selectedValue = '';

  PaymentMethodNewState() {}

  void initState() {
    _tabController = new TabController(length: 2, vsync: this);
    super.initState();
    this.asyncMethod();
  }

  static final String _PREF_REMEMBER_CARD_INFO = 'remembercreditcardinfo';
  static final String _PREF_CARD_NUMBER = 'cardnumber';
  static final String _PREF_CARD_HOLDER_NAME = 'cardholdername';
  static final String _PREF_EXPIRY_DATE = 'expirydate';
  static final String _PREF_CVVCODE = 'cvvcode';

  void asyncMethod() async {
    _isLoading = true;
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget aCreditCardForm = this.creditCardTab();
    Widget anACHForm = this.achTab();

    Widget aBody = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          /*
          Container(
            //height: MediaQuery.of(context).size.height / 2,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Enter Payment Method",
                style: TextStyle(color: Colors.black, fontSize: 25),
              ),
            ),
            color: Colors.white,
          ),
          Divider(thickness: 1, color: Colors.black),
           */
          TabBar(
            unselectedLabelColor: Colors.blue,
            labelColor: Colors.white,
            indicator: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                //  color: Colors.indigoAccent),
                color: Color(0xFF4100FF)),
            tabs: [
              Tab(
                child: Row(
                    children: [Icon(Icons.credit_card), Text('Credit Card')]),
              ),
              Tab(
                child: Row(children: [
                  Icon(Icons.account_balance),
                  Text('Bank Account')
                ]),
              )
            ],
            controller: _tabController,
            indicatorSize: TabBarIndicatorSize.tab,
          ),
          Expanded(
            child: TabBarView(
              children: [aCreditCardForm, anACHForm],
              controller: _tabController,
            ),
          ),
        ],
      ),
    );

    Widget a1 = SafeArea(
        child: Stack(children: [
      aBody,
      _showCircularProgress(),
    ]));

    aBody = SafeArea(
        child: Stack(children: [
      aBody,
      _showCircularProgress(),
    ]));

    aBody = ADKGlobal.scaffoldFullPage(context, aBody);
    Widget aScaffold = Scaffold(body: aBody);

    return GestureDetector(
        onTap: // () => FocusManager.instance.primaryFocus?.unfocus(),
            () {
          //FocusScope.of(context).unfocus();
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: DefaultTabController(length: 2, child: aScaffold));
  }

  Widget accountTypeWidget() {
    //'Checking'  CHECKING
    //'Savings'   SAVINGS
    //'Business Checking'  BIZCHECKING

    List<DropdownMenuItem<String>> menuItems = [
      DropdownMenuItem(child: Text('Choose an account type'), value: ''),
      DropdownMenuItem(child: Text('Checking'), value: 'CHECKING'),
      DropdownMenuItem(child: Text('Savings'), value: 'SAVINGS'),
      DropdownMenuItem(child: Text('Business Checking'), value: 'BIZCHECKING'),
    ];

    Widget aDropDown = DropdownButtonFormField(
      decoration: InputDecoration(
          isDense: true,
          border: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(5.0),
            ),
          ),
          //filled: true,
          //hintStyle: TextStyle(color: Colors.grey[800]),
          //hintText: "Account Type",
          fillColor: Colors.white),
      value: selectedValue,
      items: menuItems,
      validator: (value) =>
          value == null || value == '' ? "Select an account type" : null,
      onChanged: (String? newValue) {
        setState(() {
          selectedValue = newValue!;
        });
      },
    );

    return aDropDown;
  }

  Widget createField(String aLabelText, TextEditingController aController,
      String? Function(String?) aValidator) {
    String aDefault = '';
    bool hasFocus = false;

    Widget aField = Container(
        margin: EdgeInsets.all(10),
        child: TextFormField(
          //initialValue: aDefault,
          autofocus: hasFocus,
          //keyboardType: anInputType,
          //inputFormatters: inputFormatters,

          //textCapitalization: aTextCapitalization,
          validator: aValidator,
          //onChanged: onChanged,
          controller: aController,
          style: TextStyle(fontSize: 16),
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: aLabelText,
            errorStyle: TextStyle(
              fontSize: 16.0,
            ),
          ),
        ));
    return aField;
  }

  String? Function(String?) buildValidator() {
    String? Function(String?) aValidator = (String? aValue) {
      if (aValue == null) {
        return null;
      }
      if (aValue.isEmpty) {
        //return AppLocalizations.of(context)!.valueIsRequired;
        return 'Value is required';
      }

      return null;
    };
    return aValidator;
  }

  String? Function(String?) buildBankAccountNumberValidator() {
    String? Function(String?) aValidator = (String? aValue) {
      if (aValue == null) {
        return null;
      }
      if (aValue.isEmpty) {
        //return AppLocalizations.of(context)!.valueIsRequired;
        return 'Value is required';
      }
      String aConfirm = this.accountNumberConfirmation.value.text;
      if (aValue != aConfirm) {
        return 'Bank account does not match';
      }
      int minChars = 3;
      if (minChars > 0 && aValue.length < minChars) {
        //TODO i18n
        return 'Value must be at least ' + minChars.toString() + ' digits long';
      }

      return null;
    };
    return aValidator;
  }

  String? Function(String?) buildRoutingNumberValidator() {
    String? Function(String?) aValidator = (String? aValue) {
      if (aValue == null) {
        return null;
      }
      if (aValue.isEmpty) {
        //return AppLocalizations.of(context)!.valueIsRequired;
        return 'Value is required';
      }
      int minChars = 9;
      if (aValue.length != minChars) {
        //TODO i18n
        return 'Value must be ' + minChars.toString() + ' digits long';
      }

      return null;
    };
    return aValidator;
  }

  Widget achTab() {
    Widget anAccountType = accountTypeWidget();
    String? Function(String?) aNameValidator = this.buildValidator();
    Widget aFullName =
        this.createField('Name on Account', this.fullName, aNameValidator);

    String? Function(String?) aBankAccountValidator =
        this.buildBankAccountNumberValidator();
    Widget aBankAccountNumber = this.createField(
        'Bank Account Number', this.bankAccountNumber, aBankAccountValidator);
    String? Function(String?) aConfirmValidator = this.buildValidator();
    Widget aConfirmBankAccountNumber = this.createField(
        'Confirm Account Number',
        this.accountNumberConfirmation,
        aConfirmValidator);
    Widget aBankRoutingNumber = this.createField('Bank Routing Number',
        this.routingNumber, this.buildRoutingNumberValidator());
    Widget aColumn = Column(children: [
      SizedBox(height: 10),
      anAccountType,
      aFullName,
      aBankAccountNumber,
      aConfirmBankAccountNumber,
      aBankRoutingNumber,
      saveACHButton()
    ]);

    Form aForm = Form(key: achFormKey, child: aColumn);

    //aColumn = Expanded(child: SingleChildScrollView(child: aColumn));
    aColumn = SingleChildScrollView(child: aForm);

    return aColumn;
  }

  Widget creditCardTab() {
    TextStyle aStyle = TextStyle(fontSize: 22);

    Widget aCardWidget = CreditCardWidget(
      height: 160,
      cardNumber: this._cardNumber,
      expiryDate: this._expiryDate,
      cardHolderName: this._cardHolderName,
      cvvCode: this._cvvCode,
      showBackView: this.isCvvFocused,
      obscureCardNumber: true,
      obscureCardCvv: true,
      //TODO
      onCreditCardWidgetChange: (CreditCardBrand) {},
    );
    Widget aCardForm = CreditCardForm(
      formKey: formKey,
      obscureCvv: true,
      obscureNumber: true,
      cardNumber: this._cardNumber,
      cvvCode: this._cvvCode,
      cardHolderName: this._cardHolderName,
      expiryDate: this._expiryDate,
      //TODO
      //themeColor: Colors.blue,
      inputConfiguration: const InputConfiguration(
          cardNumberDecoration: const InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Number',
            hintText: 'XXXX XXXX XXXX XXXX',
          ),
          expiryDateDecoration: const InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Expired Date',
            hintText: 'XX/XX',
          ),
          cvvCodeDecoration: const InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'CVV',
            hintText: 'XXX',
          ),
          cardHolderDecoration: const InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Card Holder',
          )),
      onCreditCardModelChange: onCreditCardModelChange,
    );
    InputDecoration postalCodeDecoration = const InputDecoration(
      border: OutlineInputBorder(),
      labelText: 'Postal Code',
    );

    Widget aPostalCode = Container(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      margin: const EdgeInsets.only(left: 16, top: 8, right: 16),
      child: TextFormField(
        controller: postalCodeController,
        //cursorColor: widget.cursorColor ?? themeColor,
        //focusNode: cardHolderNode,
        style: TextStyle(color: Colors.black //widget.textColor,
            ),
        decoration: postalCodeDecoration,
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.done,
        //onEditingComplete: () {
        //  onCreditCardModelChange(creditCardModel);
        //},
      ),
    );

    Widget aBody = Column(
      children: <Widget>[
        SizedBox(height: 5),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                aCardWidget,
                aCardForm,
                aPostalCode,
                this.saveButton(),
              ],
            ),
          ),
        ),
      ],
    );
    return aBody;
  }

  Widget saveButton() {
    Widget aButton = ElevatedButton.icon(
        onPressed: () => saveCard(),
        icon: new Icon(Icons.add_circle_outline, size: 30, color: Colors.green),
        label: Text('Save Payment Method'));

    return aButton;
  }

  Widget saveACHButton() {
    Widget aButton = ElevatedButton.icon(
        onPressed: () => saveACH(),
        icon: new Icon(Icons.add_circle_outline, size: 30, color: Colors.green),
        label: Text('Save Payment Method'));

    return aButton;
  }

  void saveCard() async {
    this.setState(() {
      this._isLoading = true;
    });

    CreditCard aCreditCard = CreditCard(this._cardHolderName, this._email,
        this._cardNumber, this._expiryDate, this._cvvCode);
    if (formKey.currentState!.validate()) {
      this.setState(() {
        this._isLoading = true;
      });

      CreditCardResponse aCreditCardResponse =
          await SlimcdJS().retrieveSLIMCDToken(aCreditCard).catchError((error) {
        this.setState(() {
          this._isLoading = false;
        });
        return new CreditCardResponse.error(error.toString());
      });
      this.setState(() {
        this._isLoading = false;
      });

      if (aCreditCardResponse.isError()) {
        ADKDialog2.showAlertDialogText(
            context, aCreditCardResponse.getErrorMessage());
      } else {
        this.setState(() {
          this._isLoading = true;
        });
        try {
          ADKJSONResponse aResponse = await ADKURLBuilder().addPaymentMethod(
              aCreditCard,
              aCreditCardResponse,
              this.postalCodeController.value.text);

          this.setState(() {
            this._isLoading = false;
          });

          if (aResponse.isSuccess()) {
            ADKDialog2.showDialogText(
                context,
                '',
                'Payment method was saved',
                () => {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PaymentMethodsPage()))
                    });
          } else {
            ADKDialog2.showAlertDialogText(context, aResponse.getMessageText());
            this.setState(() {
              this._isLoading = false;
            });
          }
        } catch (all) {
          ADKDialog2.showAlertDialog(context, all, 1);
          this.setState(() {
            this._isLoading = false;
          });
        }
      }
    } else {
      ADKDialog2.showAlertDialogText(
          context, 'Invalid credit card information');
      this.setState(() {
        this._isLoading = false;
      });
    }
  }

  void saveACH() async {
    this.setState(() {
      this._isLoading = true;
    });
    String? aSelectedAccountType = this.selectedValue;
    String anAccountType = '';
    if (aSelectedAccountType != null) {
      anAccountType = aSelectedAccountType;
    }
    String aFullName = this.fullName.value.text;
    String aBankAccountNumber = this.bankAccountNumber.value.text;
    String aRoutingNumber = this.routingNumber.value.text;

    AliasWireACHInfo anACHInfo = AliasWireACHInfo(
        anAccountType, aFullName, aBankAccountNumber, aRoutingNumber);

    if (this.achFormKey.currentState!.validate()) {
      this.setState(() {
        this._isLoading = true;
      });

      AliasWireACHResponse anACHResponse =
          await AliasWireJS().retrieveACHToken(anACHInfo).catchError((error) {
        this.setState(() {
          this._isLoading = false;
        });
        return new CreditCardResponse.error(error.toString());
      });
      this.setState(() {
        this._isLoading = false;
      });

      if (anACHResponse.isError()) {
        ADKDialog2.showAlertDialogText(
            context, anACHResponse.getErrorMessage());
      } else {
        this.setState(() {
          this._isLoading = true;
        });
        try {
          ADKJSONResponse aResponse = await ADKURLBuilder()
              .addAliasWirePaymentMethod(anACHInfo, anACHResponse);

          this.setState(() {
            this._isLoading = false;
          });

          if (aResponse.isSuccess()) {
            ADKDialog2.showDialogText(
                context,
                '',
                'Payment method was saved',
                () => {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PaymentMethodsPage()))
                    });
          } else {
            ADKDialog2.showAlertDialogText(context, aResponse.getMessageText());
            this.setState(() {
              this._isLoading = false;
            });
          }
        } catch (all) {
          ADKDialog2.showAlertDialog(context, all, 1);
          this.setState(() {
            this._isLoading = false;
          });
        }
      }
    } else {
      ADKDialog2.showAlertDialogText(
          context, 'Invalid bank account information');
      this.setState(() {
        this._isLoading = false;
      });
    }
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  void onCreditCardModelChange(CreditCardModel? creditCardModel) async {
    setState(() {
      _cardNumber = creditCardModel!.cardNumber;
      _expiryDate = creditCardModel.expiryDate;
      _cardHolderName = creditCardModel.cardHolderName;
      _cvvCode = creditCardModel.cvvCode;
      isCvvFocused = creditCardModel.isCvvFocused;
    });
  }
}
