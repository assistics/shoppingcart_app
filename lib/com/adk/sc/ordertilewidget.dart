import 'package:ast_app/com/adk/ics/business/addressi.dart';
import 'package:ast_app/com/adk/ics/business/cart.dart';
import 'package:ast_app/com/adk/ics/business/salesordersession.dart';
import 'package:ast_app/com/adk/utility/adkcompare.dart';
import 'package:ast_app/com/adk/utility/adkdatetime.dart';
import 'package:ast_app/com/adk/utility/adkglobal.dart';
import 'package:ast_app/com/adk/utility/adkmiscstringfunctions.dart';
import 'package:ast_app/com/adk/utility/adkurlbuilder.dart';
import 'package:ast_app/com/adk/utility/environmentlocale.dart';
import 'package:ast_app/com/adk/utility/userdata.dart';
import 'package:flutter/material.dart';
import 'package:shoppingcart_app/com/adk/sc/adkdialog2.dart';
import 'package:shoppingcart_app/com/adk/sc/adktheme.dart';
import 'package:shoppingcart_app/com/adk/sc/chooseproductpage.dart';
import 'package:shoppingcart_app/com/adk/sc/openorderspage.dart';
import 'package:shoppingcart_app/com/adk/sc/orderpage.dart';
import 'package:shoppingcart_app/com/adk/utility/adkscglobal.dart';

class OrderTileWidget extends StatefulWidget {
  SalesOrderSession salesOrderSession;
  bool isLoading = false;
  bool isEdit;
  bool isAddProductMode;

  late VoidCallback actionRefresh;

  OrderTileWidget(
      {required this.salesOrderSession,
      required this.isEdit,
      required this.isAddProductMode,
      required this.actionRefresh,
      required Key key})
      : super(key: key) {}

  @override
  OrderTileWidgetState createState() => OrderTileWidgetState();
}

class OrderTileWidgetState extends State<OrderTileWidget> {
  OrderTileWidgetState() {
    //this.cbs = aShipToCard;
    //this.isCheckOut = anIsCheckOut;
    //this.actionRefresh = anActionRefresh;
  }

  @override
  Widget build(BuildContext context) {
    return orderTile(context, widget.salesOrderSession);
  }

  //Widget OrderTileWidget(BuildContext context, SalesOrderSession aSalesOrder);

  bool isListOrderMode() {
    return false;
  }

  Widget buildTileTitle(BuildContext context, SalesOrderSession aSalesOrder) {
    if (ADKGlobal.isWideScreen(context)) {
      return this.buildTileTitleWide(context, aSalesOrder);
    }
    List<Row> lines = [];
    if (aSalesOrder.getCustomerPO().isNotEmpty) {
      lines.add(Row(children: [
        new Text("Purchase Order: "),
        new Text(aSalesOrder.getCustomerPO(),
            style: TextStyle(fontWeight: FontWeight.bold)),
      ]));
    }
    if (aSalesOrder.getSalesOrderKey().isNotEmpty) {
      lines.add(Row(children: [
        new Text("Sales Order: "),
        new Text(aSalesOrder.getSalesOrderKey(),
            style: TextStyle(fontWeight: FontWeight.bold)),
      ]));
    }

    AddressI anAddress = aSalesOrder.getShipToAddress();
    String anAddressLine = "";
    String? anAddressLine1 = anAddress.getAddressLine1();
    String? anAddressCity = anAddress.getCity();
    String? anAddressState = anAddress.getState();
    String? anAddressPostalCode = anAddress.getPostalCode();
    if ((anAddressLine1.isNotEmpty ||
            anAddressCity.isNotEmpty ||
            anAddressState.isNotEmpty ||
            anAddressPostalCode.isNotEmpty)) {
      anAddressLine = /*anAddressLine1 +
          ", " +*/
          anAddressCity + ", " + anAddressState + " " + anAddressPostalCode;
    }

    ADKDateTime? aDeliveryDate = aSalesOrder.getDeliveryDate();
    if (aDeliveryDate != null) {
      lines.add(Row(children: [
        //new Text("Delivery: "),
        new Text(
          UserData.getLocale().displayDateTimeFormat(
              aDeliveryDate, EnvironmentLocale.DAYMONTHYYYY),
          style: TextStyle(fontWeight: FontWeight.bold),
        )
      ]));
    }
    String aShipToString = aSalesOrder.getShipToKey();
    if (aSalesOrder.getShipToName().isNotEmpty) {
      aShipToString = aShipToString + ' ' + aSalesOrder.getShipToName();
    } else if (anAddress.getAddressLine1().isNotEmpty) {
      aShipToString = aShipToString + ' ' + anAddress.getAddressLine1();
    }

    lines.add(Row(children: [new Text(aShipToString)]));

    lines.add(Row(children: [new Text(anAddressLine)]));

    return Column(children: lines);
  }

  Widget buildTileTitleWide(
      BuildContext context, SalesOrderSession aSalesOrder) {
    EnvironmentLocale aLocale = UserData.getLocale();
    ADKDateTime? aDeliveryDate = aSalesOrder.getDeliveryDate();
    Widget aDeliveryDateWidget;
    if (aDeliveryDate != null) {
      aDeliveryDateWidget = Row(children: [
        Text('Delivery Date: ', style: TextStyle(fontWeight: FontWeight.bold)),
        Text(
          aLocale.displayDateTimeFormat(
              aDeliveryDate, EnvironmentLocale.DAYMONTHYYYY),
        ),
      ]);
    } else {
      aDeliveryDateWidget = Row(children: [
        Text('Delivery Date: ', style: TextStyle(fontWeight: FontWeight.bold)),
        Text(
          '',
        ),
      ]);
    }
    AddressI anAddress = aSalesOrder.getShipToAddress();
    String anAddressLine1 = anAddress.getAddressLine1();
    if (anAddressLine1.trim().isEmpty) {
      anAddressLine1 = anAddress.getAddressLine2();
    }
    String anAddressLine = "";
    String? anAddressCity = anAddress.getCity();
    String? anAddressState = anAddress.getState();
    String? anAddressPostalCode = anAddress.getPostalCode();
    if ((anAddressCity.isNotEmpty ||
            anAddressState.isNotEmpty ||
            anAddressPostalCode.isNotEmpty)) {
      anAddressLine =
          // ", " +
          anAddressCity + ", " + anAddressState + " " + anAddressPostalCode;
    }
    Widget aPriceWidget;
    Widget aSpace = Text('');
    if (ADKGlobal.scShowPrices()) {
      aPriceWidget = Text(
          'Total: ' + ADKSCGlobal.scDisplayTotalPrice(aLocale, aSalesOrder));
    } else {
      aPriceWidget = aSpace;
    }
    Map<int, TableColumnWidth> aMap = <int, TableColumnWidth>{
      //0: IntrinsicColumnWidth(),
      //1: FlexColumnWidth(),
      0: IntrinsicColumnWidth(),
      1: IntrinsicColumnWidth(),
      2: IntrinsicColumnWidth(),
    };
    Widget x1y1 = aDeliveryDateWidget;
    Widget x1y2 = Row(children: [
      Text('Purchase Order: ', style: TextStyle(fontWeight: FontWeight.bold)),
      Text(
        aSalesOrder.getCustomerPO(),
      ),
    ]);
    Widget x1y3;
    String aSalesOrderKey = aSalesOrder.getSalesOrderKey();
    if (ADKMiscStringFunctions.isBlankOrWhitespace(aSalesOrderKey)) {
      x1y3 = aSpace;
    } else {
      x1y3 = Row(children: [
        Text('Sales Order: ', style: TextStyle(fontWeight: FontWeight.bold)),
        Text(
          aSalesOrderKey,
        ),
      ]);
    }
    Widget x1y4;
    ADKDateTime? aSalesOrderCreatedDate =
        aSalesOrder.getSalesOrderCreatedDate();
    if (aSalesOrderCreatedDate == null) {
      x1y4 = aSpace;
    } else {
      x1y4 = Row(children: [
        Text('Created: ', style: TextStyle(fontWeight: FontWeight.bold)),
        Text(
          aLocale.displayDateTime(aSalesOrderCreatedDate),
        ),
      ]);
    }
    Widget x2y1 = Row(children: [
      Text('Account #: ', style: TextStyle(fontWeight: FontWeight.bold)),
      Text(
        aSalesOrder.getShipToKey(),
      ),
    ]);
    Widget x2y2 = Text(aSalesOrder.getShipToName());
    Widget x2y3 = Text(anAddressLine1);
    Widget x2y4 = Text(anAddressLine);
    Widget x3y1 = aPriceWidget;
    Widget x3y2 = aSpace;
    Widget x3y3;
    if (widget.isEdit) {
      x3y3 = TextButton(
        child: Text('Add Product'),
        onPressed: () {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => ChooseProductScreen(widget.actionRefresh)));
        },
      );
    } else if (this.isListOrderMode()) {
      x3y3 = aSpace;
    } else {
      x3y3 = TextButton(
        child: Text('Edit Order'),
        onPressed: () {
          bool isEditOrderEnabled = widget.salesOrderSession.isChangeable();
          if (!isEditOrderEnabled) {
            if (widget.salesOrderSession.isCBSOnHold()) {
              ADKDialog2.showAlertDialogText(
                  context, ADKGlobal.scContactCustomerService());
            } else {
              ADKDialog2.showAlertDialogText(
                  context, 'This order cannot be changed at this time.');
            }
          } else {
            Cart aCart = UserData.getUser().getCart();
            if (aCart.isCartInProgress()) {
              ADKDialog2.showAlertDialogText(context,
                  'An existing order cannot be changed while a cart is in progress.');
            } else {
              aCart.resetCart();
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => OrderPage(
                      widget.salesOrderSession, true, widget.actionRefresh)));
            }
          }
        },
      );
    }

    Widget x3y4 = aSpace;
    return Table(
      //border: TableBorder.all(),
      //border:TableBorder.lerp(0,0,0,0),
      columnWidths: aMap,
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: <TableRow>[
        TableRow(
          children: <Widget>[x1y1, x2y1, x3y1],
        ),
        TableRow(
          children: <Widget>[x1y2, x2y2, x3y2],
        ),
        TableRow(
          children: <Widget>[x1y3, x2y3, x3y3],
        ),
        TableRow(
          children: <Widget>[x1y4, x2y4, x3y4],
        ),
      ],
    );
  }

  Widget buildTileTitleWideX(
      BuildContext context, SalesOrderSession aSalesOrder) {
    List<Widget> aMainRow = List.empty(growable: true);
    List<Row> aFirstColumnLines = [];
    if (aSalesOrder.getCustomerPO().isNotEmpty) {
      aFirstColumnLines.add(Row(children: [
        new Text("Purchase Order: "),
        new Text(aSalesOrder.getCustomerPO(),
            style: TextStyle(fontWeight: FontWeight.bold)),
      ]));
    }

    //if (aSalesOrder.getSalesOrderKey().isNotEmpty) {
    List<Widget> aRowWidgets = List.empty(growable: true);
    aRowWidgets.add(Row(children: [
      new Text("Sales Order: " + aSalesOrder.getSalesOrderKey(),
          style: TextStyle(fontWeight: FontWeight.bold)),
    ]));
    aRowWidgets.add(Row(children: [
      new Text('Account #: ' + aSalesOrder.getShipToKey(),
          style: TextStyle(fontWeight: FontWeight.bold)),
    ]));
    Row aRow = Row(children: aRowWidgets);
    //}
    ADKDateTime? aDeliveryDate = aSalesOrder.getDeliveryDate();
    if (aDeliveryDate != null) {
      aFirstColumnLines.add(Row(children: [
        //new Text("Delivery: "),
        new Text(
          UserData.getLocale().displayDateTimeFormat(
              aDeliveryDate, EnvironmentLocale.DAYMONTHYYYY),
          style: TextStyle(fontWeight: FontWeight.bold),
        )
      ]));
    }
    Column aFirstColumn = Column(
      children: aFirstColumnLines,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    );
    aMainRow.add(aFirstColumn);

    List<Row> aSecondColumnLines = [];
    aSecondColumnLines.add(Row(children: [
      new Text('Account #: '),
      new Text(aSalesOrder.getShipToKey(),
          style: TextStyle(fontWeight: FontWeight.bold)),
    ]));

    aSecondColumnLines.add(Row(children: [
      new Text(aSalesOrder.getShipToName()),
    ]));

    AddressI anAddress = aSalesOrder.getShipToAddress();
    String anAddressLine = "";
    String? anAddressLine1 = anAddress.getAddressLine1();
    String? anAddressCity = anAddress.getCity();
    String? anAddressState = anAddress.getState();
    String? anAddressPostalCode = anAddress.getPostalCode();
    if ((anAddressLine1.isNotEmpty ||
            anAddressCity.isNotEmpty ||
            anAddressState.isNotEmpty ||
            anAddressPostalCode.isNotEmpty)) {
      anAddressLine = /*anAddressLine1 +
          ", " +*/
          anAddressCity + ", " + anAddressState + " " + anAddressPostalCode;
    }

    String aShipToString = aSalesOrder.getShipToKey();
    if (aSalesOrder.getShipToName().isNotEmpty) {
      aShipToString = aShipToString + ' ' + aSalesOrder.getShipToName();
    } else if (anAddress.getAddressLine1().isNotEmpty) {
      aShipToString = aShipToString + ' ' + anAddress.getAddressLine1();
    }

    aSecondColumnLines.add(Row(children: [new Text(aShipToString)]));
    aSecondColumnLines.add(Row(children: [new Text(anAddressLine)]));

    Column aSecondColumn = Column(
      children: aSecondColumnLines,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
    );
    aMainRow.add(aSecondColumn);

    //Row aColumns = Row();
    Row aColumns = Row(
      children: aMainRow, //crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.max,
    );

    //return Expanded(child: aColumns);
    return aColumns;
  }

  Widget orderTile(BuildContext context, SalesOrderSession aSalesOrder) {
    Color? aColor;
    Icon aLeadingIcon = Icon(
      Icons.local_shipping,
      color: ADKTheme.BUTTON_ICONCOLOR,
    );

    Widget lines = this.buildTileTitle(context, aSalesOrder);
    bool allowAddProduct;
    if (ADKGlobal.isWideScreen(context)) {
      allowAddProduct = false;
    } else {
      allowAddProduct = true;
    }
    Card aCard = Card(
        color: aColor,
        child: ListTile(
          title: lines,
          trailing: this.menu(context, allowAddProduct),
        ));
    return aCard;
  }

  Widget menu(BuildContext context, bool allowAddProduct) {
    int CANCEL_ORDER = 0;
    int EDIT_ORDER = 1;
    int ADD_PRODUCT = 2;
    int CANCEL_CHANGES = 3;

    bool isCancelOrderEnabled = widget.salesOrderSession.isChangeable();
    bool isEditOrderEnabled = isCancelOrderEnabled;

    bool isAddProductEnabled = isCancelOrderEnabled;

    bool isCancelChangesEnabled = isCancelOrderEnabled;

    List<PopupMenuEntry<int>> aList = List.empty(growable: true);
    if (!widget.isEdit) {
      if (!widget.isAddProductMode) {
        if (allowAddProduct) {
          aList.add(PopupMenuItem(
            value: EDIT_ORDER,
            enabled: isEditOrderEnabled,
            child: Row(children: [
              Icon(
                Icons.edit,
                color: ADKTheme.BUTTON_ICONCOLOR,
              ),
              Text(
                'Edit Order',
              )
            ]),
          ));
        }
      }
    } else {
      if (allowAddProduct) {
        aList.add(PopupMenuItem(
          value: ADD_PRODUCT,
          enabled: isAddProductEnabled,
          child: Row(children: [
            Icon(
              Icons.add,
              color: ADKTheme.BUTTON_ICONCOLOR,
            ),
            Text(
              'Add Product',
            )
          ]),
        ));
      }
      if (UserData.getUser().getCart().isCartInProgress()) {
        aList.add(PopupMenuItem(
          value: CANCEL_CHANGES,
          enabled: isCancelChangesEnabled,
          child: Row(children: [
            Icon(
              Icons.clear,
              color: ADKTheme.BUTTON_ICONCANCELCOLOR,
            ),
            Text(
              'Cancel Changes',
            )
          ]),
        ));
      }
    }
    aList.add(PopupMenuItem(
      value: CANCEL_ORDER,
      enabled: isCancelOrderEnabled,
      child: Row(children: [
        Icon(
          Icons.cancel,
          color: ADKTheme.BUTTON_ICONCANCELCOLOR,
        ),
        Text(
          'Cancel Order',
        )
      ]),
    ));

    Widget aMenu = PopupMenuButton<int>(
      elevation: 4,
      onSelected: (int result) {
        if (result == CANCEL_ORDER) {
          if (!isCancelOrderEnabled) {
            ADKDialog2.showAlertDialogText(
                context, 'This order can no longer be canceled at this time.');
          } else {
            confirmCancelOrderPopup(context);
          }
        } else if (result == CANCEL_CHANGES) {
          if (!isCancelChangesEnabled) {
            ADKDialog2.showAlertDialogText(
                context, 'This order can no longer be changed at this time.');
          } else {
            showDialog(
              useRootNavigator: false,
              context: context,
              builder: (context) {
                return StatefulBuilder(
                  builder: (BuildContext context, StateSetter setState) {
                    return this.buildCancelChangesDialog(
                        context, widget.salesOrderSession, setState);
                  },
                );
              },
            );
          }
        } else if (result == EDIT_ORDER) {
          if (!isEditOrderEnabled) {
            if (widget.salesOrderSession.isCBSOnHold()) {
              ADKDialog2.showAlertDialogText(
                  context, ADKGlobal.scContactCustomerService());
            } else {
              ADKDialog2.showAlertDialogText(
                  context, 'This order cannot be changed at this time.');
            }
          } else {
            Cart aCart = UserData.getUser().getCart();
            if (aCart.isCartInProgress()) {
              ADKDialog2.showAlertDialogText(context,
                  'An existing order cannot be changed while a cart is in progress.');
            } else {
              aCart.resetCart();
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => OrderPage(
                      widget.salesOrderSession, true, widget.actionRefresh)));
            }
          }
        } else if (result == ADD_PRODUCT) {
          if (!isAddProductEnabled) {
            if (widget.salesOrderSession.isCBSOnHold()) {
              ADKDialog2.showAlertDialogText(
                  context, ADKGlobal.scContactCustomerService());
            } else {
              ADKDialog2.showAlertDialogText(
                  context, 'This order cannot be changed at this time.');
            }
          } else {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) =>
                    ChooseProductScreen(widget.actionRefresh)));
          }
        }
      },
      itemBuilder: (context) => aList,
      child: Container(
        child: Icon(
          Icons.more_vert,
          color: ADKTheme.BUTTON_ICONCOLOR,
        ),
      ),
    );

    return aMenu;
  }

  AlertDialog buildCancelChangesDialog(BuildContext context,
      SalesOrderSession aSalesOrderSession, StateSetter setState) {
    // set up the buttons
    Widget noButton = TextButton(
      child: Text('No'),
      onPressed: () {
        setState(() {
          widget.isLoading = false;
        });
        Navigator.of(context).pop();
      },
    );
    Widget yesButton = TextButton(
      child: Text('Yes'),
      onPressed: () {
        setState(() {
          widget.isLoading = true;
          widget.actionRefresh();
        });
        //Attempt to cancel the appointment
        try {
          UserData.getUser().getCart().resetCart();
          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => OpenOrdersPage(widget.actionRefresh,
                  snackBarMessage: 'Changes were canceled')));
        } catch (all) {
          setState(() {
            widget.isLoading = false;
          });
          ADKDialog2.showAlertDialog(context, all, 2);
        }
      },
    );

    AlertDialog aConfirmCancelDialog = AlertDialog(
      title: Text("Confirm"),
      content: Stack(children: [
        Text("Are you sure you want to lose the changes to this order?"),
        _showCircularProgress()
      ]),
      actions: [
        noButton,
        yesButton,
      ],
    );

    return aConfirmCancelDialog;
  }

  void confirmCancelOrderPopup(BuildContext context) {
    showDialog(
      useRootNavigator: false,
      context: context,
      builder: (context) {
        String contentText = "Content of Dialog";
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
            return this
                .buildCancelDialog(context, widget.salesOrderSession, setState);
          },
        );
      },
    );
  }

  void orderCannotBeCanceledPopup(BuildContext context) {
    showDialog(
      useRootNavigator: false,
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Text('This order can no longer be canceled.'),
          actions: <Widget>[
            TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('Close')),
          ],
        );
      },
    );
  }

  AlertDialog buildCancelDialog(BuildContext context,
      SalesOrderSession aSalesOrderSession, StateSetter setState) {
    // set up the buttons
    Widget noButton = TextButton(
      child: Text('No'),
      onPressed: () {
        setState(() {
          widget.isLoading = false;
        });
        Navigator.of(context).pop();
      },
    );
    Widget yesButton = TextButton(
      child: Text('Yes'),
      onPressed: () {
        setState(() {
          //_errorMessage = "";
          widget.isLoading = true;
        });
        //Attempt to cancel the appointment
        try {
          ADKDateTime? aDeliveryDate = aSalesOrderSession.getDeliveryDate();
          ADKURLBuilder()
              .cancelSalesOrder(aSalesOrderSession)
              .then((aSalesOrderSession) {
            setState(() {
              widget.isLoading = false;
              Cart aCart = UserData.getUser().getCart();
              if (aCart.isCartInProgress()) {
                SalesOrderSession? anInProgressSalesOrder =
                    aCart.getSalesOrderSession();
                if (anInProgressSalesOrder != null) {
                  if (ADKCompare.equals(aSalesOrderSession.getSalesOrderKey(),
                      anInProgressSalesOrder.getSalesOrderKey())) {
                    //Only reset the cart if its the same order we are canceling
                    aCart.resetCart();
                  }
                }
              }
            });
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => OpenOrdersPage(widget.actionRefresh,
                    snackBarMessage: 'Order for ' +
                        UserData.getLocale().displayDateTimeFormat(
                            aDeliveryDate, EnvironmentLocale.DAYMONTHYYYY) +
                        ' has been canceled')));
          }).catchError((all) {
            setState(() {
              widget.isLoading = false;
            });
            ADKDialog2.showAlertDialog(context, all, 2);
          });
        } catch (all) {
          setState(() {
            widget.isLoading = false;
          });
          ADKDialog2.showAlertDialog(context, all, 2);
        }
      },
    );

    AlertDialog aConfirmCancelDialog = AlertDialog(
      title: Text("Confirm"),
      content: Stack(children: [
        Text("Are you sure you want to cancel this order?"),
        _showCircularProgress()
      ]),
      actions: [
        noButton,
        yesButton,
      ],
    );

    return aConfirmCancelDialog;
  }

  Widget _showCircularProgress() {
    if (widget.isLoading) {
      //return Center(child: CircularProgressIndicator());
      return CircularProgressIndicator();
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }
}
