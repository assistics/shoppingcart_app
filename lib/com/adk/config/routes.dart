class ADKSCRoutes {
  static const home = '/';
  static final String PAGE_OPENORDERSPAGE = "/openorderspage";
  static final String PAGE_WEEKORDERSPAGE = "/weekorderspage";
  static final String PAGE_CHOOSEPRODUCTS = "/chooseproductspage";
  static final String PAGE_CART = "/cart";
  static final String PAGE_LOGIN = "/loginpage";
  static final String PAGE_ORDER = "/order";

  static const String PAGE_PAYMENTMETHODS = '/paymentmethodspage';

  static final String PAGE_MENUPAGE = "/menupage";
}
